#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include "bitmapio.h"

main(){

       BITMAPINFOHEADER bitmapInfoHeader;
       unsigned char *imagem;
       
       if((imagem = LoadBitmapFile("imagem.bmp",&bitmapInfoHeader)) != NULL){
            puts("***IMAGE INFO***");
            printf("Image size: %d\n",bitmapInfoHeader.biSizeImage);
            printf("Image width: %d\n",bitmapInfoHeader.biWidth);
            printf("Image height: %d\n",bitmapInfoHeader.biHeight);
            printf("Image planes: %d\n",bitmapInfoHeader.biPlanes);
            system("pause");
            free(imagem);
       }else{
             printf("Erro ao abrir a imagem!");
             system("pause");     
       }
       
}
