#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <string.h>

#define GAME_SIZE 8

/*
    -*BATALHA NAVAL*-
    
    Autor: Lucas M. Carvalhaes
    Data: 04/05/2011
    Descri��o: Jogo de batalha naval
    jogado entre o computador e um jogador
    humano.
    Observa��o: Comentado em ingl�s para
    treino.

*/

int random(int min, int max){
     return min + (rand()%((max+1)-min));
}

int main(){
    
    // VARIABLES INITIALIZATION \\
    
    //Random setup
    srand(time(NULL));
    //Game grids
    char playerbattlegrid[GAME_SIZE][GAME_SIZE];
    char computerbattlegrid[GAME_SIZE][GAME_SIZE];
    int computeracctualgrid[GAME_SIZE][GAME_SIZE];
    int playeracctualgrid[GAME_SIZE][GAME_SIZE];
    //Grid states
    const char sea = '@';
    const char exploded = '*';
    const char waterhit = '-';
    //Ships
    const char names[3][13] = { "submarino", "destroyer", "porta-avioes" };
    const char submarino[] = "ED";
    const char destroyer[] = "E==D";
    const char portaavioes[] = "E====D";
    //Score handlers
    int player_hits = 0;
    int computer_hits = 0;
    int GAME_STATE = 1;
    int vez = 0;
    //Toggle and control vars
    int instructions = 0;
    int place = 1;
    int cplace = 1;
    int prepare = 1;
    char curr_ship[50];
    int comp_good_guess_x = -1;
    char joga_denovo = '0';
    int round_counter = 0;
    //Counters
    int x = 0;
    int y = 0;
    int i = 0;
    //Key holder
    char ph;
    //Map selector
    int sx = 0;
    int sy = 0;
    
    // PROGRAM START
    
    //Send a welcome screen
    puts("******************************");
    puts("*      BATALHA * NAVAL       *");
    puts("******************************");
    
    //Request the user the matrix size
    puts("");
    puts("Bem vindo a batalha naval!");
    puts("Pressione qualquer tecla para continuar...");
    getch();
    
    
    
    // GAME LOOP
    while(GAME_STATE == 1){
        
        //Prepare the arenas
        if(prepare){
           //Prepare the battle grids with the sea
           for(x=0;x<GAME_SIZE;x++){
               for(y=0;y<GAME_SIZE;y++){
                       playerbattlegrid[x][y] = sea;
                       computerbattlegrid[x][y] = sea;
                       playeracctualgrid[x][y] = 0;
                       computeracctualgrid[x][y] = 0;
               }
           }
           prepare = 0;
        }
        
        //Show the instructions
        if(instructions == 1){
                 system("color e");
                 system("cls");
                 puts("******************************");
                 puts("*         INSTRUCOES         *");
                 puts("******************************");
                 puts("*1-Na sua vez com WASD       *");
                 puts("*escolha um local do mapa e  *");
                 puts("*pressione f para atirar la. *");
                 puts("*                            *");
                 puts("*2-Na vez do oponente veja   *");
                 puts("*onde ele vai atirar.        *");
                 puts("******************************");
                 puts("Pressione qualquer tecla para iniciar o jogo...");
                 getch();
                 instructions = 0;
        }
        
        //Place the ships - computer
        if(cplace == 1){
             int placed = 0;
             system("cls");
             
            while(placed < 7){
                          
                //Set the chip for size check
                if(placed < 4){
                        strcpy(curr_ship,submarino);
                }
                if(placed >= 4 && placed <= 5){
                        strcpy(curr_ship,destroyer);
                }
                if(placed > 5){
                        strcpy(curr_ship,portaavioes);
                }
                 
                //Choose a random spot
                int allowed = 0;
                int spx=0, spy=0;
                while(allowed == 0){
                    allowed = 1;
                    spx = random(0,GAME_SIZE);
                    spy = random(0,GAME_SIZE);
                    //Check the spot
                    if((spx+strlen(curr_ship)) <= GAME_SIZE){
                        for(i=0;i<strlen(curr_ship);i++){
                            if(computeracctualgrid[i+spx][spy] != 0){
                                    allowed = 0;
                            }
                        }
                    }else{
                        allowed = 0;     
                    }
                }
                 
                 //If allowed place the current chip
                 for(x=0;x<strlen(curr_ship);x++){
                         computeracctualgrid[x+spx][spy] = 1;
                 }
                 placed++;
            }
             //Debug
             /*for(y=0;y<GAME_SIZE;y++){
                  for(x=0;x<GAME_SIZE;x++){
                        printf("%d",computeracctualgrid[x][y]);
                  }
                  printf("\n");
             }
             getch();*/
             
             cplace = 0;
        }
        
        //Place the ships - player
        if(place == 1){
                 system("cls");
                 puts("Coloque seus barcos no seu mapa.");
                 puts("A ordem sera 4 submarinos, 2 destroyers, 1 porta avioes");
                 puts("Para se mover use \"WASD\". Para colocar use \'e\'.");
                 int placed = 0;
                 
                 //Get the player current key and set the pos
                 //of the selector to s(0,0)
                 sx = 0;
                 sy = 0;
                while(placed < 7){
                      //Get the input
                      ph = getch();
                      system("cls");
                      
                      //Move the selector
                      if(ph == 'w' && sy>0){sy--;}
                      if(ph == 's' && sy<GAME_SIZE-1){sy++;}
                      if(ph == 'a' && sx>0){sx--;}
                      if(ph == 'd' && sx<GAME_SIZE-1){sx++;}
                      
                      //Print the current ship
                      printf("*******************************\n");
                      if(placed < 4){
                               printf("Peca atual: %s\n",names[0]);
                               strcpy(curr_ship,submarino);
                      }
                      if(placed >= 4 && placed <= 5){
                               printf("Peca atual: %s\n",names[1]);
                               strcpy(curr_ship,destroyer);
                      }
                      if(placed > 5){
                               printf("Peca atual: %s\n",names[2]);
                               strcpy(curr_ship,portaavioes);
                      }
                      puts("*******************************");
                      
                    //Check if the spot is clear
                    int allow = 1;
                    if((sx+strlen(curr_ship)) <= GAME_SIZE){
                        for(i=0;i<strlen(curr_ship);i++){
                            if(playerbattlegrid[i+sx][sy] != sea){
                                allow = 0;
                                system("color c");
                            }
                        }
                    }else{
                        allow = 0;
                        system("color c");      
                    }
                    if(allow){system("color a");}
                      
                    //add the ship
                    if(ph == 'e' && allow == 1){
                        //Draw the ship into the board
                        //if it does not pass the border
                        for(x=0;x<strlen(curr_ship);x++){
                            playerbattlegrid[x+sx][sy] = curr_ship[x];
                            playeracctualgrid[x+sx][sy] = 1;
                        }
                        placed++;
                    }
                      
                      //Draw the player arena
                    for(y=0;y<GAME_SIZE;y++){
                        for(x=0;x<GAME_SIZE;x++){
                            if(x == sx && y == sy){
                                printf("%c",177);
                            }else{
                                printf("%c",playerbattlegrid[x][y]);
                            }
                        }
                        printf("\n");
                    }
                }
                 
                ph = '0';
                place = 0;
                
                //Get the player current key and set the pos
                //of the selector to s(0,0)
                sx = 0;
                sy = 0;
        }
        
        //Player round
        
        //Set the color
        system("color 2");
        ph = '0';
        
        while(ph != 'f'){
                //Get the input
                ph = getch();
                system("cls");
                     
                //Move the selector
                if(ph == 'w' && sy>0){sy--;}
                if(ph == 's' && sy<GAME_SIZE-1){sy++;}
                if(ph == 'a' && sx>0){sx--;}
                if(ph == 'd' && sx<GAME_SIZE-1){sx++;}
                 
                //Draw the arenas
                //Computer
                puts("***********COMPUTER ARENA************");
                printf("*Computer hits: %d\n",computer_hits);
                for(y=0;y<GAME_SIZE;y++){
                    for(x=0;x<GAME_SIZE;x++){
                        if(x == sx && y == sy){
                            printf("%c",177);
                        }else{
                            printf("%c",computerbattlegrid[x][y]);
                        }
                    }
                    printf("\n");
                }
                puts("************PLAYER ARENA*************");
                printf("*Player hits: %d\n",player_hits);
                //Player
                for(y=0;y<GAME_SIZE;y++){
                    for(x=0;x<GAME_SIZE;x++){
                        printf("%c",playerbattlegrid[x][y]);
                    }
                    printf("\n");
                }
                
                //Fire! And check hit...
                if(ph == 'f' && computeracctualgrid[sx][sy] == 1
                                  && computerbattlegrid[sx][sy] != waterhit
                                  && computerbattlegrid[sx][sy] != exploded){
                    computerbattlegrid[sx][sy] = exploded;
                    player_hits++;
                    vez = 0;
                }else if(ph == 'f' && computerbattlegrid[sx][sy] != exploded){
                    computerbattlegrid[sx][sy] = waterhit;
                    vez = 0;
                }
        }
        
        //Computer round
        
        while(vez == 0){
             
             // AI : Select a center piece and prepare the next selection
             
             //Guesses
             int spx=0, spy=0;
             int valid_guess = 0;
             
             //No guesses, start in the middle, else shoot around
             while(valid_guess == 0){
                 if(comp_good_guess_x == -1){
                     //Guarantee that there is something in the middle to pick
                     //else, pick the borders too!
                     if(round_counter < 10){
                         spx = random(1,GAME_SIZE-1);
                         spy = random(1,GAME_SIZE-1);
                     }else{
                         spx = random(0,GAME_SIZE);
                         spy = random(0,GAME_SIZE);
                     }
                 }else{
                     spx = comp_good_guess_x + random(-1,1);
                 }
                 
                 //Check for validity
                 if(spx >= 0 && spx <= GAME_SIZE && spy >= 0 && spy <= GAME_SIZE){
                     if(playerbattlegrid[spx][spy] != exploded && playerbattlegrid[spx][spy] != waterhit){
                          valid_guess = 1;
                     }
                 }
                 
                 //Check if the computer picked a lonely spot
                 int lone_counter = 0;
                 if(spx-1 >= 0 && (playerbattlegrid[spx-1][spy] == waterhit
                          || playerbattlegrid[spx-1][spy] == exploded)){
                      lone_counter++;
                 }
                 if(spx+1 <= GAME_SIZE && (playerbattlegrid[spx+1][spy] == waterhit
                          || playerbattlegrid[spx+1][spy] == exploded)){
                      lone_counter++;
                 }
                 //He is alone, its not valid for this super inteligent computer!
                 if(lone_counter >= 2){
                     valid_guess = 0;
                     comp_good_guess_x = -1;
                 }else{
                     valid_guess = 1; 
                 }
                 
                 //Debug
                 //printf("Guess: [%d,%d] Good Guess: [%d] Validity: %d\n lone: %d",spx,spy,comp_good_guess_x,valid_guess,lone_counter);
             }
             
             //Computer did a valid guess, FIRE!
             if(playeracctualgrid[spx][spy] == 1){
                  playerbattlegrid[spx][spy] = '*';
                  comp_good_guess_x = spx;
                  computer_hits++;
             }else{
                  playerbattlegrid[spx][spy] = '-';
                  comp_good_guess_x = -1;
             }
             
             //Player time!
             vez = 1;
        }
        
        //Round
        round_counter++;
        
        //Victory check
        if(computer_hits == 22){
              GAME_STATE = 2;
        }
        if(player_hits == 22){
              GAME_STATE = 3;
        }
        
        //End Game!
        if(GAME_STATE != 1){
             system("cls");
             puts("******************************");
             puts("*      BATALHA * NAVAL       *");
             puts("******************************");
             puts("");
             puts("FIM DE JOGO!");
             if(GAME_STATE == 2)
             puts("O COMPUTADOR GANHOU!");
             else
             puts("VOCE GANHOU!");
             printf("TOTAL DE RODADAS: %d",round_counter);
             puts("");
             puts("******************************");
             puts("");
             //Play again menu
             puts("Deseja jogar novamente? (s/n)");
             joga_denovo = getch();
             //For dummies...
             while(joga_denovo != 's' && joga_denovo != 'n'){
                 puts("somente: s/n!");
                 joga_denovo = getch();
             }
        }
        
        //Play again
        if(joga_denovo == 's'){
             computer_hits = 0;
             player_hits = 0;
             GAME_STATE = 1;
             vez = 0;
             prepare = 1;
             place = 1;
             cplace = 1;
             prepare = 1;
             curr_ship[50];
             comp_good_guess_x = -1;
             joga_denovo = '0';
             x = 0;
             y = 0;
             i = 0;
             ph = '0';
             sx = 0;
             sy = 0;
             round_counter = 0;
        }
    }
    
    return 0;   
}
