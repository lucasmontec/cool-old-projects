#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <string.h>

/*
	Autor: Lucas Montenegro Carvalhaes
	Data: 27/04/2011
	Revis�o: 20
	Descri��o:	Jogo de forca feito em C sem o uso de fun��es ou labels.
*/


//INICIO DO CODIGO PRINCIPAL

int main(){
 //Jogo da forca feito em C - o sofrimento!
 //Declara��o das variaveis
 
 //Seed do random
 srand(time(NULL));
 
 //Variavel para armazenar as palavras
 char palavra[10];
 char mpalavras[10][10] = {
     "macaco",
     "peixe",
     "ronaldo",
     "foguete",
     "grama",
     "assintota",
     "guerra",
     "fogo",
     "lua",
     "forca" 
 };
 //Var para receber a letra como buffer
 char input = ' ';
 //Vars para guardar coisas
 int erros = 0;
 int estado_do_jogo = 1; // jogo come�a rodando edj = 1, edj = 2 derrota, edj = 3 vitoria
 int instrucoes = 1;
 int escolhe = 1;
 int tamanho_da_palavra = 0;
 int lec=0, i=0;
 int ja_digitada = 0, certa = 0, jogar_denovo = 0, checador_vitoria = 0;
 //Var para guardar as letras erradas
 char letras_erradas[28] = "                           ";
 char letras_certas[12] = "           ";
 //Var para desenhar a forca
 //as barras duplas s�o para evitar o uso de fun��es
 //do printf
  char forca[7][5][8] = {
    {
      "  ___  ",
      "     | ",
      "     | ",
      "     | ",
      "_____|_",
    },{  
      "  ___  ",
      "  O  | ",
      "     | ",
      "     | ",
      "_____|_",
    },{  
      "  ___  ",
      "  O  | ",
      "  |  | ",
      "     | ",
      "_____|_",
    },{
      "  ___  ",
      "  O  | ",
      " /|  | ",
      "     | ",
      "_____|_"
    },{
      "  ___  ",
      "  O  | ",
      " /|\\ | ",
      "     | ",
      "_____|_"
    },{
      "  ___  ",
      "  O  | ",
      " /|\\ | ",
      " /   | ",
      "_____|_"
    },{
      "  ___  ",
      "  O  | ",
      " /|\\ | ",
      " / \\ | ",
      "_____|_"
    }
 };

 //LOOP DO JOGO
 while(estado_do_jogo == 1){
     //escolhe uma palavra
     if(escolhe){
         int index = rand()%10;
         strcpy(palavra,mpalavras[index]);
         tamanho_da_palavra = strlen(palavra);
         
         //trava escolha
         escolhe = 0;
     }          
                       
     //Desenha as instrucoes
     if(instrucoes){
         puts("*********************************");
         puts("************ FORCA **************");
         puts("*********************************");
         puts("*Instrucoes:                    *");
         puts("*Escolha uma letra(A-Z).        *");
         puts("*Se errar perde uma chance.     *");
         puts("*Se perder todas as 6 perde     *");
         puts("*o jogo.                        *");
         puts("*********************************");
         //trava esse trecho
         instrucoes = 0;
         puts("");
         puts("Pressione qualquer tecla para comecar...");
         getch();
         system("cls");
     }
     
     //Desenha chapa
     puts("************ FORCA **************");
     puts("");
     
     //Desenha o estado da forca atual forca[7][5][8]
     for(i=0;i<5;i++){
           printf("%s\n",forca[erros][i]);
     }
     puts("");
     
     //Dsesenha as pads de letras
     puts("Palavra:");
     for(i=0;i<tamanho_da_palavra;i++){
           if(letras_certas[i] != ' '){
                printf("%c",letras_certas[i]);
           }else{
                printf("_'");
           }
     }
     puts("");
     
     //Desenha as letras erradas
     puts("");
     puts("Letras erradas:");
     puts(letras_erradas);
     puts("");
     printf("Tentativas: %d",6-erros);
     
     //Desenha chapa
     puts("");
     puts("*********************************");
     
     //Pede uma letra
     puts("");
     puts("");
     puts("Digite uma letra:");
     input = getch();
     
     //Trabalha com o input
     //Ve se a letra ja foi digitada
     ja_digitada = 0;
     certa = 0;
     checador_vitoria = 0;
     for(i=0;i<27;i++){
           if(letras_erradas[i] == input){
               ja_digitada = 1;
           }
     }
     //puts("Letra ja digitada!"); -- teoricamente deveria funcionar... nao pode usar funcoes  
     if(ja_digitada == 0){
         //Ve se a letra esta na palavra
         for(i=0;i<tamanho_da_palavra;i++){
             if(input == palavra[i]){
                   letras_certas[i] = input;
                   certa = 1;
             }
             if(letras_certas[i] == palavra[i]){
                  checador_vitoria++;
             }                             
         }
         if(certa == 0){
             erros++;
             letras_erradas[lec] = input;
             lec++;
             
             //checa a derrota
             if(erros == 6){
                  estado_do_jogo = 2;    
             }
         }
         //checa vitoria
         if(checador_vitoria == tamanho_da_palavra){
             estado_do_jogo = 3;
         }
         
     }
     
     //Limpa a tela
     system("cls");
     
     //Caso ocorra um fim de jogo
      if(estado_do_jogo == 2 || estado_do_jogo == 3){
         puts("*********************************");
         puts("************ FORCA **************");
         puts("*********************************");
         if(estado_do_jogo == 2)
             puts("*   Infelizmente voce perdeu!   *");
         else
             puts("*     Voce ganhou, parabens!    *");
         puts("*********************************");
         puts("");
         puts("Deseja jogar denovo? (s/n)");
         jogar_denovo = getch();
         if(jogar_denovo == 's'){
              //Reseta tudo
              estado_do_jogo = 1;
              escolhe = 1;
              erros = 0;
              strcpy(letras_erradas,"                           ");
              strcpy(letras_certas,"           ");
              lec = 0;
              input = ' ';
              jogar_denovo = ' ';
              checador_vitoria = 0;
              system("cls");
         }else{
              estado_do_jogo = 0;
         }
      }
 }
 
 return 0;
}


/*
	Coment�rios do programa:
		-Imports:
		Como ainda n�o aprendemos fun��es e n�o quero
		saturar meu codigo de fors vou chamar algumas funcoes
		prontas para lidar com entradas e string
*/