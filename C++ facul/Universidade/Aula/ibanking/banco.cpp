/*
***************INTERNET BANKING***************
*Equipe:                                     *
*Lucas Yamanaka                  RA: 407550  *
*Thiago Faria Nogueira           RA: 407534  *
*Lucas Montenegro Carvalhaes     RA: 407666  *
**********************************************

**Objetivos:
Dominar o uso de arquivos em programacao;
Aprender a abstrair problemas complexos em
solu��es simples e modulares;
Aprender a trabalhar em grupo dividindo tarefas
e trechos de c�digo (m�dulos);

**Principais Dificuldades:
1-Organiza��o do grupo.
2-Divis�o de tarefas.
3-Padroniza��o do sistema.

1: O principal problema quando se tem uma
tarefa mais complexa � a organiza��o do grupo
para fazer a abstra��o dos maiores problemas.
Tivemos muitas ideias de solu��es que muitas
vezes conflitavam por quest�es como tempo
de resolu��o, numero de utiliza��es do aplicativo,
complexidade da resposta, praticidade. Esse problema
resultou em um c�digo quebrado que exigiu trabalho
em dobro para uma organiza��o final afim de atingir
funcionalidade com o custo da eficiencia.

2: Dividir as tarefas foi algo complexo pois cada
membro tinha uma vis�o diferente do projeto como um
todo e a padroniza��o poderia se tornar um problema.
Acabamos por separar a produ��o das fun��es para cada
membro mantendo como padr�o o tipo conta que iriamos
utilizar. Isso fez com que surgissem abordagens diferentes
na forma de acessar o arquivo contendo as contas. Vis�es
diferentes s�o boas quando se est� na etapa de projeto,
por�m na produ��o do mesmo e em m�dulos diferentes essas
diverg�ncias nos causaram perda em efici�ncia.

3: A padroniza��o foi um pesadelo desde o come�o. Todos
viam maneiras diferentes de se montar um sistema de link
entre as memorias dinamica e est�tica. A jun��o das id�ias
levou muito tempo e como os outros dois problemas nos atrasaram
isso causou um problema de acesso. A padroniza��o teve de ser
realizada na linkagem final dos m�dulos e isso causou muitos
bugs.

**Conclus�es:
Tiramos como conclus�o do nosso primeiro grande projeto uma s�rie
de fatores que se devem levar em conta na hora de montar um sistema.
Primeiro aprendemos as prioridades tanto funcionais quanto temporais
de se montar um sistema de software. Vimos tambem a importancia de
comunica��o constante durante a produ��o do c�digo, que foi um de
nossos problemas, assim como aprendemos o quanto � importante explorar
diferentes ideias e abordagens mantendo o escopo do projeto. � importante
mencionar tambem que sempre que se trabalha com memoria est�tica deve se
planejar o uso em conjunto e com grande foco no numero de acessos
minimo necessario, poupando tempo e melhorando a eficiencia do programa.
Com o vicio de computadores super rapidos que a maioria de nos possui
tournou se dificil a persep��o da lentidao do acesso estatico e esse projeto
nos permitiu guardar essa informa��o muito bem.


**Principais atuacoes:
Lucas Yamanaka:
-Sistema de conta (typedef)
-Fun��o de cria��o de arquivo padr�o
-Fun��o de carga de contas na mem�ria din�mica
-Debuging

Thiago Faria Nogueira:
-Sistema de extrato (hist�rico de movimenta��es banc�rias)
-Sistema de login
-Sistema de menu
-Corre��o de problemas p�s debug

Lucas Montenegro Carvalhaes:
-Sistema de transfer�ncia entre contas
-Sistema de saque
-Sistema de leitura de extrato (hist�rico de movimenta��es banc�rias)
-Sistema de dep�sito
-Linkagem dos m�dulos
-Organiza��o do c�digo
-Cabe�alho (comentarios)
*/



//Inicio das declara��es
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <conio.h>


//Variaveis e tipos customizados
//Lucas Yamanaka
typedef struct account
{
    int accountNumber;
    int agencyNumber;
    char accountPass[20];
    int failedAttempts;
    char lastLogin[50];
    char accountOwner[100];
    double transAcc_Balance;
    double savingsAcc_Balance;
}account;
//Lucas Carvalhaes
account acc[3]; //Variavel global para armazenar as contas

//Prot�tipos de fun��es
//Lucas carvalhaes
void readAcc(account *p1);
int createAccounts();
void get_history(int accountnumber);
int withdraw(int acctype, int accountnumber);
int deposit(int acctype, int accountnumber);
int transfer(int acctype, int accountnumber);
int trans_savings();
void add_action(int accountNumber,int action,int accountType,double quantity,int accountTarget,int accountTargetType);
void menu(int account);
int login();


//Inicio da fun��o principal MAIN
int main(){
    //Declaracao de variaveis
	FILE *file;

    //Ao abrir o software devemos checar se existe o arquivo padr�o
    if((file = fopen("accounts.bin", "rb")) == NULL){
             printf("Essa e a primeira vez que voce utiliza esse software!\n");
             printf("O arquivo de contas sera criado.\nPressione enter para prosseguir...\n");
             getch();
             createAccounts();
    }else{
          fclose(file);
    }

    readAcc(acc);

    //Com o arquivo pronto pede ao cliente que fa�a o login
    login();
    //O login ja acessa a fun��o menu passando o parametro necessario.
    //A fun��o menu � um loop que realiza opera��es at� o usuario
    //desejar sair.

    return 0;
}


//Inicio do corpo funcional

//Lucas Yamanaka:
/*
*****************************************
*         Transfere os dados            *
*    armazenados no arquivo para a      *
*          memoria volatil              *
*****************************************
*/
void readAcc(account *p1)
{
    //Cria um ponteiro para arquivo
    FILE *file;
    //Utiliza o ponteiro criado para ler o arquivo de contas
    if((file=fopen("accounts.bin", "rb"))!=NULL){
        //Le todas as 3 contas do arquivo padrao
        fread(p1, sizeof(account), 3, file);
        //Feita a leitura fecha o arquivo
        fclose(file);
    }else{
        //Quando nao encontra o arquivo
        p1 = NULL;
    }
}

/*
*****************************************
*         Cria um arquivo padrao        *
*****************************************
*/
int createAccounts()
{
    //Prepara um ponteiro para acessar o arquivo
    FILE *file;
    //Prepara um vetor com as tres contas dos integrantes do grupo
    account accVector[3];

    //Monta no vetor de contas os valores padr�o de cada conta
    //Numero da conta
    accVector[0].accountNumber=111111;
    accVector[1].accountNumber=111112;
    accVector[2].accountNumber=111113;
	//Numero da agencia
	accVector[0].agencyNumber=12345;
	accVector[1].agencyNumber=12345;
	accVector[2].agencyNumber=12345;
    //Password
    strcpy(accVector[0].accountPass, "aaaaaa");
    strcpy(accVector[1].accountPass, "aaaaaa");
    strcpy(accVector[2].accountPass, "123456");
    //Tentativas falhas
    accVector[0].failedAttempts=0;
    accVector[1].failedAttempts=0;
    accVector[2].failedAttempts=0;
    //Titulares
    strcpy(accVector[0].accountOwner, "Lucas Yamanaka");
    strcpy(accVector[1].accountOwner, "Thiago Faria Nogueira");
    strcpy(accVector[2].accountOwner, "Lucas Montenegro Carvalhaes");
    //Dinheiro - Corrente
    accVector[0].transAcc_Balance=4075.50;
    accVector[1].transAcc_Balance=4075.34;
    accVector[2].transAcc_Balance=0;
    //Dinheiro - Poupan�a
    accVector[0].savingsAcc_Balance=2481.34;
    accVector[1].savingsAcc_Balance=4075.34;
    accVector[2].savingsAcc_Balance=9999.99;

    //Tenta abrir o arquivo e retorna 1 caso haja problema
    if((file = fopen("accounts.bin", "wb")) == NULL)
             return 1;

    //Realiza a escrita do conteudo das contas gerado
    fwrite(accVector, sizeof(account), 3, file);
    //Fecha o arquivo
    fclose(file);
    //Retorna 0 para opera��o bem sucedida
    return 0;
}

//Lucas Carvalhaes:
/*
*****************************************
*       Fun��o para pegar o extrato     *
*****************************************
*/
void get_history(int accountnumber){
    FILE *accountfile;
    char accountname[5];
    char line[100];
    sprintf(accountname,"h%d.txt",accountnumber);
    
    system("cls");
    //Tenta abrir a conta desejada
    if((accountfile = fopen(accountname,"rb")) != NULL){

        while(fgets( line, 100, accountfile ) != NULL){
            printf("%s",line);
        }
    }else{
        printf("Erro, Nao foi possivel localizar o arquivo de historico!\n");
    }
}

/*
*****************************************
*      Fun��o para realizar um saque    *
*****************************************
*/
int withdraw(int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    double amount = -1;

    //Procura a conta para fazer o saque
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!\n");
        return -1;
    }

    //Quantia a sacar
    //While mantem logico
    do{
        printf("Digite a quantidade de dinheiro a sacar: (0 para cancelar)\n");
        scanf("%lf",&amount);
    }while(amount < 0);

    //Cancela a opera��o digitando 0
    if(amount == 0){
        printf("Operacao cancelada.\n");
        //retorna cancelamento
        return 0;
    }

    //Confere se existe dinheiro suficiente para sacar
    if(acctype == 1){ //Conta poupan�a
        if(acc[accountindex].transAcc_Balance - amount < 0){
            printf("Nao ha fundos para a operacao!\n");
            //Nao ha fundos. Retorna erro.
            return -1;
        }else{
            //Realiza o saque
            acc[accountindex].transAcc_Balance -= amount;
            //Salva dados no historico
            add_action(accountnumber,1,acctype,amount,0,0);
            //Retorna sucesso
            return 1;
        }
    }else if(acctype == 2){ //Conta corrente
        if(acc[accountindex].savingsAcc_Balance - amount < 0){
            printf("Nao ha fundos para a operacao!\n");
            //Nao ha fundos! Retorna erro!
            return -1;
        }else{
            //Realiza o saque
            acc[accountindex].savingsAcc_Balance -= amount;
            //Salva dados no historico
            add_action(accountnumber,1,acctype,amount,0,0);
            //Retorna sucesso
            return 1;
        }
    }
}

/*
*****************************************
*    Fun��o para realizar um deposito   *
*****************************************
*/
int deposit(int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    double amount = -1;

    //Procura a conta para fazer o deposito
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!\n");
        return -1;
    }

    //Quantia a depositar
    do{
        system("cls");
        printf("Digite a quantidade de dinheiro a depositar: (0 para cancelar)\n");
        fflush(stdin);
        scanf("%lf",&amount);
    }while(amount < 0);

    //Cancelamento
    if(amount == 0){
        printf("Operacao cancelada.\n");
        //retorna cancelamento
        return 0;
    }

    //Seleciona a conta
    if(acctype == 1){ //Conta poupan�a
        acc[accountindex].transAcc_Balance += amount;
        //Salva dados no historico
        add_action(accountnumber,2,acctype,amount,0,0);
        //returna sucesso
        return 1;
    }else if(acctype == 2){ //Conta corrente
        acc[accountindex].savingsAcc_Balance += amount;
        //Salva dados no historico
        add_action(accountnumber,2,acctype,amount,0,0);
        //retorna sucesso
        return 1;
    }
}

/*
******************************************
* Fun��o para realizar uma transferencia *
******************************************
*/
int transfer(int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    int targetaccindex = -1;
    int targetaccnumber = -1;
    double amount = -1;

    //Procura a conta para fazer a tranferencia
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!");
        return -1;
    }

    //Recebe e procura conta alvo
    do{
        printf("Digite o numero da conta para receber a transferencia: (0 para cancelar)\n");
        scanf("%d",&targetaccnumber);
    }while(targetaccnumber < 0);

    if(targetaccnumber == 0){
        printf("Operacao cancelada.\n");
        return 0;
    }

    //Localiza conta alvo
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == targetaccnumber)
            targetaccindex = i;
    }

    //Erro na localiza��o da conta!
    if(targetaccindex == -1){
        system("cls");
        printf("Erro! Nao foi possivel localizar a conta!\n");
        return -1;
    }

    //Quantia a tranferir
    //While mantem logico
    do{
        printf("Digite a quantidade de dinheiro a transferir: (0 para cancelar)\n");
        scanf("%lf",&amount);
    }while(amount < 0);

    //Cancelamento
    if(amount == 0){
        printf("Operacao cancelada.\n");
        //Retorna cancelamento
        return 0;
    }

    //Seleciona a conta
    if(acctype == 1){ //Conta poupan�a
        //Checa se h� dinheiro para transferir
        if(acc[accountindex].transAcc_Balance - amount < 0){
            printf("Fundo insuficiente para a operacao.\n");
            //Retorna erro, sem fundos.
            return -1;
        }

        //Transfere
        acc[targetaccindex].transAcc_Balance += amount;
        acc[accountindex].transAcc_Balance -= amount;
        //Salva dados no historico
        add_action(accountnumber,3,acctype,amount,targetaccnumber,acctype);
        //Sucesso
        return 1;
    }else if(acctype == 2){ //Conta corrente
        //Checa se h� dinheiro para transferir
        if(acc[accountindex].savingsAcc_Balance - amount < 0){
            printf("Fundo insuficiente para a operacao.\n");
            //Erro, sem fundos
            return -1;
        }

        //Transfere
        acc[targetaccindex].savingsAcc_Balance += amount;
        acc[accountindex].savingsAcc_Balance -= amount;
        //Salva dados no historico
        add_action(accountnumber,3,acctype,amount,targetaccnumber,acctype);
        //Sucesso
        return 1;
    }
}


//Thiago Faria Nogueira:
/*
******************************************
*   fun��o para o cliente excolher       *
*  trabalhar com a conta poupan�a ou     *
*         a conta corrente               *
******************************************
*/
int trans_savings()
{
	int accountType=0;//come�a em zero para entrar no loop

	while(accountType!=1 && accountType!=2)//sai do loop s� quando tiver certeza que o valor digitado � um valor v�lido
	{
		system("cls");
		printf("Quer fazer uma operacao com sua Conta Corrente (Digite 1) ou com sua Poupanca (Digite 2)?\n");
		scanf("%d",&accountType);//op��o do usuario
	}
	return accountType;//retorna a op��o escolhida pelo usuario

}

/*
**************************************
*     fun��o para adicionar no       *
*    historico de a��es da conta     *
*    as transa��es feitas por ela    *
**************************************
*/
void add_action(int accountNumber,int action,int accountType,double quantity,int accountTarget,int accountTargetType)
{
	char accountName[7],workingType[15],targetType[15],accountTargetName[7];
	FILE *file;
	FILE *file2;

	time_t rawtime;//variavel do tipo time_t vai guardar um valor em segundos desde 1 de janeiro de 1970
	struct tm * date;//o struct tm eh uma fun��o do time.h e permite gravar uma data completa na forma bruta
	time (&rawtime);//time vai retornar o valor da data de agora em segundos
	date = localtime(&rawtime);//o date (struct tm) vai guardar a data no instante de forma numerica

    sprintf(accountName,"h%d.txt",accountNumber);
    sprintf(accountTargetName,"h%d.txt",accountTarget);

    //abre o arquivo de historico da conta do usuario e adiciona, se nao existir ele criar�
	if((file = fopen(accountName,"a")) == NULL){
        puts("Erro na abertura do arquivo de historico");
    }
	//abre o arquivo de historico da conta alvo e adiciona, se nao existir ele criar�
	if(accountTarget != 0){
    	if((file2 = fopen(accountTargetName,"a")) == NULL){
            puts("Erro na abertura do arquivo de historico");
        }
    }

    if(accountType == 1)//pega o tipo int da conta do cliente e faz uma string que entrar� no hst�rico
		strcpy(workingType,"conta corrente");
    else//os unicos valores que entrar�o como referencia alem do 1 sera 0 e 2, j� q se for 0 n�o ser� usada essa op��o, pra todos os casos � poupan�a
		strcpy(workingType,"conta poupan�a");

	switch(action)
		{
		case 1:
			fprintf(file,"%s - %d fez um saque de %lf reais na %s\n",asctime(date),accountNumber,quantity,workingType);//o asctime converte o valor do date para uma string
			break;
		case 2:
			fprintf(file,"%s - %d fez um deposito de %lf reais na %s\n",asctime(date),accountNumber,quantity,workingType);
			break;
		case 3://j� que as informa��es da conta alvo s� ser�o necess�ria aqui, � melhor que as condicionais s� sejam feitas aqui dentro

			if(accountTargetType == 1)//pega o tipo int da conta do cliente e faz uma string que entrar� no hst�rico
				strcpy(targetType,"conta corrente");
			else//os unicos valores que entrar�o como referencia alem do 1 sera 0 e 2, j� q se for 0 n�o ser� usada essa op��o, pra todos os casos � poupan�a
				strcpy(targetType,"conta poupan�a");

			fprintf(file,"%s - %d fez uma transferencia de %lf reais de sua %s para a %s de %d\n",asctime(date),accountNumber,quantity,workingType,targetType,accountTarget);//esse � o texto mais complexo dos extratos j� que precisa mostrar qual � a conta que recebeu a transf�rencia e de que tipo ela �
			fprintf(file2,"%s - %d recebeu uma transferencia de %lf reais da %s para a %s de %d\n",asctime(date),accountTarget,quantity,workingType,targetType,accountNumber);
			break;
		}
	fclose(file);
	if(file2 != NULL)
             fclose(file2);
}

/*
**************************************
*     fun��o para criar um menu      *
*    para o cliente escolher que     *
*         opera��es fazer            *
**************************************
*/
void menu(int account)
{
	int action=0,option;//come�a em zero para entrar no loop
	char redo='s';//come�a em s para entrar no loop

	while(redo == 's')
	{
	    //Reseta action
	    action = 0;
	    //Escolhe a a��o desejada
		while(action<1 || action>4)
		{
			system("cls");
			printf("Menu:\n1 - Saque\n2 - Deposito\n3 - Transferencia\n4 - Extrato\n");
			scanf("%d",&action);
		}
        
        if(action != 4)
		          option = trans_savings();//guarda a op��o de conta escolhida pelo usuario

		switch(action)
		{
		case 1:
			withdraw(option,account);//saque com a op��o escolhida pelo usuario da conta ou da poupan�a
			break;
		case 2:
			deposit(option,account);//dep�sito com a op��o escolhida pelo usuario da conta ou da poupan�a
			break;
		case 3:
			transfer(option,account);//transferencia com a op��o escolhida pelo usuario da conta ou da poupan�a para conta ou popupan�a
			break;
		case 4:
			get_history(account);//extrato com todo o hist�rico da conta
			break;
		}
		printf("Quer executar outra operacao? s/n\n");
		fflush(stdin);
		scanf("%c",&redo);//se s for digitado, o menu abrir� novamaente permitindo outra opera��o, se for digitado n encerrar� o sistema

		while(redo!='s' && redo!='n')//se nao for sigitado nenhum valor v�lido ele continuar� pedindo at� ser digitado um valor v�lido
		{
			printf("Opcao invalida, por favor digite 's' ou 'n'\n");
			fflush(stdin);
			scanf("%c",&redo);
		}
	}
}

/*
**************************************
*     fun��o para criar login do     *
*    usuario com sua conta e senha   *
**************************************
*/
int login()
{
	FILE *file;
	int i,accountnumber,j=-1;
	char password[20],seeManager='n';

	while(seeManager == 'n')
	{
		while(j==-1)//enquanto n�o achar uma conta v�lida o j n�o vai mudar e continuar� a perguntar a conta
		{
            accountnumber = 0;
			system("cls");
			printf("Login - Digite o numero da sua conta:\n");
			scanf("%d",&accountnumber);
			for(i=0;i<3;i++)
				if(acc[i].accountNumber == accountnumber)//checa se a conta existe no nosso banco
					j=i;//com isso eu guardo a posi��o no vetor onde a conta est�
		}
		while(acc[j].failedAttempts<3)
		{
            system("cls");
			printf("Login - Digite a sua senha:\n");
			fflush(stdin);
			gets(password);

			if(strcmp(password,acc[j].accountPass)!=0)//toda vez que errar adicionar� um ao contador do gerente
			{
				acc[j].failedAttempts++;
			}
			else
			{
				menu(accountnumber);//depois de ter acertado a conta e a senha, o menu ser� chamado
				exit(1);
			}

			if(acc[j].failedAttempts==3)//se o contador atingir 3 entrar� a op��o de chamar o gerente
			{
				printf("Voce excedeu o limite de tentativas. Gostaria de falar com o gerente? s/n\n");
				scanf("%c",&seeManager);// se a pessoa escolher n ela voltar� ao come�o do sistema para poder tentar com outra conta

				if(seeManager=='s')
				{
                    system("cls");
                    //depois de uma conversa com o gerente o n�mero de tentativas � zerado
					printf("Gerente: Ola senhor, esta tendo problemas com a sua senha? Sem problemas, o senhor eh um dos nossos\nclientes mais antigos entao eu permitirei que continue tentando\nAgradecemos a preferencia\n");
					acc[j].failedAttempts=0;
					getch();
				}
				else//se a pessoa escolheu nao, devemos gravar no arquivo que a conta ja tem 3 tentaivas
				{
					file = fopen("accounts.bin", "wb");//vou sobrescrever o arquivo antigo e trocar o numero de tentativas
					if(file == NULL)
						return 1;
					fwrite(acc, sizeof(account), 3, file);
					fclose(file);
					return 1;
				}
			}
		}
	}
	return 0;
}
