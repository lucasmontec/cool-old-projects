#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/*
  Exerc�cio:
            
  2) Existem 2 mochilas, uma com capacidade para at� 20 kilos e outra com
  capacidade para at� 10 kilos. O uso de mochilas pode facilitar o transporte
  de v�rios objetos de pequeno peso. Em um trem existem 5 vag�es com 10 objetos
  em cada um. O peso de cada objeto ser� sorteado pelo programa.
  O programa dever� dizer quais os objetos que ficar�o na primeira mochila,
  quais os que ficar�o na segunda mochila e quais ficar�o de fora de forma
  que as mochilas carreguem o maior n�mero de objetos.
*/

typedef struct mch{
        float peso;
        int itens;
        float  pesoMax;     
} Mochila;

void randArray(float matrix[], int size, float min, float max);
void sort(float* array,int size);

float random(float min, float max){
     int v = 0;
     min *= 100;
     max *= 100;
     v = ((int)min) + (rand()%(( ((int)max+1) )-((int)min)));
     return ((float)v)/100.0f;
}

main(){
       
       srand(time(NULL));
       
       float itens[50];
       randArray(itens, 50, 0.3f, 5.0f);
       
       Mochila m1;
       Mochila m2;
       
       m1.pesoMax = 10.0f;
       m2.pesoMax = 20.0f;
       m1.itens = 0;
       m2.itens = 0;
       
       //Ordena os itens do menor para o maior
       sort(itens,50);
       
       //Passa os itens para as mochilas
       int i, itensFora=0;
       for(i=0;i<50;i++){
              //Coloca item na mochila 1
              if(m1.peso + itens[i] < m1.pesoMax){
                   printf("Colocando item %d na mochila 1: [%f]\n",i,itens[i]);
                   m1.peso += itens[i];
                   m1.itens++;
                   itens[i] = 0.0f;
              }else{
                    
                  //Coloca item na mochila 2
                  if(m2.peso + itens[i] < m2.pesoMax){
                       printf("\nColocando item %d na mochila 2: [%f]",i,itens[i]);
                       m2.peso += itens[i];
                       m2.itens++;
                       itens[i] = 0.0f;      
                  }else{
                        itensFora++;
                  }      
                  
              }
                   
       }
       
       //Diz quem foi e quem ficou de fora
       printf("\n\nNa mochila 1: %d itens, %f kg.\n",m1.itens,m1.peso);
       printf("Na mochila 2: %d itens, %f kg.\n",m2.itens,m2.peso);
       printf("%d itens ficaram fora.\n\n",itensFora);
       
       system("pause");
}

void sort(float* array,int size){
     int trocou = 1,i=0;
     float temp=0;
     while(trocou){
           trocou = 0;
           for(i=0;i<size-1;i++){
               if(array[i]>array[i+1]){
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    trocou=1;
               }
           }
     }
}

void randArray(float* arr, int size, float min, float max){
     int i;
     for(i=0;i<size;i++){
             arr[i] = random(min,max);            
     }  
}
