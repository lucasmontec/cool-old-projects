#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int getVal(char msg[], char errMsg[]);
int random(int min, int max);
void randMatrix(int** matrix,int m, int n, int min, int max);
void printmf(int** matrix,int m, int n );
int perfeito(int n);
int countPerfects(int** matrix,int m, int n);

/*
Exercicio:

1) Crie uma matriz MxN (valores definidos pelo usu�rio) que
tenha os valores preenchidos pelo programa (sorteio de valores entre 0 e 100)
e que imprima a quantidade de n�meros perfeitos na matriz.

*/

main(){ 
       int** matrix;
       int m,n,i;
       
       m = getVal("Digite o numero de linhas:","Pelomenos 1 linha:");
       n = getVal("Digite o numero de colunas:","Pelomenos 1 coluna:");
       
       matrix = (int**) calloc(m,sizeof(int*));
       for(i=0;i<m;i++){
             matrix[i] = (int*) calloc(n,sizeof(int));
       }
       
       if(matrix != NULL){
               randMatrix(matrix,m,n, 0, 100);
               printmf(matrix,m,n);
               printf("\nPerfeitos na matriz: %d\n\n",countPerfects(matrix,m,n));
               
       }
       
       system("pause");
       free(matrix);
}

int getVal(char msg[], char errMsg[]){
    int valor = 0;
    puts(msg);
    scanf("%d",&valor);
    while(valor<=0){
           puts(errMsg);
           scanf("%d",&valor);    
    }
    return valor;
}

int countPerfects(int** matrix,int m, int n){
    int count = 0;
    int i,j;
    for(i=0;i<m;i++){
           for(j=0;j<n;j++){
                     if(perfeito(matrix[i][j])){
                        count++;                          
                     }
           }             
    }
    return count;
}

void randMatrix(int** matrix,int m, int n, int min, int max){
     int i,j;
     for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                      matrix[i][j] = random(min,max);
            }             
     }  
}

void printmf(int** matrix,int m, int n ){
     int i,j;
     for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                      printf("%4d",matrix[i][j]);
            }         
            printf("\n");    
     }  
}

int perfeito(int n){
    int c = n-1, soma=0;
    if(c>0){
        while(c>0){
                if(!(n % c)){
                     soma += c;
                }
                c--;
        }
        if(soma == n){
             return 1;     
        }else{
              return 0;
        }
    }else{
          return -1;      
    }
}

int random(int min, int max){
     return min + (rand()%((max+1)-min));
}
