#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define TAMANHO 3

/*
    Exerc�cio:

    4) Fazer um programa que leia 10 nomes de pessoas (utilizando gets)
    e imprima a lista de nomes ordenada pelo sobrenome.
    A lista deve ter o formato: Sobrenome, I. (onde I. � a inicial do nome).
*/

void sortBySurname(char nomes[][50]);
int findSurnamePos(char* arr);
void printNome(char* nome);

main(){
       
       char nomes[TAMANHO][50];
       int i;
       
       for(i=0;i<TAMANHO;i++){
             printf("[%d]Digite o nome:",i+1);
             fflush(stdin);
             gets(nomes[i]);
             system("cls");
       }
       
       sortBySurname(nomes);
       system("cls");
       
       for(i=0;i<10;i++){
             printNome(nomes[i]);
       }
       
       system("pause");
}

void sortBySurname(char nomes[][50]){
     int trocou = 1,i=0;
     char temp[50];
     while(trocou){
           trocou = 0;
           for(i=0;i<TAMANHO-1;i++){
               //Compara nomes
               if(nomes[i][findSurnamePos(nomes[i])] > nomes[i+1][findSurnamePos(nomes[i+1])]){
                    strcpy(temp,nomes[i]);
                    strcpy(nomes[i],nomes[i+1]);
                    strcpy(nomes[i+1],temp);
                    trocou=1;
               }
           }
     }
}

int findSurnamePos(char* nome){
    int pos = 0;
    while(*nome != ' '){nome++;pos++;}
    pos++;
    return pos;
}

void printNome(char *nome){
     char INICIAL = nome[0];
     char SOBRENOME[25];
     int i=0;
     
     //Encontra o sobrenome
     while(*nome != ' '){nome++;}
     
     while(*nome != '\0'){
            SOBRENOME[i] = *nome;
            nome++;
            i++;
     }
     
     printf("%s, %c.",SOBRENOME,INICIAL);
}
