#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/*
    Exerc�cio:
           
    3) Uma lista de contatos armazena informa��es como nome, endere�o e telefone de v�rias pessoas. A lista est� desorganizada.
    Crie duas agendas com os dados da lista de contatos: uma orgaizada
    pelos nomes das pessoas e outra pelos endere�os.
*/

typedef struct inf{
      char nome[50];
      char endereco[50];
      int telefone;  
}contato;

void sortByName(contato* array,int size);
int recebeInt(char msg[]);
void arrcpy(contato* dst, contato* org,int size);
void printContato(contato c);

main(){
       
       int num_contatos = 0, i;
       puts("Digite quantos contatos voce vai adicionar na lista:");
       scanf("%d", &num_contatos);
       
       contato* lista = (contato*) calloc(num_contatos,sizeof(contato));
       contato* agenda1 = (contato*) calloc(num_contatos,sizeof(contato));
       contato* agenda2 = (contato*) calloc(num_contatos,sizeof(contato));
       
       if(lista != NULL){
             for(i=0;i<num_contatos;i++){
                 puts("Digite o nome do contato:");
                 fflush(stdin);
                 gets(lista[i].nome);
                 
                 puts("Digite o endereco do contato:");
                 fflush(stdin);
                 gets(lista[i].endereco);
                 
                 lista[i].telefone = recebeInt("Digite o telefone(somente numeros):");
                 system("cls");
             }
             
             //Mostra a lista
             for(int i=0;i<num_contatos;i++){
                      printContato(lista[i]);
             }
             
       }else{
             puts("Nao ha memoria suficiente!");      
       }
       
       //Monta a agenda 1
       if(agenda1 != NULL){
                arrcpy(agenda1, lista, num_contatos);
                sortByName(agenda1,num_contatos);
                
                puts("Agenda-1-------------------");
                
                for(int i=0;i<num_contatos;i++){
                      printContato(agenda1[i]);
                }
       }
       
       //Monta a agenda 2
       if(agenda2 != NULL){
                arrcpy(agenda2, lista, num_contatos);
                sortByAddr(agenda2,num_contatos);
                
                puts("Agenda-2-------------------");
                
                for(int i=0;i<num_contatos;i++){
                      printContato(agenda1[i]);
                }
       }
       
       system("pause");
       free(lista);
       free(agenda1);
       free(agenda2);
}

void arrcpy(contato* dst, contato* org,int size){
     int i;
     for(i=0;i<size;i++){
             strcpy(dst[i].nome, org[i].nome);
             strcpy(dst[i].endereco, org[i].endereco);
             dst[i].telefone = org[i].telefone;
     }
}

void printContato(contato c){
     printf("Contato %s:\n\t-Endereco: %s\n\t-Telefone: %d\n",c.nome,c.endereco,c.telefone);
}

void sortByName(contato* array,int size){
     int trocou = 1,i=0;
     char temp[50];
     while(trocou){
           trocou = 0;
           for(i=0;i<size-1;i++){
               if(array[i].nome[0] > array[i+1].nome[0]){
                    strcpy(temp,array[i].nome);
                    strcpy(array[i].nome,array[i+1].nome);
                    strcpy(array[i+1].nome,temp);
                    trocou=1;
               }
           }
     }
}

void sortByAddr(contato* array,int size){
     int trocou = 1,i=0;
     char temp[50];
     while(trocou){
           trocou = 0;
           for(i=0;i<size-1;i++){
               if(array[i].endereco[0] > array[i+1].endereco[0]){
                    strcpy(temp,array[i].endereco);
                    strcpy(array[i].endereco,array[i+1].endereco);
                    strcpy(array[i+1].endereco,temp);
                    trocou=1;
               }
           }
     }
}

int recebeInt(char msg[]){
       int ret=0;
       puts(msg);
       fflush(stdin);
       scanf("%d",&ret);
       return ret;
}
