#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <time.h>

#define SIZE 10

main(){

       //Random seed
       srand(time(NULL));

       char charmatrix[SIZE][SIZE];
       int submatrix[SIZE][SIZE];
       int i,j;
       int px,py;
       int valid_pos = 0;
       int vertical = 0;
       const int DEBUG = 0;

       const char words[][15] = { "porco", "leite", "foguete", "bigorna", "monitor" };
       int stamped_words = 1;

       //Seta a matriz sub para zero
       for(i=0;i<SIZE;i++){
             for(j=0;j<SIZE;j++){
                   submatrix[i][j] = 0;
             }
       }

       //Inicializacao da matriz
       while(stamped_words < 6){
               //Pega informacoes da palavra
               int sz = strlen(words[stamped_words-1]);

               //Pega uma posicao para a palavra
               px=0;
               py=0;
               valid_pos=0;
               while(valid_pos == 0){
                    valid_pos = 1;
                    px = rand()%(SIZE);
                    py = rand()%(SIZE);
                    vertical = rand()%2;

                    //Debug
                    if(DEBUG){
                              printf("Trying at [%d,%d]\n",px,py);
                    }

                    //Checa se � valida
                    if((vertical==0 && px+sz < SIZE-1) || (vertical==1 && py+sz < SIZE-1)){
                        if(px >= 0 && py >= 0){
                             if(vertical==1){
                                  for(i=0;i<sz;i++){
                                        if(submatrix[px][py+i] == 1 && words[stamped_words-1][i] != charmatrix[px][py+i]){
                                             valid_pos = 0;
                                        }
                                  }
                             }else{
                                  for(i=0;i<sz;i++){
                                        if(submatrix[px+i][py] == 1 && words[stamped_words-1][i] != charmatrix[px+i][py]){
                                             valid_pos = 0;
                                        }
                                  }
                             }
                        }else{
                              valid_pos = 0;
                        }
                    }else{
                          valid_pos = 0;
                    }
               }

               //Debug
               if(DEBUG){
                         printf("Placing: %s at [%d,%d]\n",words[stamped_words-1],px,py);
               }

               //Encontrada uma posicao valida, aplica a palavra
               if(vertical == 1){
                      for(i=0;i<sz;i++){
                              charmatrix[px][py+i] = words[stamped_words-1][i];
                              submatrix[px][py+i] = 1;
                      }
               }else{
                      for(i=0;i<sz;i++){
                              charmatrix[px+i][py] = words[stamped_words-1][i];
                              submatrix[px+i][py] = 1;
                      }
               }
               stamped_words++;
       }

       //Preenche as letras aleatorias
       for(i=0;i<SIZE;i++){
             for(j=0;j<SIZE;j++){
                   if(submatrix[i][j] != 1){
                        if(!DEBUG)
                        charmatrix[i][j] = 97 + rand()%26;
                        else
                        charmatrix[i][j] = (char)177;
                   }
             }
       }

       //Mostra o jogo
       for(i=0;i<SIZE;i++){
             for(j=0;j<SIZE;j++){
                   printf("%2c",charmatrix[i][j]);
             }
             printf("\n");
       }
       getch();

}
