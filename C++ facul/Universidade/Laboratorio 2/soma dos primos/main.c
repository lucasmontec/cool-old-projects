#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, roda=2, primo, soma = 0;
    for(i=90;i<1478;i++){
        primo = 1;
        for(roda = 2;roda <= (i/2);roda++){
            if((i%roda) == 0){
                primo = 0;
            }
        }

        if(primo == 1){
            soma += i;
        }

    }
    printf("A soma e: %d",soma);
    return 0;
}
