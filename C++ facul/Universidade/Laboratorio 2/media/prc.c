#include <stdio.h>

int media(int* vec, int t);

int main(){
    int i, k=10;
    int vec[k];

    for(i=0;i<k;i++){
        printf("Digite o valor[%d]:",i+1);
        scanf("%d",&vec[i]);
        printf("\n");
    }
    printf("Media:%d",media(vec,k));
    return 0;
}

int media(int* vec, int t){
    int c=0,i;
    for(i=0;i<t;i++){
       c += vec[i];
    }
    return c/t;
}
