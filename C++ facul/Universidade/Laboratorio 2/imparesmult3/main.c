#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, soma=0;
    for(i=1;i<500;i++){
        if(i % 3 == 0 && i % 2 != 0){
            soma += i;
        }
    }
    printf("Valor da soma dos impares multiplos de 3: %d",soma);
    return 0;
}
