#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

char* captura(char *buffer, int tkid, char token);

main(){
    puts(captura("nam;lol;total;",1,';'));
    puts(captura("name;lol;total;",2,';'));
    puts(captura("name;lol;total;",3,';'));
    puts(captura("name;lol;total;roma;",3,';'));
    puts(captura("wefqwr;lol;total;",1,';'));
    puts(captura("name;lol;total;vida;irado;mano;",5,';'));
    puts(captura("name;lol;total;debug;",4,';'));
    getch();
    return 0;
}

char* captura(char *buffer, int tkid, char token){

    int token_counter=0, start=0, i=0, sz=0;
    char *output;
    char *initialpos;

    initialpos = buffer;

    while(token_counter < tkid && *buffer != '\0'){
        
        if(token_counter == (tkid-1) && *buffer != token){
             sz++;
        }
        
        if(*buffer == token)
            token_counter++;
        
        if(token_counter == (tkid) && *buffer == token){
             start = i-sz;
        }else{
             start = 0;      
        }

        buffer++;
        i++;
    }
   
    buffer = initialpos;
   
    output = (char*) calloc(sizeof(char),sz);
    //printf("Start point: %d Size: %d\n",start,sz);
    for(i=0;i<sz;i++){
        output[i] = buffer[start+i];
    }

    return output;
}
