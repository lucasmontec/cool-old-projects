#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

//Declaracoes
typedef struct est{
        float nota;
        int RA;
}aluno;

aluno *alunos;
int opcao, n_alunos;

//Prototipos
int printMenu();
void PlotaAlunos();
void plotGraph(int nal, aluno* alunos);
aluno* alocaAlunos(int amt);
void ConsultaAlunos();
void limpaTela();

//Codigo
int main(){
       int escolha = 0;
      
       while(escolha != 3){
           escolha = printMenu();
           
           if(escolha == 1)
                      PlotaAlunos();
           if(escolha == 2)
                      ConsultaAlunos();
       }
       
       return 0;       
}

int printMenu(){
     int opcao=0;
     limpaTela();
     puts("***Grafico das notas dos alunos***");
     puts("* 1-Plotar notas                 *");
     puts("* 2-Consultar nota               *");
     puts("* 3-Sair                         *");
     puts("**********************************");
     scanf("%d",&opcao);
     limpaTela();
     return opcao;
}

void ConsultaAlunos(){
     limpaTela();
     if(alunos != NULL){
             //Recebe o RA para saber a nota
             int RA = 0, loc = 0;
             printf("Digite o RA para buscar a nota:\n");
             scanf("%d",&RA);
             
             //Localiza a nota
             system("color a");
             int i;
             for(i=0;i<n_alunos;i++){
                   if(RA == alunos[i].RA){
                        printf("Nota: %f",alunos[i].nota);
                        loc++;
                   }
             }
             if(loc == 0){
                    system("color c");
                    printf("Aluno nao localizado!\n");
             }
             getch();
             system("cls");
             system("color 0");
     }else{
           system("color c");
           printf("Nenhum aluno registrado! Plote as notas primeiro.\n");
           getch();
           system("cls");
           system("color 0");
     }
}

void PlotaAlunos(){
     limpaTela();
     //Monta o vetor de alunos
     char n_alunos_char[500];
     puts("Quantos alunos voce vai plotar?");
     fflush(stdin);
     gets(n_alunos_char);
     n_alunos = atoi(n_alunos_char);
     alunos = alocaAlunos(n_alunos);
     
     //Recebe as notas para cada RA
     limpaTela();
     if(alunos != NULL){
         int i=0;
         for(i=0;i<n_alunos;i++){
               printf("[%d]Digite o RA:\n",i+1);
               scanf("%d",&alunos[i].RA);
               printf("[%d]Digite a nota:\n",i+1);
               scanf("%f",&alunos[i].nota);
               while(alunos[i].nota<0 || alunos[i].nota>10){
                        system("color c");
                        printf("[%d]Nota entre 0 e 10:\n",i+1);
                        scanf("%f",&alunos[i].nota);
               }
               system("cls");
         }
     }else{
           printf("Memoria insuficiente!");
           exit(1);      
     }
     
     plotGraph(n_alunos, alunos);
}

void plotGraph(int nal, aluno* alunos){
     limpaTela();
     int i, notas[10],j;
     //Monta um vetor com as notas de 0 a 10
     for(i=0;i<10;i++){
             notas[i] = 0;                  
     }
     for(i=0;i<nal;i++){
           notas[((int)alunos[i].nota)]++;
     }
     
     //Printa o vetor das notas
     for(i=0;i<10;i++){
           printf("[%d-%2d]",i,i+1);
           if(notas[i]>0){
               for(int j=0;j<notas[i];j++){
                     printf("-");
               }
           }else{
                 printf("*");
           }
           printf("\n");
     }
     system("pause");
     limpaTela();
}

aluno* alocaAlunos(int amt){
       return (aluno*) calloc(amt,sizeof(aluno));
}

void limpaTela(){
     system("cls");
     system("color 0");     
}
