#include <stdio.h>
#include <stdlib.h>

int perfeito(int n);

int main(){
    printf("E perfeito: %d, %d\n",28,perfeito(28));
    system("pause");
    return 0;
}

int perfeito(int n){
    int c = n-1, buffer=0;
    while(c > 0){
              if(!(n % c)){
                   buffer += c;
              }
              c--;      
    }
    
    if(buffer == n){
        return 1;      
    }else{
        return 0;
    }
       
}
