#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void clear_vector(int *vec, int size);

int main(){
       //random seeder
       srand(time(NULL));
    
       int produtos[12][10];
       int i,j;
       int mais_vendido_v[10], mais_vendido = 0, pior_mes = 0, melhor_do_mes[2];
       int mvb=0,pmb=0;
       int debug = 0;
       puts("******************************");
       puts("******Digital Bar Helper******");
       puts("******************************");
       puts("Deseja inicializar em modo de teste (1/0)?");
       scanf("%d",&debug);
       
       for(j=0;j<12;j++){
             if(debug == 0)
             printf(" -Sobre o mes [%d]:\n",(j+1));
             
             for(i=0;i<10;i++){
                      if(debug == 0){
                           printf(" -Digite quantos produtos de ID: [%d] foram vendidos:\n",(i+1));
                           scanf("%d",&produtos[j][i]);
                      }else{
                            produtos[j][i] = rand()%100;
                            printf("*Automatic* Mes: [%d] ID: [%d] Qtd: [%d]\n",(j+1),(i+1),produtos[j][i]);                            
                      }
             }
       }
       
       //pior mes
       for(j=0;j<12;j++){
           int soma = 0;
           for(i=0;i<10;i++){
                  soma += produtos[j][i];
           }
           if(j==0){
                pmb = soma;
           }
           if(soma < pmb){
                pmb = soma;
                pior_mes = j;
           }  
       }
       
       //melhor produto do ano
       clear_vector(mais_vendido_v, 10);
       int ss =0;
       for(j=0;j<12;j++){
           for(i=0;i<10;i++){
                  mais_vendido_v[i] += produtos[j][i];
           }
       }
       for(i=0;i<10;i++){
                         if(i==0){
                              mvb = mais_vendido_v[i];
                              mais_vendido = i;
                         }
                         if(mais_vendido_v[i] > mvb){
                              mvb = mais_vendido_v[i];
                              mais_vendido = i;
                         }
       }
       
       //melhor produto em um mes
       for(j=0;j<12;j++){
           int soma = 0;
           for(i=0;i<10;i++){
                  int k;
                  for(k=0;k<10;k++){
                       if(k!=i && produtos[j][k] > produtos[j][i]){
                               if(produtos[j][k] > melhor_do_mes[1]){
                                                 melhor_do_mes[0] = k;
                                                 melhor_do_mes[1] = produtos[j][k];
                               }
                       }else{
                             if(produtos[j][i] > melhor_do_mes[1]){
                                               melhor_do_mes[0] = i;
                                               melhor_do_mes[1] = produtos[j][i];
                             }
                       }
                  }   
           }
           
       }
       
       puts("******************************");
       printf("*O produto mais vendido do ano foi o de ID: %d*\n",mais_vendido);
       printf("*O mes de pior venda foi o mes: %d*\n",pior_mes);
       printf("*O produto com melhor venda em um mes foi o de ID: %d*\n",melhor_do_mes[0]);
       puts("******************************");
       system("PAUSE");
}

void clear_vector(int *vec, int size){
     int i;
     for(i=0;i<size;i++){
         vec[i] = 0;
     }
}
