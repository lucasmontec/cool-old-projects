#include <stdio.h>
#include <stdlib.h>

int menor(int a, int b);
int maior(int a, int b);
void sort(int *v, int tam, int (*func)(int , int));
void troca(int *a, int *b);

main(){
    int v[5] = {4,10,34,3,1};
    sort(v,5,menor);

    int i;
    for(i=0;i<5;i++){
        printf("%4d",v[i]);
    }
}

int menor(int a, int b){
    return (a>b?0:1);
}

int maior(int a, int b){
    return (a>b?1:0);
}

void sort(int *v, int tam, int (*func)(int , int)){
    int i,j;
    for(i=0;i<tam;i++){
        for(j=0;j<tam-1;j++){
            if(func(v[j],v[j+1])){
                troca(&v[j],&v[j+1]);
            }
        }
    }
}

void troca(int *a, int *b){
    int c = *a;
    *a = *b;
    *b = c;
}
