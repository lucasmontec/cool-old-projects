#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n=0,i=0, soma=0;
    printf("Digite o numero para checar se e perfeito.\n");
    scanf("%d",&n);

    for(i=1;i<n;i++){
        if((n % i) == 0){
            soma += i;
        }
    }

    if(soma == n){
        printf("O numero digitado, %d, e perfeito.\n",n);
    }else{
        printf("O numero digitado nao e perfeito.\n");
    }
    return 0;
}
