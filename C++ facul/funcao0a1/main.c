#include <stdio.h>
#include <stdlib.h>

int main()
{
    float x, y;
    float f;

    for(x=0;x<1;x += 0.1){
        for(y=-1;y<1;y+=0.1){
            if(((x*x*x)-(3*y)) != 0){
                f = ((x*x)+(2*y))/((x*x*x)-(3*y));
                printf(" x[%2f] y[%2f] : %lf \n",x,y,f);
            }else{
                printf(" x[%2f] y[%2f] : INDEF. \n",x,y);
            }

        }
    }
    return 0;
}
