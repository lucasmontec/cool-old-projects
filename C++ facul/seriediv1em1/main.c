#include <stdio.h>
#include <stdlib.h>

int main()
{
    const int TERMOS = 20;
    int i;
    double soma = 0.0f;
    for(i=1;i<=TERMOS;i++){
        soma += 1.0/i;
    }
    printf("Valor calculado (1 + 1/2 + 1/3 + 1/4...): %lf",soma);
    return 0;
}
