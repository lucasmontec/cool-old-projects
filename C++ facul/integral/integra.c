#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define RES 1000000

float y3x2(float x);
float yxx(float x);
float y2x(float x);
float ysinx(float x);
float integrate(float a, float b, int resolution, float (*fnc)(float x));
float integrateTrap(float a, float b, int resolution, float (*fnc)(float x));

main(){

    puts("Integrais de 2 a 3:");
    puts("");

    puts("y = 3x + 2");
    printf("ret: %f\n",integrate(2,3,RES,y3x2));
    printf("trap: %f\n",integrateTrap(2,3,RES,y3x2));

    puts("");
    puts("y = x*x");
    printf("ret: %f\n",integrate(2,3,RES,yxx));
    printf("trap: %f\n",integrateTrap(2,3,RES,yxx));

    puts("");
    puts("y = 2x");
    printf("ret: %f\n",integrate(2,3,RES,y2x));
    printf("trap: %f\n",integrateTrap(2,3,RES,y2x));

    puts("");
    puts("y = sin(x)");
    printf("ret: %f\n",integrate(2,3,RES,ysinx));
    printf("trap: %f\n",integrateTrap(2,3,RES,ysinx));
}

float ysinx(float x){
    return sin(x);
}

float y3x2(float x){
    return 3*x + 2;
}

float yxx(float x){
    return x*x;
}

float y2x(float x){
    return 2*x;
}

float integrate(float a, float b, int resolution, float (*fnc)(float x)){
    float area = 0.0f, dx = 0.0f;
    int i;
    dx = (b-a)/resolution;
    for(i=0;i<resolution-1;i++){
        area += fnc(a+(dx*i))*dx;
    }
    return area;
}

float integrateTrap(float a, float b, int resolution, float (*fnc)(float x)){
    float area = 0.0f, dx = 0.0f;
    int i;
    dx = (b-a)/resolution;
    for(i=0;i<resolution-1;i++){
        area += (fnc(a+(dx*i)) + fnc(a+(dx*(i+1))))*dx*0.5;
    }
    return area;
}
