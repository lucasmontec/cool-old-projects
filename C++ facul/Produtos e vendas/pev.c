#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct est{
        char nome[50];
        float preco;
        int ID;
}Produto;

void venda(int ID, int unid, int mes, int mat[5][12]);
int mostraMenu();
void cleanMatrix(int matrix[5][12]);
void regVenda(int mat[5][12]);
void mostraRelat(Produto p[5], int vend[5][12],char meses[12][20]);

main(){

    Produto produtos[5];
    int vendas[5][12];
    char meses[12][20] = {
        "janeiro",
        "fevereiro",
        "marco",
        "abril",
        "maio",
        "junho",
        "julho",
        "agosto",
        "setembro",
        "outubro",
        "novembro",
        "dezembro"
    };

    //Maca
    strcpy(produtos[0].nome,"maca");
    produtos[0].preco = 1.25f;

    //Banana
    strcpy(produtos[1].nome,"banana");
    produtos[1].preco = 50.0f;

    //Leite
    strcpy(produtos[2].nome,"leite");
    produtos[2].preco = 0.5f;

    //Cachorro
    strcpy(produtos[3].nome,"cachorro");
    produtos[3].preco = 3.1415f;

    //Jatof15
    strcpy(produtos[4].nome,"jato f16");
    produtos[4].preco = 2000.0f;

    int opcao;
    cleanMatrix(vendas);

    do{
        opcao = mostraMenu();

        //Realiza venda
        if(opcao == 1){
            regVenda(vendas);
        }

        //Mostra o relatorio
        if(opcao == 2){
            mostraRelat(produtos, vendas, meses);
        }
    }while(opcao != 3);
}

void cleanMatrix(int matrix[5][12]){
    int i,j;
    for(i=0;i<5;i++){
        for(j=0;j<12;j++){
            matrix[i][j]=0;
        }
    }
}

void mostraRelat(Produto p[5], int vend[5][12],char meses[12][20]){
    int i,j;
    system("cls");
    system("color 6");
    for(i=0;i<5;i++){
        printf("\n*Produto[%d] ( %s ):\n",i,p[i].nome);

        for(j=0;j<12;j++){
            printf("%s: ",meses[j]);
            printf("(%d, R$%6.4f)\n",vend[i][j],vend[i][j]*p[i].preco);
        }
    }
    puts("");
    system("pause");
}

int mostraMenu(){
    int op=0;
    do{
        system("cls");
        system("color 8");
        puts("****GERENCIADOR DE VENDAS****");
        puts("");
        puts("*Menu");
        puts("1- Registrar venda.");
        puts("2- Mostrar registro.");
        puts("3- Sair.");
        puts("Digite a opcao:");
        fflush(stdin);
        scanf("%d",&op);
    }while(op > 3 || op < 1);
    return op;
}

void regVenda(int mat[5][12]){
    int ID = 0, UND = 1,MES = 1;
    system("cls");
    system("color 5");
    puts("**Registro de vendas**");
    puts("");

    do{
        puts("");
        puts("Digite o mes(1-12):");
        scanf("%d",&MES);
    }while(MES < 1 || MES > 12);

    do{
        puts("");
        puts("Digite o produto pelo seu ID (1-5):");
        scanf("%d",&ID);
        ID--;
    }while(ID < 0 || ID > 4);

    do{
        puts("");
        puts("Digite quantas unidades foram vendidas:");
        scanf("%d",&UND);
    }while(UND < 0);

    venda(ID,UND,MES,mat);
}

void venda(int ID, int unid, int mes, int mat[5][12]){
    mat[ID][mes] += unid;
}
