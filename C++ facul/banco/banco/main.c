#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Hello world!\n");
    return 0;
}

void get_history(int accountnumber){
    FILE *accountfile;
    char accountname[5];
    char line[100];
    itoa(accountnumber,accountname,5);

    //Tenta abrir a conta desejada
    if((accountfile = fopen(accountname,"rb")) != NULL){

        while(fgets( line, 100, accountfile ) != NULL){
            printf("%s",line);
        }
    }else{
        printf("Erro, Nao foi possivel localizar o arquivo de historico!\n");
    }
}

int withdraw(int action, int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    double amount = -1;

    //Procura a conta para fazer o saque
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!\n");
        return -1;
    }

    //Quanta grana vai sacar
    do{
        printf("Digite a quantidade de dinheiro a sacar: (0 para cancelar)\n");
        scanf("%lf",&amount);
    }while(amount < 0);

    if(amount == 0){
        printf("Operacao cancelada.\n");
        return 0;
    }

    //Confere se existe grana suficiente para sacar
    if(acctype == 1){ //Conta poupan�a
        if(acc[accountindex].savingsAcc_Balance - amount < 0){
            printf("Nao ha fundos para a operacao!\n");
            return -1;
        }else{
            //Realiza o saque
            acc[accountindex].savingsAcc_Balance -= amount;
            add_action(accountnumber,1,acctype,amount,0,0);
            return 1;
        }
    }else if(acctype == 2){ //Conta corrente
        if(acc[accountindex].transAcc_Balance - amount < 0){
            printf("Nao ha fundos para a operacao!\n");
            return -1;
        }else{
            //Realiza o saque
            acc[accountindex].transAcc_Balance -= amount;
            add_action(accountnumber,1,acctype,amount,0,0);
            return 1;
        }
    }
}

int deposit(int action, int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    double amount = -1;

    //Procura a conta para fazer o deposito
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!\n");
        return -1;
    }

    //Quanta grana vai depositar
    do{
        printf("Digite a quantidade de dinheiro a sacar: (0 para cancelar)\n");
        scanf("%lf",&amount);
    }while(amount < 0);

    if(amount == 0){
        printf("Operacao cancelada.\n");
        return 0;
    }

    //Seleciona a conta
    if(acctype == 1){ //Conta poupan�a
        acc[accountindex].savingsAcc_Balance += amount;
        add_action(accountnumber,2,acctype,amount,0,0);//Salva no hist�rico
        return 1;
    }else if(acctype == 2){ //Conta corrente
        acc[accountindex].transAcc_Balance += amount;
        add_action(accountnumber,2,acctype,amount,0,0);//Salva no hist�rico
        return 1;
    }
}

int transfer(int action, int acctype, int accountnumber){
    //retorna 1 para sucesso, 0 para cancelamento, -1 para erro
    int accountindex = -1,i;
    int targetaccindex = -1;
    int targetaccnumber = -1;
    double amount = -1;

    //Procura a conta para fazer a tranferencia
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == accountnumber)
            accountindex = i;
    }

    //Erro na localiza��o da conta!
    if(accountindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!");
        return -1;
    }

    //Recebe e procura conta alvo
    do{
        printf("Digite o numero da conta para receber a transferencia: (0 para cancelar)\n");
        scanf("%d",&targetaccnumber);
    }while(targetaccnumber < 0);

    if(targetaccnumber == 0){
        printf("Operacao cancelada.\n");
        return 0;
    }

    //Localiza conta alvo
    for(i=0;i<3;i++){
        if(acc[i].accountNumber == targetaccnumber)
            targetaccindex = i;
    }

    //Erro na localiza��o da conta!
    if(targetaccindex == -1){
        printf("Erro! Nao foi possivel localizar a conta!");
        return -1;
    }

    //Quanta grana vai tranferir
    do{
        printf("Digite a quantidade de dinheiro a transferir: (0 para cancelar)\n");
        scanf("%lf",&amount);
    }while(amount < 0);

    if(amount == 0){
        printf("Operacao cancelada.");
        return 0;
    }

    //Seleciona a conta
    if(acctype == 1){ //Conta poupan�a
        //Checa se h� dinheiro para transferir
        if(acc[accountindex].transAcc_Balance - amount < 0){
            printf("Fundo insuficiente para a operacao.\n");
            return -1;
        }

        //Transfere
        acc[targetaccindex].savingsAcc_Balance += amount;
        acc[accountindex].savingsAcc_Balance -= amount;
        add_action(accountnumber,3,acctype,amount,targetaccnumber,acctype);//Salva no hist�rico
        //add_action(targetaccnumber,3,acctype,amount,0,0);//Registra o recebimento de uma transferencia
        return 1;
    }else if(acctype == 2){ //Conta corrente
        //Checa se h� dinheiro para transferir
        if(acc[accountindex].transAcc_Balance - amount < 0){
            printf("Fundo insuficiente para a operacao.\n");
            return -1;
        }

        //Transfere
        acc[targetaccindex].transAcc_Balance += amount;
        acc[accountindex].transAcc_Balance -= amount;
        add_action(accountnumber,3,acctype,amount,targetaccnumber,acctype);//Salva no hist�rico
        //add_action(targetaccnumber,3,acctype,amount,0,0);//Registra o recebimento de uma transferencia
        return 1;
    }
}
