/* asctime example */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void update_daybar(char daybar[24], struct tm * time);
void print_daybar(char daybar[24], struct tm * time);

char blank = ' ';
char complete = (char)178;
char line = (char)205;
char upper_left = (char)201;
char upper_right = (char)187;
char lower_left = (char)200;
char lower_right = (char)188;

int main ()
{
  time_t rawtime;
  struct tm * timeinfo;
  int lastsec = 0;
  char your_day[24];

  system("color a");
  system("echo off");
  system("cls");

  while(1){
      time ( &rawtime );
      timeinfo = localtime ( &rawtime );

      if(lastsec != timeinfo->tm_sec)
        print_daybar(your_day,timeinfo);

      lastsec = timeinfo->tm_sec;
  }

  return 0;
}

void update_daybar(char daybar[24], struct tm * time){
    int i;
    for(i=0;i<24;i++){
        if(i < (time->tm_hour-1)){
            daybar[i] = complete;
        }else{
            daybar[i] = blank;
        }
    }
}

void print_daybar(char daybar[24], struct tm * time){
    update_daybar(daybar, time);
    system("cls");
    printf ( "Your day: [%2.3f%%]\n",  100.0*(float((time->tm_hour*3600+time->tm_min*60)+time->tm_sec)/86400.0) );
    int step,i;
    for(step=0;step<3;step++){
        for(i=0;i<26;i++){
            //Upper bar
            if(step == 0){
                if(i==0){
                    printf("%c",upper_left);
                }
                if(i==25){
                    printf("%c\n",upper_right);
                }
                if(i != 0 && i != 25){
                    printf("%c",line);
                }
            }

            //Day bar
            if(step == 1 && i > 0 && i < 25)
                printf("%c",daybar[i-2]);
            else if(step == 1)
                printf("%c",(char)186);

            //Lower bar
            if(step == 2){
                if(i==0){
                    printf("\n%c",lower_left);
                }
                if(i==25){
                    printf("%c\n",lower_right);
                }
                if(i != 0 && i != 25){
                    printf("%c",line);
                }
            }
        }
    }

    printf ( "Only: [%d] seconds left!\n", 86400 - ((time->tm_hour*3600+time->tm_min*60)+time->tm_sec) );
}
