#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

main(){
 //Random
 srand(time(NULL));

 //Alocacao dinamica
 int i = 0;
 int tx = rand()%8 + 3;
 int ty = rand()%8 + 3;

 //Aloca
 int **m1;
 int **m2;
 int **m3;

 m1 = (int**) calloc(tx,sizeof(int*));
 m2 = (int**) calloc(tx,sizeof(int*));
 m3 = (int**) calloc(tx,sizeof(int*));

 for(i=0;i<tx;i++)
    m1[i] = (int*) calloc(ty,sizeof(int));
 for(i=0;i<tx;i++)
    m2[i] = (int*) calloc(ty,sizeof(int));
 for(i=0;i<tx;i++)
    m3[i] = (int*) calloc(ty,sizeof(int));

  //Numeros pras matrizes
  int x,y;
  for(x=0;x<tx;x++){
    for(y=0;y<ty;y++){
        m1[x][y] = rand()%6;
        printf("%3d",m1[x][y]);
    }
    printf("\n");
  }
  printf("\n");

  for(x=0;x<tx;x++){
    for(y=0;y<ty;y++){
        m2[x][y] = rand()%6;
        printf("%3d",m2[x][y]);
    }
    printf("\n");
  }
  printf("\n");

  for(x=0;x<tx;x++){
    for(y=0;y<ty;y++){
        if(m1[x][y] == m2[x][y]){
            m3[x][y] = m1[x][y];
        }else{
            m3[x][y] = 0;
        }
        printf("%3d",m3[x][y]);
    }
    printf("\n");
  }

  system("pause");


  //Free the world
  for(i=0;i<tx;i++)
    free(m1[i]);
  for(i=0;i<tx;i++)
    free(m2[i]);
  for(i=0;i<tx;i++)
    free(m3[i]);

  free(m1);
  free(m2);
  free(m3);

  return 0;
}
