#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Sistema de logins

int main(){

    //Seta os logins do sistema
    char logins[10][2][50] = {
        {
            "admin",
            "123456"
        },{
            "roberto",
            "abc341"
        },{
            "pedro",
            "roar"
        },{
            "lucas",
            "123"
        },{
            "batata",
            "potato"
        },{
            "seis",
            "orgy"
        },{
            "trololo",
            "ahahaha"
        },{
            "ocho",
            "nueve"
        },{
            "nueve",
            "urss"
        },{
            "cccp",
            "free"
        }
    };

    //Variaveis de controle
    int i, l_exists=0, s_exists=0;
    char login[50],senha[50];

    puts("* CIA information systems *");
    printf("Login>");
    gets(login);

    //Checa os logins
    for(i=0;i<10;i++){
        if(strcmp(logins[i][0],login)==0){
            l_exists = 1;
        }
    }

    //Pede a senha
    if(l_exists){
        printf("Password>");
        gets(senha);

        for(i=0;i<10;i++){
            if(strcmp(logins[i][1],senha)==0){
                s_exists = 1;
            }
        }
    }else{
        puts("");
        puts("Invalid login!");
        system("pause");
        exit(0);
    }

    if(l_exists == 1 && s_exists == 1){
        puts("");
        puts("Login information confirmed!");
        puts("Acessing....");
        system("pause");
        exit(0);
    }else{
        puts("");
        puts("Invalid password!");
        puts("Erasing your harddrive!");
        system("pause");
        exit(0);
    }
}
