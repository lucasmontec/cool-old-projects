#include <stdio.h>
#include <stdlib.h>

int* menormaior(int mat[5][5]);

main(){
    int matrix[5][5] = {
        {10,6,100,436,45},
        {34,34,65,34,535},
        {234,43,978,234,3},
        {235,54,86,5234,546},
        {2457,245,3,65,3}
    };

    int *vals = menormaior(matrix);
    printf("menor %d, maior %d",vals[1],vals[0]);

}

int* menormaior(int mat[5][5]){
    int resp[2];
    int i,j;

    for(i=0;i<5;i++){
        for(j=0;j<5;j++){

            //Define o menor e o maior inicialmente
            if(i==0 && j==0){
                resp[0] = mat[0][0];
                resp[1] = mat[0][0];
            }


            //Pega o maior
            if(mat[i][j] > resp[0]){
                resp[0] = mat[i][j];
            }

            //Pega o menor
            if(mat[i][j] < resp[1]){
                resp[1] = mat[i][j];
            }
        }
    }

    return resp;
}
