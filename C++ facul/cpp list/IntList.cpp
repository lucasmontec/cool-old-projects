#include "IntList.h"

#include <iostream>
using namespace std;

IntList::IntList(): currIndex(0)
{
    list = new int[10];
    lenght = 10;
}

IntList::IntList(int len): currIndex(0)
{
    lenght = len;
    list = new int[len];
}

IntList::~IntList()
{
    delete [] list;
}

IntList::IntList(const IntList& other): lenght(other.lenght)
{
    list = new int[lenght];

    for(int i=0;i<lenght;i++)
        list[i] = other.list[i];
}

IntList& IntList::operator=(const IntList& rhs)
{
    if (this == &rhs) return *this; // handle self assignment

    if(lenght != rhs.lenght){
        lenght = rhs.lenght;
        delete [] list;
        list = new int[lenght];
    }

    for(int i=0;i<lenght;i++){
        list[i] = rhs.list[i];
    }

    return *this;
}

IntList IntList::operator+(const IntList& entry){
    int totalSize = lenght + entry.lenght;
    IntList exit(totalSize);

    //Copy the current values
    for(int i=0;i<lenght;i++){
        exit.list[i] = list[i];
    }

    //Copy the entry values
    for(int i=lenght;i<totalSize;i++){
        exit.list[i] = entry.list[i-lenght];
    }

    return exit;
}

IntList& IntList::operator+=(const IntList& entry){
    *this = (*this + entry);

    return *this;
}

IntList& IntList::operator+=(int a){
    if(currIndex < lenght)
        list[currIndex++] = a;

    return *this;
}

void IntList::resetLocation(){
    currIndex = 0;
}

void IntList::clear(){
    for(int i=0;i<lenght;i++){
        list[i] = 0;
    }
    resetLocation();
}

void IntList::clearResize(int newSize){
    lenght = newSize;
    delete [] list;
    list = new int[lenght];

    clear();
}

ostream& operator<<(ostream& output, IntList& thelist){
    output << "{ ";
    for(int i=0;i<thelist.lenght;i++){
        output << thelist.list[i];
        if(i < thelist.lenght-1)
            output << ", ";
    }
    output << " }";

    return output;
}
