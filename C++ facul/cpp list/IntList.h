#ifndef INTLIST_H
#define INTLIST_H

#include <iostream>
using namespace std;

class IntList
{
    friend ostream &operator<<(ostream &, IntList &);
    public:
        IntList();
        IntList(int);
        ~IntList();
        IntList(const IntList& other);
        IntList& operator=(const IntList& other);
        IntList operator+(const IntList& other);
        IntList& operator+=(const IntList& other);
        IntList& operator+=(int);
        void resetLocation();
        void clear();
        void clearResize(int);
    protected:
    private:
        int *list;
        int currIndex;
        int lenght;
};

#endif // INTLIST_H
