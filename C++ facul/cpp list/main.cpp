#include <iostream>
#include "IntList.h"

using namespace std;

int main()
{
    IntList list(10);
    list += 1;
    list += 5;
    list += -10;

    cout << "First list: " << list << endl;

    IntList list2(list);

    cout << "Copied list: " << list2 << endl;

    IntList list3;

    list3 = list + list2;

    cout << "Addition of lists (and assignment): " << list3 << endl;
    return 0;
}
