#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a=0,b=0,c=0;
    int na,nb,nc;
    printf("Digite o primeiro valor: \n");
    scanf("%d",&a);
    printf("Digite o segundo valor: \n");
    scanf("%d",&b);
    printf("Digite o terceiro valor: \n");
    scanf("%d",&c);

    if(a > b && a > c){
        na = a;
        if(b > c){
            nb = b;
            nc = c;
        }else{
            nb = c;
            nc = b;
        }
    }else if(b > a && b > c){
        na = b;
        if(a > c){
            nb = a;
            nc = c;
        }else{
            nb = c;
            nc = a;
        }
    }else{
        na = c;
        if(a > b){
            nb = a;
            nc = b;
        }else{
            nb = b;
            nc = a;
        }
    }

    printf("Sequencia: [%d][%d][%d]",nc,nb,na);

    return 0;
}
