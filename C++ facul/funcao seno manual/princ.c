#include <stdio.h>
#include <math.h>
#define PI 3.141516

double fatorial(double n)
{
    if (n <= 1){
        return 1;
    }else{
        return  n * fatorial(n-1);
    }
}

double seno(double ang, int iterations){
    double a = ang*(PI/180);
    double sen = 0;
    int i,alternante=-1,n=0;

    for(i=0;i<iterations;i++){
        n = (2*i)+1;
        sen += pow(alternante,i)*(pow(a,n)/fatorial(n));
    }

    return sen;
}

int main(){
    printf("Sin 30: %lf\nSistema: %lf",seno(60,100),sin(60*(PI/180)));
    return 0;
}
