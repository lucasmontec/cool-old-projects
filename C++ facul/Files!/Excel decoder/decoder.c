#include <stdio.h>
#include <stdlib.h>

char* captura(char *buffer, int loc, char token);

main(){
    puts(captura("name;lol;total;",1,';'));
    puts(captura("name;lol;total;",2,';'));
    puts(captura("name;lol;total;",3,';'));

    return 0;
}

char* captura(char *buffer, int loc, char token){

    int sz=0, token_counter=1, start=0, i;
    char *output;

    while(buffer[i] != '\0'){
        if(buffer[i] == token)
            token_counter++;

        if(loc == token_counter && buffer[i] != token){
            sz++;
        }

        if(buffer[i] == token && token_counter == loc)
            start = i+1;

        i++;
    }

    output = (char*) calloc(sizeof(char),sz);

    for(i=0;i<sz;i++){
        output[i] = buffer[start+i];
    }

    return output;
}
