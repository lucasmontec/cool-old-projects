/*
  Name: programa notepad CMD
  Copyright: none
  Author: Lucas M.C.
  Date: 26/12/07 20:19
  Description: like a note pad
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

FILE *p;
char blocker, formato[10], texto[200], nome[50], w, s;
int i;
char escolha;

int menu()
{
    do {
    puts ("\nEscolha oque deseja fazer o digite a primeira letra da funcao...\n\n");
    puts ("Fechar (F)");
    puts ("Inserir/Salvar (I)");
    puts ("Abrir (A)");
   scanf("%c",&s);
} while (s != 'f' && s != 'i' && s != 'a');
return(s);
}

void abrir()
{
    if (!(p = fopen(nome,"r")))
    {
            printf("ERROR! FECHANDO...\a");
            exit(1);
    }
    while(!feof(p))
    {
    w = getc(p);
    printf("%c",w);
    }
    menu();
    blocker = getch();
    fclose(p);
}

void salvar()
{
if (!(p = fopen(nome,"a+")))
    {
            printf("ERROR! FECHANDO...\a");
            exit(1);
    }
printf ("Escreva o texto a ser gravado:\n");
scanf("%s",&texto);
for (i=0; texto[i]; i++)
{
putc(texto[i],p);
}
fclose(p);
}

int saida()
{
    exit(1);
}

int main()
{
    printf ("---Bem vindo ao notepad CMD criado por Lucas M.C---\a\n");
    printf ("Digite o nome do arquivo a ser trabalhado:\n");
    gets(nome);
    printf ("Digite o formato do arquivo a ser aberto:\n");
    gets(formato);
    strcat(nome, formato);
    escolha = menu();
     for(;;)
    {
           switch (escolha) {
           case 'f':
           saida();
             break;
           case 'i':
           salvar();
             break;
           case 'a':
           abrir();
    }
}
}
