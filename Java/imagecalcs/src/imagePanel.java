import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

class ImagePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	boolean autoSize;
	String text = "";
	
	//constructors
	public ImagePanel(){
		autoSize = true;
	}
	
	public ImagePanel(boolean autosize){
		autoSize = autosize;
	}
	
	public BufferedImage img = null;
	public BufferedImage bgimg = null;
	
	public void setText(String txt){
		text = txt;
	}
	
	public void setImage(BufferedImage img)
	{
		this.img = img;
	    if(autoSize && img != null && (this.getWidth() != img.getWidth() || this.getHeight() != img.getHeight())){
			Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		    setPreferredSize(size);
	    }
		this.repaint();
		this.revalidate();
	}

	public void setBgImage(BufferedImage img)
	{
		this.bgimg = img;
	}
	
	public void paintComponent(Graphics g)
	{
		if (img != null)
		{
			if(bgimg != null){
				g.drawImage(bgimg, 0, 0, this.getWidth(), this.getHeight(), this);
			}
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
			g.setColor(Color.white);
			if(text != ""){
				g.drawString(text, 5, 15);
			}
			//g.drawString("Size: ["+this.getWidth()+"/"+this.getHeight()+"]", 5, 15);
		}else{
			g.setColor(Color.black);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			g.setColor(Color.white);
			g.drawLine(1, 1, this.getWidth()-1, 1);
			g.drawLine(1, 1, 1, this.getHeight()-1);
			g.drawLine(this.getWidth()-1, this.getHeight()-1, this.getWidth()-1, 1);
			g.drawLine(this.getWidth()-1, this.getHeight()-1, 1, this.getHeight()-1);
			g.drawLine(1, 1, this.getWidth(), this.getHeight());
			g.drawLine(1, this.getHeight(), this.getWidth(), 1);
		}
	}
}