import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class imageFrame extends JFrame{
	private static final long serialVersionUID = 1L;

	ImagePanel ip = new ImagePanel();
	
	public imageFrame(String name, BufferedImage img){
		super(name);
		this.setSize(img.getWidth(), img.getHeight());
		ip.setImage(img);
		this.add(ip);
		this.setVisible(true);
	}
	
}
