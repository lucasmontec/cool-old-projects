import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class imagecalcmain {
	
	//globals
	static BufferedImage bgi = null;
	static imageAnalyzer ia = new imageAnalyzer();
	static mediacore mc = new mediacore();
	static double th = 0.2;
	
	public static void main(String[] args) {
		//screen data
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = ge.getDefaultScreenDevice();
		DisplayMode dm = device.getDisplayMode();
		
		//webcam data
		final webcam wc = new webcam();
		
		//frame building
		JFrame frame = new JFrame("Image Calculation");
		frame.setCursor(Cursor.CROSSHAIR_CURSOR);
		frame.setLayout(new BorderLayout());
		frame.setSize(600,500);
		frame.setLocation((dm.getWidth()/2) - (frame.getWidth()/2), (dm.getHeight()/2) - (frame.getHeight()/2));
		frame.setVisible(true);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				wc.close();
				System.exit(0);
			}
		});
		
		//main image panel setup
		ImagePanel ip = new ImagePanel(false);
		frame.add(ip,BorderLayout.CENTER);
		
		//still frame button
		JButton still = new JButton("still");
		still.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				BufferedImage stillFrame = null;
				stillFrame = wc.getImage();
				imageFrame iframe = new imageFrame("STILL FRAME", stillFrame);
			}
		}		
		);
		frame.add(still,BorderLayout.SOUTH);
		
		//remove bg button
		final JButton BG = new JButton("remove background");
		BG.addActionListener( new ActionListener(){
			boolean toggle = false;
			public void actionPerformed(ActionEvent e) {
				if(!toggle){
					bgi = wc.getImage();
					BG.setText("Clear bg removal");
					th = Double.parseDouble(JOptionPane.showInputDialog("Type the threshold:"));
				}else{
					bgi = null;
					BG.setText("remove background");
				}
				toggle = !toggle;
			}
		}		
		);
		frame.add(BG,BorderLayout.NORTH);
		
		//load a bg img
		BufferedImage bg = mc.loadImage("resources/fundo.png");
		
		//main loop
		while(true){
			
			//set the main image panel as webcam
			if(bgi == null){
				ip.setImage(wc.getImage());
				ip.setText("");
				ip.setBgImage(null);
			}else{
				ip.setImage(ia.removeBackgroundByBrightness(bgi, wc.getImage(), th, new Color(0,0,0,0)));
				ip.setBgImage(bg);
				//ip.setText("Comparison: "+ia.compareImagesBrightness(bgi, wc.getImage())*100+"%");
			}
			
		}
	}

}
