import java.awt.Color;
import java.awt.image.BufferedImage;


public class imageAnalyzer {

	public int brightness(int color){
		int br = (((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff))/3;
		return br;
	}
	
	public double comparePixelBrightness(int pa, int pb){
		//grab data
		float pab = brightness(pa);
		float pbb = brightness(pb);

		//compare info
		double factor = pab/pbb;
		if(factor > 1){
			factor -= 1;
			return factor;
		}else{
			return factor;
		}
	}
	
	public double comparePixel(int pa, int pb){
		//grab data
		float par = pa << 16;
		float pag = pa << 8;
		float pab = pa;
		float pbr = pb << 16;
		float pbg = pb << 8;
		float pbb = pb;
		//compare info
		double factor = ((par/pbr) + (pag/pbg) + (pab/pbb))/3;
		if(factor > 1){
			factor -= 1;
			return factor;
		}else{
			return factor;
		}
	}
	
	public double compareImagesBrightness(BufferedImage a, BufferedImage b){
		if(a != null && b != null && sameSize(a,b)){
			double factor = 0;
			for(int x=0;x<a.getWidth();x++){
				for(int y=0;y<a.getHeight();y++){
					factor += comparePixelBrightness(b.getRGB(x, y), a.getRGB(x, y));
				}
			}
			factor /= a.getWidth()*a.getHeight();
			return factor;
		}else{
			return -1;
		}
	}
	
	public double compareImages(BufferedImage a, BufferedImage b){
		if(a != null && b != null && sameSize(a,b)){
			double factor = 0;
			for(int x=0;x<a.getWidth();x++){
				for(int y=0;y<a.getHeight();y++){
					factor += comparePixel(b.getRGB(x, y), a.getRGB(x, y));
				}
			}
			factor /= a.getWidth()*a.getHeight();
			return factor;
		}else{
			return -1;
		}
	}
	
	public boolean sameSize(BufferedImage a, BufferedImage b){
		if(a.getWidth() == b.getWidth() && a.getHeight() == b.getHeight()){
			return true;
		}else{
			return false;
		}
	}
	
	public BufferedImage removeBackgroundByBrightness(BufferedImage bg, BufferedImage target, double th, Color bgc){
		BufferedImage rt = new BufferedImage(target.getWidth(),target.getHeight(),BufferedImage.TYPE_INT_ARGB);
		if(bg != null && target != null && sameSize(bg,target)){
			for(int x=0;x<bg.getWidth();x++){
				for(int y=0;y<bg.getHeight();y++){
					if(comparePixelBrightness(bg.getRGB(x, y), target.getRGB(x,y)) >= th){
						rt.setRGB(x, y, bgc.getRGB());
					}else{
						rt.setRGB(x, y, target.getRGB(x,y));
					}
				}
			}
			return rt;
		}
		return null;
	}
	
	public BufferedImage removeBackground(BufferedImage bg, BufferedImage target, double th, Color bgc){
		BufferedImage rt = new BufferedImage(target.getWidth(),target.getHeight(),BufferedImage.TYPE_INT_ARGB);
		if(bg != null && target != null && sameSize(bg,target)){
			for(int x=0;x<bg.getWidth();x++){
				for(int y=0;y<bg.getHeight();y++){
					if(comparePixel(bg.getRGB(x, y), target.getRGB(x,y)) >= th){
						rt.setRGB(x, y, bgc.getRGB());
					}else{
						rt.setRGB(x, y, target.getRGB(x,y));
					}
				}
			}
			return rt;
		}
		return null;
	}
}
