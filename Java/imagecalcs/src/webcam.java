import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.media.Buffer;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.Player;
import javax.media.control.FrameGrabbingControl;
import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;
import javax.swing.JOptionPane;


public class webcam {

	private static Player player = null;
	private MediaLocator ml = null;
	private static Buffer buf = null;
	private static Image img = null;
	private static BufferToImage btoi = null;
	private String locator;
	
	//constructors
	public webcam(){
		ml = new MediaLocator("vfw://0");
		locator = "vfw://0";
		try
		{
			player = Manager.createRealizedPlayer(ml);
			player.start();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "N�o foi possivel conectar com a webcam!", "ERRO", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public webcam(String locator){
		ml = new MediaLocator(locator);
		this.locator = locator;
		try
		{
			player = Manager.createRealizedPlayer(ml);
			player.start();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "N�o foi possivel conectar com a webcam!", "ERRO", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	//functions
	private static BufferedImage bufferImage(Image img){
		if(img != null){
			BufferedImage buffimg = new BufferedImage ( img.getWidth(null),img.getHeight(null),BufferedImage.TYPE_INT_BGR  );
			buffimg.createGraphics().drawImage( img, 0, 0, null );
			return buffimg;
		}
		return null;
	}
	
	private static void closeplayer()
	{
		if(player != null){
			player.close();
			player.deallocate();
		}
	}
	
	public void close(){
		closeplayer();
	}
	
	public void reboot(){
		ml = new MediaLocator(locator);
		try
		{
			player = Manager.createRealizedPlayer(ml);
			player.start();
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "N�o foi possivel reconectar com a webcam!", "ERRO", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public BufferedImage getImage()
	{
		FrameGrabbingControl fgc = (FrameGrabbingControl)
		player.getControl("javax.media.control.FrameGrabbingControl");
		buf = fgc.grabFrame();
		btoi = new BufferToImage((VideoFormat)buf.getFormat());
		img = btoi.createImage(buf);
		return bufferImage(img);
	}
	
}
