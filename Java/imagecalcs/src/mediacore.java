import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;


public class mediacore {

	public BufferedImage loadImage(String arquivo) {
		File f =  new File( arquivo );
		if(!f.exists()){
			JOptionPane.showMessageDialog(null, "Falha ao localizar o arquivo!", "Erro", JOptionPane.ERROR_MESSAGE);
		}else{
			try {
				return ImageIO.read( f );
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Falha ao importar imagem! \nMotivo: "+e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		return null;
	}
	
	public int saveImage(BufferedImage img, String format, String fileName){
		// BufferedImage to File
		try {
			ImageIO.write( img, format /* "png" "jpeg" "jpg" "gif" */,new File ( fileName+"."+format ) /* target */ );
			return 0;
		} catch (Exception e) {
			return 1;
		}
	}
	
}
