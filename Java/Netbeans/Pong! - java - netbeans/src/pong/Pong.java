package pong;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;

/**
 *
 * @author lucasmontec
 */
public class Pong extends render{

    ball b;
    player p;
    
    public static void main(String[] args) {
        Pong png = new Pong();
    }

    @Override
    public void initialize() {
        //inicializa o player
        p = new player();
        p.size = 90;
        p.x = 500 - p.playerW;
        p.y = 0;
        
        //inicializa a bola
        Random rnd = new Random();
        b = new ball();
        b.setPos(250,250);
        b.size = 50;
        b.vy = (float)(0.5-Math.random()*1);
        b.vx = (5+rnd.nextInt(6))/10.0f;
        
    }
    
    @Override
    public void draw(Graphics2D g) {
        //bg        
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 500, 500);
        
        //desenha a bola
        if(b != null && p != null){
            g.setColor(Color.RED);
            g.fillOval((int)(b.x - b.size/2),(int)(b.y - b.size/2), (int)b.size, (int)b.size);

            //desenha o player
            g.setColor(Color.blue);
            g.fillRect((int)(500 - p.playerW), (int)(mi.y - p.size/2), (int)(p.playerW), (int)(p.size));

            //desenha os pontos
            g.setColor(Color.white);
            g.drawString("Pontos: "+p.pts, 20, 40);
        }
    }

    @Override
    public void update() {
        if(p != null && b != null){
            b.update();
            p.y = mi.y;
            
            //pong coll check
            if(b.y < b.size || b.y > (500 - b.size/2)){
                b.collisionY();
            }
            //to player coll
            if(b.y < (p.y+(p.size/2)) && b.y > p.y-(p.size/2) && (b.x+(b.size/2)+p.playerW) >= 500){
                b.x = 500 - b.size/2 - p.playerW;
                float prc = ( (p.y+(p.size/2)) - (b.y+(b.size/2)) )*0.01f;
                b.collisionX(prc);
                p.pts += 100;
                //diminui a bola e a barra
                if(b.size > 10){
                    b.size -= 0.5;
                }else{
                    if(p.size > 10){
                        p.size -= 0.5;
                    }
                }
            }else if(b.x > 500){
                b.setPos(250, 250);
                b.vy = (float)(1-Math.random()*2);
                b.vx = (float)(1-Math.random()*2);
                p.pts -= 10;
            }
            //to left
            if ((b.x-b.size/2) <= 0){
                b.collisionX(0);
            }
        }
    }

}
