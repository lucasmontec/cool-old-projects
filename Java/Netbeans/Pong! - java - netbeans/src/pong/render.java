/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pong;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public abstract class render extends JFrame{

    boolean running = true;
    BufferStrategy strategy = null;
    mouseInput mi = new mouseInput();
    
    public render(){
          super("Wild Rendering Engine");
          Toolkit tk = Toolkit.getDefaultToolkit();
          int w = tk.getScreenSize().width;
          int h = tk.getScreenSize().height;
          this.setLayout(new FlowLayout());
          this.setIgnoreRepaint(true);
          this.setSize(500, 500);
          this.setLocation(w/2 - 250, h/2 - 250);
          this.setResizable(false);
          this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          this.setVisible(true);
          this.addMouseMotionListener(mi);
          
          this.createBufferStrategy(2);
          renderloop();
    }
      
    private void renderloop(){
          initialize();
          
          try{
              strategy = this.getBufferStrategy();
          }catch(Exception e){
              JOptionPane.showMessageDialog(null, "Erro: "+e.getMessage());
          }
          
          if(strategy != null){
              Graphics g = strategy.getDrawGraphics();
              ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                                RenderingHints.VALUE_ANTIALIAS_ON);
              ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                                RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
              Graphics2D g2d = ((Graphics2D) g);

              // Game loop
              while (running) {
                    draw(g2d);
                    update();

                    strategy.show();
              }
          }
    }
    
    public abstract void draw(Graphics2D g);
    public abstract void update();
    public abstract void initialize();
}

class mouseInput implements MouseMotionListener{

    float x,y;
    
    @Override
    public void mouseDragged(MouseEvent e) {
        //nothing
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }
    
}