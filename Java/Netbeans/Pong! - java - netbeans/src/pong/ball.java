/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pong;

/**
 *
 * @author lucasmontec
 */
public class ball {
    public float x,y;
    public float size;
    public float vx = 0,vy = 0;
    
    public void setPos(float xx, float yy){
        x = xx;
        y = yy;
    }
    
    public void move(float dx,float dy){
        x += dx;
        y += dy;
    }
    
    public void update(){
        this.move(vx,vy);
    }
    
    public void collisionY(){
        vy = -vy;
    }
    
    public void collisionX(float prc){
        vy += prc;
        vx = -vx;
    }
}
