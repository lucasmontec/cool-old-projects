/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package borntobewildengine;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lucasmontec
 */
public class render extends JFrame{

    boolean done = false;
    BufferStrategy strategy = null;
    mouseInput mi = new mouseInput();
    
    public render(){
          super("Wild Rendering Engine");
          Toolkit tk = Toolkit.getDefaultToolkit();
          int w = tk.getScreenSize().width;
          int h = tk.getScreenSize().height;
          this.setLayout(new FlowLayout());
          this.setIgnoreRepaint(true);
          this.setSize(500, 500);
          this.setLocation(w/2 - 250, h/2 - 250);
          this.setResizable(false);
          this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          this.setVisible(true);
          this.addMouseMotionListener(mi);
          
          this.createBufferStrategy(2);
          draw();
    }
      
      public void draw(){
          try{
              strategy = this.getBufferStrategy();
          }catch(Exception e){
              JOptionPane.showMessageDialog(null, "Erro: "+e.getMessage());
          }
          
          Graphics g = strategy.getDrawGraphics();
          ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
          ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                            RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);

          
          //follower
          float bx = 250, by = 250;
          float dx = 0;
          float dy = 0;
          
          Graphics2D g2d = ((Graphics2D) g);
          
          // Render loop
          while (!done) {
                dx = (float) ((mi.x - bx)*0.005);
                dy = (float) ((mi.y - by)*0.005);
                bx += dx;
                by += dy;
                g2d.setColor(Color.black);
                g2d.fillRect(0, 0, 500, 500);
                g2d.setColor(Color.red);
                g2d.fillOval((int)(bx-50),(int)(by-50), 100, 100);
                g2d.setColor(Color.lightGray);
                g2d.fillOval((int)(bx-40),(int)(by-40), 80, 80);
                g2d.drawString("X:["+(int)bx+"] Y:["+(int)by+"]",(int)(bx-40),(int)(by+65));
                
                strategy.show();
          }
      }
}


class mouseInput implements MouseMotionListener{

    float x,y;
    
    @Override
    public void mouseDragged(MouseEvent e) {
        //nothing
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }
    
}