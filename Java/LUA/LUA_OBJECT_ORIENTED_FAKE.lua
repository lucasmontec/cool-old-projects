--Lua OO HAX

--Essa é a tabela do objeto
local Object = {}

--Isso é a metatable. Uma metatable é uma tabela em lua que especifica operações
--customizadas para tabelas.
local mt = {}

--[[
13 – Metatables and Metamethods

Usually, tables in Lua have a quite predictable set of operations. We can add key-value pairs,
we can check the value associated with a key, we can traverse all key-value pairs,
and that is all. We cannot add tables, we cannot compare tables, and we cannot call a table.

Metatables allow us to change the behavior of a table. For instance, using metatables,
we can define how Lua computes the expression a+b, where a and b are tables. Whenever Lua
tries to add two tables, it checks whether either of them has a metatable and whether that
metatable has an __add field. If Lua finds this field, it calls the corresponding value
(the so-called metamethod, which should be a function) to compute the sum.

Each table in Lua may have its own metatable. (As we will see later, userdata also can have
metatables.) Lua always create new tables without metatables:

    t = {}
    print(getmetatable(t))   --> nil
We can use setmetatable to set or change the metatable of any table:
    t1 = {}
    setmetatable(t, t1)
    assert(getmetatable(t) == t1)
Any table can be the metatable of any other table; a group of related tables may share a
common metatable (which describes their common behavior); a table can be its own metatable
(so that it describes its own individual behavior). Any configuration is valid.

]]--

--Teste de criação de um método para o objeto
function Object:getName()
	--retorna o campo name do objeto
	return self._name
end

--Metodo para criar uma 'instancia' do objeto
--Na verdade ele cria uma table e coloca a MT do objeto
function Object:new(name)
	--Cria a table
	local o = {}
	--Adiciona as propriedades 'cosntrutor'
	--Esse 'o._name' é só um exemplo
	--Esse _ antes de name diz que ele é 'invisível'
	o._name = name
	
	--Seta a metatable da table criada 'o' para nossa 'mt'
	--É o mesmo que transformar nossa table 'o' em um objeto com funções
	--metametodos
	local m = setmetatable(o, mt)
	
	--Retorna nossa table
	return o
end

--Impede que o usuário mude o objeto
--tab é a table (FOO.key), key é a chave (foo.KEY), e value é o valor (foo.key = VALUE)
mt.__newindex = function(tab,key,value)
	--Imutable members, manda um erro se o usuário tentar modificar o campo name
	if key == "name" then
		error("name is read-only")
	end
	--Imutable table
	error("This object has no member called '"..key.."'")
end

--Faz com que as chamadas de coisas na table com a metatable 'mt'
mt.__index = function(tab,key)
	--Usa as funções e atributos do Objetc
	return Object[key]
end

--TESTING

--Criamos um objeto o
local o = Object:new("HelloWorld")
--Chamamos seu nome pela propriedade
print(o._name)
--Chamamos seu nome pelo metodo
print(o:getName())
--Tentaremos modificar name
o.name = "not"
--Chamamos seu nome pela propriedade
print(o.name)