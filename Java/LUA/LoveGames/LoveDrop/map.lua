require 'tile'

Map = {}
Map.divx = 16
Map.divy = 16
Map.color = {0,0,255}
Map.time = 0
Map.xFactor = 50
Map.yFactor = 50

function Map.draw()
	width = love.graphics.getWidth( )
	height = love.graphics.getHeight( )
	
	divxx = width/Map.divx
	divyy = height/Map.divy
	
	for X=0, Map.divx do
		for Y=0, Map.divy do
			--Draw the tile
			Tile.draw(divxx*X,divyy*Y,divxx, divyy, Color.lerp(Color.col(200,40,40),Color.col(20,50,140), (1+math.sin( Map.time+(1+Y)*Map.yFactor+(1+X)*Map.xFactor ))/2 ) )
		end
	end
end

function Map.update(dt)
  Map.time = Map.time + dt
end