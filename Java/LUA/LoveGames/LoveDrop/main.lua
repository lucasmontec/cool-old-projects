require 'map'

function love.load()
   local f = love.graphics.newFont(12)
   love.graphics.setFont(f)
   love.graphics.setColor(0,0,0,255)
   love.graphics.setBackgroundColor(255,255,255)
end

function love.update(dt)
  if love.keyboard.isDown("up") then
    Map.update(dt*-3);
  elseif love.keyboard.isDown("down") then
    Map.update(dt*3);
  else
    Map.update(dt);
  end
  
  if love.keyboard.isDown("left") then
    Map.xFactor = Map.xFactor + 0.03
  end
  
  if love.keyboard.isDown("right") then
    Map.xFactor = Map.xFactor -  0.03
  end

end

function love.keypressed(key)
  if key == "kp+" then
    if(Map.divx < 50) then
      Map.divx = Map.divx + 1
      Map.divy = Map.divy + 1
    end
  end
  
  if key == "kp-" then
    if(Map.divx >= 2) then
      Map.divx = Map.divx - 1
      Map.divy = Map.divy - 1
    end
  end
end

function love.draw()
	love.graphics.setColor( 255, 0, 0 )
    love.graphics.print("Zumbi ta cheio de amor!", 400, 300)
	
	Map.draw();
	
	love.graphics.setColor( 0, 0, 0 )
	
	love.graphics.print("LOVVVEEEE =0 MUITO AMOR!", 10, 10)
end