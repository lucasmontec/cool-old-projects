require 'amath'

Color = {}

function Color.lerp(cola,colb, t)
  colc = {}
	if (t <= 1 and t >= 0) then
		colc.r = lerp(cola.r,colb.r,t)
		colc.g = lerp(cola.g,colb.g,t)
		colc.b = lerp(cola.b,colb.b,t)
	end
  return colc
end

function Color.col(r,g,b)
	cc = {}
	cc.r = r
	cc.g = g
	cc.b = b
	return cc
end