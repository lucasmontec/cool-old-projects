require 'amath'

Color = {}
local ColorMeta = {}

function Color:new(r,g,b,a)
	--Cria a table
	local o = {}
	
  o.r = r
  o.g = g
  o.b = b
  o.a = a
  
	--Seta a metatable da table criada 'o' para nossa 'mt'
	--É o mesmo que transformar nossa table 'o' em um objeto com funções
	--metametodos
	local m = setmetatable(o, ColorMeta)
	
	--Retorna nossa table
	return o
end

--Object
function Color:lerp(colto, t)
  if (t > 1) then t=1 end
  if (t < 0) then t=0 end
  
  self.r = lerp(self.r,colto.r,t)
	self.g = lerp(self.g,colto.g,t)
	self.b = lerp(self.b,colto.b,t)
  self.a = lerp(self.a,colto.a,t)
end

function Color:set()
  love.graphics.setColor( self.r, self.g, self.b, self.a )
end

--'static'
function lerpColor(cola, colb, t)
  colc = Color:new(0,0,0,0)
	
  if (t > 1) then t=1 end
  if (t < 0) then t=0 end
  
	colc.r = lerp(cola.r,colb.r,t)
	colc.g = lerp(cola.g,colb.g,t)
	colc.b = lerp(cola.b,colb.b,t)
  
  return colc
end

ColorMeta.__newindex = function(tab,key,value)
	--Imutable table
	error("This object has no member called '"..key.."'")
end

--Faz com que as chamadas de coisas na table com a metatable 'mt'
ColorMeta.__index = function(tab,key)
	--Usa as funções e atributos do Objetc
	return Color[key]
end