require 'node'
require 'oo'

Vertex = {}
VertexMeta = NodeMeta


function Vertex:new(x,y,s,col,cap,rg)
  local nv = setmetatable({} , VertexMeta)
  
  nv.capacity = cap or 10
  nv.range = rg or 200
  nv.x = x
  nv.y = y
  nv.size = s
  nv.color = col
  nv._maxConnections = 4
  nv._currConnectionID = 1
  nv.connectedVertices = {}
  
	--Retorna nossa table
	return nv
end

--Herança feita a mao
VertexMeta.__index = function(tab,key)
	--Usa as funções e atributos do Object
  if(Vertex[key] ~= nil) then
    return Vertex[key]
  else
    return Node[key]
  end
end

function Vertex:canConnect()
  if self._currConnectionID <= self._maxConnections then
    return true
  end
  return false
end

function Vertex:inRange(target)
  local dist = math.dist(target.x + target.size/2, target.y + target.size/2, self.x + self.size/2, self.y + self.size/2)
  if dist < self.range then
    return true
  end
  return false
end

function Vertex:connect(target)
  if target ~= self and self:canConnect() and self:inRange(target)then
    self.connectedVertices[self._currConnectionID] = target
    self._currConnectionID = self._currConnectionID+1
  end
end

--Shouldn't be needed
function Vertex:isConnected(vert)
  for _,v in ipairs(vert.connectedVertices) do
    if v == self then
      return true
    end
  end
  return false
end

function Vertex:draw()
  --Draw a range if the mouse is close
  local mx, my = love.mouse.getPosition()
  local dist = math.dist(mx,my, self.x + self.size/2, self.y + self.size/2)
  if dist < (1.5*self.range) then
    love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a*(0.2 - (dist/(1.5*self.range)*0.2))  )
    love.graphics.circle( "fill", self.x, self.y, self.range, self.range )
  end
  
  --Draw the pulsating bg
  local pulsar = ( (1+math.sin(love.timer.getTime()*9))/2 )*(0.2*self.size)
  love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a*0.3 )
  love.graphics.circle( "fill", self.x, self.y, self.size+pulsar, self.size+pulsar )
  
  --Draw connections
  love.graphics.setLineWidth( 2 )
  for _,v in ipairs(self.connectedVertices) do
    love.graphics.line(self.x, self.y, v.x, v.y)
  end
  
  --Draw the main node circle
  love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a )
  love.graphics.circle( "fill", self.x, self.y, self.size, self.size )
end