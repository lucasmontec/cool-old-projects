Packet = {}
PacketMeta = {}

function Packet:new(starting_vertex, information_amount)
  
	local ret = {}
  ret.currentNode = starting_vertex
  ret.targetNode = starting_vertex.connectedVertices[ math.random( #starting_vertex.connectedVertices ) ]
  ret.x = starting_vertex.x + starting_vertex.size/2
  ret.y = starting_vertex.y + starting_vertex.size/2
  ret.color = starting_vertex.color
  ret.size = information_amount*0.15
  ret.inf = information_amount
  ret.alive = true --Means it didn't got lost in the network
  ret.deathCause = nil --Store the vertex that killed this packet

  setmetatable(ret,  PacketMeta)
  
	--Retorna nossa table
	return ret
end

--Get a new objective to go
function Packet:getTarget(vert)
  self.targetNode = vert.connectedVertices[ math.random( #vert.connectedVertices ) ] or vert.connectedVertices[1]
end

--Arrive at a vertex
function Packet:arrive(vert)
  print("lol")
  --First thing to do is lose information if the vertex cannot handle
  if vert.capacity < self.inf then
    self.inf = vert.capacity
  end
  
  --Set the position
  self.x = vert.x
  self.y = vert.y
  self.color = vert.color
  
  --Set the current vertex
  currentNode = vert
  
  --Death
  if #vert.connectedVertices == 0 then
    self.alive = false --The packet got lost
    self.deathCause = vert
  else
    self:getTarget(currentNode)
    while(self.targetNode == nil or self.targetNode == self.currentNode) do
      --Get a new target
      self:getTarget(currentNode)
    end
  end
end

function Packet:canArrive(vert)
  if math.dist(self.x, self.y, vert.x, vert.y) < 10 then
    return true
  end
  return false
end

function Packet:update(dt)

  --Check if we have a target
  if self.targetNode ~= nil then

  --Now check normal arrivals
  if self:canArrive(self.targetNode) then
      self:arrive(self.targetNode)
  end

  --Move the packet
    self.x = self.x + (self.targetNode.x - self.x)*0.06
    self.y = self.y + (self.targetNode.y - self.y)*0.06
  end
end

function Packet:draw()
  --Draw the pulsating bg
  local pulsar = ( (1+math.sin(love.timer.getTime()*4))/2 )*(0.2*self.size)
  love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a*0.5 )
  love.graphics.circle( "fill", self.x, self.y, self.size+pulsar, self.size+pulsar )
  
  --love.graphics.circle( "fill", self.targetNode.x, self.targetNode.y , 50 , 50)
end

PacketMeta.__index = function(tab,key)
	--Usa as funções e atributos do Objetc
	return Packet[key]
end