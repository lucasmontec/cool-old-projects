require 'color'

Node = {}
NodeMeta = {}

function Node:new(x,y,s,col)
	--Cria a table
	local o = {}
	
  o.x = x
  o.y = y
  o.size = s
  o.color = col
  
	--Seta a metatable da table criada 'o' para nossa 'mt'
	--É o mesmo que transformar nossa table 'o' em um objeto com funções
	--metametodos
	setmetatable(o, NodeMeta)
	
	--Retorna nossa table
	return o
end

function Node:update(dt)
--Nothing
end

function Node:draw()
  --Draw the pulsating bg
  local pulsar = ( (1+math.sin(love.timer.getTime()*9))/2 )*(0.2*self.size)
  love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a*0.3 )
  love.graphics.circle( "fill", self.x, self.y, self.size+pulsar, self.size+pulsar )
  
  --Draw the main node circle
  love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a )
  love.graphics.circle( "fill", self.x, self.y, self.size, self.size )
end

--Faz com que as chamadas de coisas na table com a metatable 'mt'
NodeMeta.__index = function(tab,key)
	--Usa as funções e atributos do Objetc
	return Node[key]
end