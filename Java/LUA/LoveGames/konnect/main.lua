require 'vertex'
require 'packet'

vertices = {}
id = 3

--Create a start and end vertex
v_start = Vertex:new(50,55,15,Color:new(40,40,200,255),100,120)
v_end = Vertex:new(600,600,15,Color:new(40,200,40,255),100,120)

vertices[1] = v_start
vertices[2] = v_end

packets = {}
p_id = 1

timer = 0
packet_time = 1

flow_timer = 1
received_info = 0
flow = 0
desired_flow = 80

music_timer = 1
pulse_timer = 1
pulse_counter = 0

s_emit = love.audio.newSource("emit.wav", "static")
s_receive = love.audio.newSource("receive.wav", "static")
s_receive:setVolume(0.6)
s_connected = love.audio.newSource("music.wav", "static")

music = { 0.7, 0.9, 0.9, 0.5, 0.7, 0.9, 0.9, 0.8 }
music_id = 1

-------------------------------------------------------LOAD
function love.load()
   local f = love.graphics.newFont(12)
   love.graphics.setFont(f)
   love.graphics.setColor(0,0,0,255)
   love.graphics.setBackgroundColor(255,255,255)
   love.graphics.setMode(650, 650, false, true, 8)
   love.graphics.setCaption("Konnect")
end

----------------------------------------------------UPDATE
function love.update(dt)
  for _,v in ipairs(vertices) do
    v:update(dt)
  end
  
  for k,v in ipairs(packets) do
    if v ~= nil then
      v:update(dt)
      if not v.alive then
        if v.deathCause == v_end then
          received_info = received_info + v.inf
          pulse_counter = pulse_counter + 1
        end
        table.remove(packets, k)
        p_id = #packets + 1
      end
    end
  end
  
  -----------------------------------------spawnpackets
  timer = timer + dt
  if timer > packet_time then
    if #v_start.connectedVertices > 0 then
      local pt = Packet:new(v_start, 200)
      packets[p_id] = pt
      p_id = p_id + 1
      love.audio.rewind(s_emit)
      love.audio.play(s_emit)
    end
    timer = 0
  end
  
  ------------------------------------------Calculate flow
  if #v_start.connectedVertices > 0 then
    flow_timer = flow_timer + dt
  end
  --Get the info over time
  flow = received_info/flow_timer
  
  -------------------------------------------Enrich sounds
  music_timer = music_timer + dt
  if flow > 0 and music_timer > 0.5 then
    love.audio.rewind(s_connected)
    love.audio.play(s_connected)
    music_timer = 0
  end
  
  --------------------------------------------Pulse when packets are received
  pulse_timer = pulse_timer + dt
  if pulse_timer > 0.25 and pulse_counter > 0 then
    s_receive:setPitch(music[music_id])
    if music_id < #music then music_id = music_id + 1 else music_id = 1 end
    love.audio.rewind(s_receive)
    love.audio.play(s_receive)
    pulse_counter = pulse_counter - 1
  end
end

---------------------------------------------RESET
function love.keypressed(key)
  if key == "r" then
    --reset
    vertices = {}
    id = 3
    v_start = Vertex:new(50,50,15,Color:new(40,40,200,255),100,120)
    v_end = Vertex:new(600,600,15,Color:new(40,200,40,255),100,120)
    vertices[1] = v_start
    vertices[2] = v_end

    packets = {}
    p_id = 1
    
    timer = 0
    score = 0
    
    flow_timer = 1
    received_info = 0
    flow = 0
  end
end

-------------------------------------------------------ADD VERTICES
function love.mousepressed(x,y,btn)
  if btn == "l" then
    --Create the vertex to check stuff
    local nv = Vertex:new(x,y,8,Color:new(255,0,0,255),100,120)
    --Check if the vertex was placed
    local placed = false
    --Check if can place and connect
    for _,v in ipairs(vertices) do
      if v:inRange(nv) then
        
        --Connect with end and start
        if v == v_end then
          nv:connect(v)
        else
          --Conect to the rest
          v:connect(nv)
        end
        
        if not placed then
          vertices[id] = nv
          id = id + 1
          placed = true
        end
      end
    end
  end
end

---------------------------------------DRAW
function love.draw()
	for _,v in ipairs(vertices) do
    if(v ~= v_end)then
      v:draw()
    end
  end
  
  for _,v in ipairs(packets) do
    if v ~= nil then
      v:draw()
    end
  end
  
	v_end:draw()
  
	love.graphics.setColor( 0, 0, 0 )
	love.graphics.print("KONNECT PROTOTYPE: 1", 10, 10)
  love.graphics.print("Desired flow: "..desired_flow.." Flow: "..math.floor(flow).." Level status: "..math.floor(((flow/desired_flow)*100)).."%", 10, 25)
end