require 'map'

function love.load()
   local f = love.graphics.newFont(12)
   love.graphics.setFont(f)
   love.graphics.setColor(0,0,0,255)
   love.graphics.setBackgroundColor(255,255,255)
end

function love.update(dt)
  Map.update(dt);
end

function love.mousepressed(x, y, button)
   if button == "l" then
      Map.makeripple(50)
   end
end

function love.keypressed(key)
  if key == "kp+" then
    if(Map.divx < 50) then
      Map.divx = Map.divx + 1
      Map.divy = Map.divy + 1
    end
  end
  
  if key == "kp-" then
    if(Map.divx >= 2) then
      Map.divx = Map.divx - 1
      Map.divy = Map.divy - 1
    end
  end
end

function love.draw()
	
	Map.draw();
	
	love.graphics.setColor( 0, 0, 0 )
	
	love.graphics.print("LOVVVEEEE =0 MUITO AMOR!", 10, 10)
end