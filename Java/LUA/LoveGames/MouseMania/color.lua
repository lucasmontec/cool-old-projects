require 'amath'

Color = {}

function Color.lerp(cola,colb, t)
  colc = {}
	if (t > 1) then t=1 end
  if (t < 0) then t=0 end
  
	colc.r = lerp(cola.r,colb.r,t)
	colc.g = lerp(cola.g,colb.g,t)
	colc.b = lerp(cola.b,colb.b,t)
  
  return colc
end

function Color.col(r,g,b)
	cc = {}
	cc.r = r
	cc.g = g
	cc.b = b
	return cc
end