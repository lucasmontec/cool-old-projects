require 'tile'

Map = {}
Map.divx = 16
Map.divy = 16
Map.color = {0,0,255}
Map.xFactor = 50
Map.yFactor = 50
Map.ripple = 0
Map.time = 0

function Map.makeripple(rp)
  if Map.ripple < 250 then
    Map.ripple = Map.ripple + rp
  end
end

function Map.draw()
	width = love.graphics.getWidth( )
	height = love.graphics.getHeight( )
	
	divxx = width/Map.divx
	divyy = height/Map.divy
	
  mx, my = love.mouse.getPosition()
  
	for X=0, Map.divx do
		for Y=0, Map.divy do
			--Draw the tile
			Tile.draw(divxx*X,divyy*Y,divxx, divyy, Color.lerp(Color.col(0,0,0),Color.col(255,255,255), math.dist( (divxx*X)+(divxx/2) ,(divyy*Y)+(divyy/2), mx,my)/(80 + ((1+math.sin(Map.time*8))/2)*Map.ripple) ))
		end
	end
end

function Map.update(dt)
  Map.time = Map.time + dt
  
  if Map.ripple > 1 then
    Map.ripple = Map.ripple - 0.4
  end
end