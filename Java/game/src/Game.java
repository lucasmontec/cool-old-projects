import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

import RPI.Cores.GameManager;
import RPI.Cores.InputManager;
import RPI.Cores.MediaManager;
import RPI.bases.GameAction;

public class Game extends GameManager {
	
	static InputManager input;
	static MediaManager media;
	
	protected GameAction exit = new GameAction("exit",GameAction.Behavior.DETECT_INITIAL_PRESS_ONLY);
	
	public void initialize(){
		input = new InputManager(screen.getScreen());
		
		input.mapToKey(exit, KeyEvent.VK_ESCAPE);
	}
	
	public void draw(Graphics g){
		//hints - antialiasing
		if(g instanceof Graphics2D){
			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}
		
		//bg
		g.setColor(new Color(0,64,128));
		g.fillRect(0, 0, screen.getWidth(), screen.getHeight());
		
		//game
		g.setFont(new Font("verdana",Font.PLAIN,12));
		
		//fontes
		g.setFont(DEFAULT_FONT);
		g.setColor(screen.getScreen().getForeground());
		g.drawString("Game", 10, 28);
		g.drawString("Em desenvolvimento.", 10, 60);
	}
	
	public static void main(String args[]){
		Game game = new Game();
		game.run();
	}

	public synchronized void update(long elapsedTime) {
		
		//desliga
		if(exit.isPressed()){
			stop();
		}
	}
}