package RPI.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class Sprite {
	
	private Animation anim;
	//scale
	private float xscale = 1;
	private float yscale = 1;
	//pos
	private float x;
	private float y;
	//vel
	private float vel_x;
	private float vel_y;
	//angle
	private float angle = 0;
	private float vel_ang;
	//transformer
	AffineTransform affine;
	
	//constroi a sprite
	public Sprite(Animation anim){
		this.anim = anim;
		affine = new AffineTransform();
	}
	
	public Sprite(BufferedImage img){
		this.anim = new Animation();
		this.anim.addFrame(img, 100);
		affine = new AffineTransform();
	}
	
	public Sprite(){
		this.anim = new Animation();
		affine = new AffineTransform();
	}
	
	//transforms
	public void applyTransform(Image image){
	affine.setToTranslation(0,0);
	affine.translate(x+anim.getImage().getWidth(null)/2, y+anim.getImage().getHeight(null)/2);
	
	affine.translate(-xscale*anim.getImage().getWidth(null)/2,-yscale*anim.getImage().getHeight(null)/2);
	affine.scale(xscale, yscale);
	
	affine.rotate(Math.toRadians(angle),this.anim.getImage().getWidth(null)/2,this.anim.getImage().getHeight(null)/2);
	}
	
	//draw
	public void draw(Graphics2D g){
		if(this.anim.getImage() != null){
			applyTransform(this.anim.getImage());
			g.drawImage(this.anim.getImage(),affine, null);
		}
	}
	
	//add frames
	public void addFrame(BufferedImage image, int duration){
		this.anim.addFrame(image, duration);
	}
	
	//update based on time
	public void update(long elapsedTime){
		this.rotate(vel_ang*elapsedTime);
		this.move(vel_x*elapsedTime,vel_y*elapsedTime);
		this.anim.update(elapsedTime);
	}
	
	//infos
	public float getAngle(){
		return angle;
	}
	
	public float getCenterX(){
		return x + this.anim.getImage().getWidth(null)/2;
	}
	
	public float getCenterY(){
		return y + this.anim.getImage().getHeight(null)/2;
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public float getAngularVelocity(){
		return this.vel_ang;
	}
	
	public float getVelocityX(){
		return this.vel_x;
	}
	
	public float getVelocityY(){
		return this.vel_y;
	}
	
	public int getWidth(){
		return this.anim.getImage().getWidth(null);
	}
	
	public int getHeight(){
		return this.anim.getImage().getHeight(null);
	}
	
	public Image getImage(){
		return this.anim.getImage();
	}
	
	public float getXScale(){
		return xscale;
	}
	
	public float getYScale(){
		return yscale;
	}
	
	//sets - posi��o
	public void setPos(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public void setAngularVelocity(float a){
		this.vel_ang = a;
	}
	
	public void setAngle(float ang){
		this.angle = ang;
	}
	
	public void rotate(float ang){
		this.angle += ang;
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public void move(float x, float y){
		this.setX(this.x+x);
		this.setY(this.y+y);
	}
	
	//sets - velocidade
	public void setVelocity(double vx, double vy){
		this.vel_x = (float)vx;
		this.vel_y = (float) vy;
	}
	
	public void setVelocity(float vx, float vy){
		this.vel_x = vx;
		this.vel_y = vy;
	}
	
	public void setVelocityX(float vx){
		this.vel_x = vx;
	}
	
	public void setVelocityY(float vy){
		this.vel_y = vy;
	}
	
	public void accelerate(double v){
		this.setVelocityX((float)(this.vel_x+v));
		this.setVelocityY((float)(this.vel_y+v));
	}
	
	public void accelerate(double vx, double vy){
		this.setVelocityX((float)(this.vel_x+vx));
		this.setVelocityY((float)(this.vel_y+vy));
	}
	
	public void accelerate(float vx, float vy){
		this.setVelocityX(this.vel_x+vx);
		this.setVelocityY(this.vel_y+vy);
	}
	
	//sets - scala
	public void setYScale(float s){
		this.yscale = s;
	}
	
	public void setXScale(float s){
		this.xscale = s;
	}
	
	public void setScale(float x, float y){
		this.xscale = x;
		this.yscale = y;
	}
}
