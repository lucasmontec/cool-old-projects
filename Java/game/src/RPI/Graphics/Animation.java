package RPI.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class Animation {

	private ArrayList<AnimFrame> frames;
	private int currentFrameIndex;
	private long animTime;
	private long totalDuration;
	
	public Animation(){
		frames = new ArrayList<AnimFrame>();
		totalDuration = 0;
		start();
	}
	
	public synchronized void addFrame(BufferedImage image, long duration){
		totalDuration += duration;
		frames.add(new AnimFrame(image,totalDuration));
	}
	
	public synchronized void start(){
		animTime = 0;
		currentFrameIndex = 0;
	}
	
	public synchronized void update(long elapsedTime){
		if(frames.size() > 1){
			animTime += elapsedTime;
			
			if(animTime >= totalDuration){
				animTime = animTime%totalDuration;
				currentFrameIndex = 0;
			}
			
			while(animTime > getFrame(currentFrameIndex).endTime){
				currentFrameIndex++;
			}
		}
	}
	
	public synchronized BufferedImage getImage(){
		if(frames.size() == 0){
			return null;
		}else{
			return getFrame(currentFrameIndex).image;
		}
	}
	
	private AnimFrame getFrame(int index){
		return (AnimFrame)frames.get(index);
	}
	
	private class AnimFrame{
		
		BufferedImage image;
		long endTime;
		
		public AnimFrame(BufferedImage image,long endTime){
			this.image = image;
			this.endTime = endTime;
		}
		
	}
}