package RPI.Cores;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundManager{
	
    Clip sound;
    protected AudioInputStream soundStream;
    
	public SoundManager(){
	}
	
	public SoundManager(String path){
		sound = loadFromFile(new File(path)); 
	}
	
	public Clip loadSound(String path){
        return loadFromFile(new File(path));
	}
	
	private Clip loadFromFile(File f){
		Clip snd;
		try { 
            File audioFile = f;
            soundStream = AudioSystem.getAudioInputStream( audioFile );
            AudioFormat streamFormat = soundStream.getFormat( );
            DataLine.Info clipInfo = new DataLine.Info( Clip.class, streamFormat );
 
            snd = ( Clip ) AudioSystem.getLine( clipInfo );
            snd.open( soundStream );
            return snd;
        }
        catch ( UnsupportedAudioFileException e ) { return null; }
        catch ( IOException e ) { return null; }
        catch ( LineUnavailableException e ) { return null; }
	}
	
	public void play(){
		sound.start();
	}

}
