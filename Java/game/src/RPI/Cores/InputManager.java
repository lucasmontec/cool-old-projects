package RPI.Cores;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import RPI.bases.GameAction;

public class InputManager implements KeyListener, MouseListener, MouseMotionListener , MouseWheelListener {
	
	//mouse invisivel
	public static final Cursor INVISIBLE_CURSOR = Toolkit.getDefaultToolkit().createCustomCursor(
			Toolkit.getDefaultToolkit().getImage(""),new Point(0,0),"invisible");

	//mouse fake states
	public static final int MOUSE_MOVE_LEFT = 0;
	public static final int MOUSE_MOVE_RIGHT = 1;
	public static final int MOUSE_MOVE_UP = 2;
	public static final int MOUSE_MOVE_DOWN = 3;
	public static final int MOUSE_WHEEL_UP = 4;
	public static final int MOUSE_WHEEL_DOWN = 5;
	public static final int MOUSE_BUTTON_1 = 6;
	public static final int MOUSE_BUTTON_2 = 7;
	public static final int MOUSE_BUTTON_3 = 8;
	
	private static final int NUM_MOUSE_CODES = 9;
	private static final int NUM_KEY_CODES = 600;
	
	private GameAction[] keyActions = new GameAction[NUM_KEY_CODES];
	private GameAction[] mouseActions = new GameAction[NUM_MOUSE_CODES];
	
	private Point mouseLocation;
	private Point centerLocation;
	private Component comp;
	private Robot robo;
	private boolean isRecentering;
	
	public InputManager(Component comp0){
		comp = comp0;
		mouseLocation = new Point();
		centerLocation = new Point();
		
		//register the listeners
		comp.addKeyListener(this);
		comp.addMouseListener(this);
		comp.addMouseMotionListener(this);
		comp.addMouseWheelListener(this);
		
		//permite TAB e outras keys do sistema
		comp.setFocusTraversalKeysEnabled(false);
	}
	
	public void setCursor(Cursor cursor){
		comp.setCursor(cursor);
	}
	
	public void setRelativeMouseMode(Boolean mode){
		if(mode == isRelativeMouseMode()){
			return;
		}
		if(mode){
			try{
				robo = new Robot();
				recenterMouse();
			}catch(AWTException e){
				robo = null;
			}
		}else{
			robo = null;
		}
	}
	
	public boolean isRelativeMouseMode(){
		return (robo != null);
	}
	
	/**
	 * Mapeia uma game action a uma key. Os keycodes est�o em java.awt.KeyEvent.
	 * Se a key ja possui uma action a nova action sobreescreve a anterior.
	 */
	public void mapToKey(GameAction action, int keyCode){
		keyActions[keyCode] = action;
	}
	
	/**
	 * Mapeia uma a��o para o mouse. Os codigos do mouse est�o definidos nesta
	 * classe.
	 */
	public void mapToMouse(GameAction action, int mouseCode){
		mouseActions[mouseCode] = action;
	}
	
	public void clearMap(GameAction gameAction){
		for(int i=0; i<keyActions.length; i++){
			if(keyActions[i] == gameAction){
				keyActions[i] = null;
			}
		}
		for(int i=0; i<mouseActions.length; i++){
			if(mouseActions[i] == gameAction){
				mouseActions[i] = null;
			}
		}
		gameAction.reset();
	}
	
	public List<String> getMaps(GameAction gameCode){
		ArrayList<String> list = new ArrayList<String>();
		
		for(int i=0; i<keyActions.length; i++){
			if(keyActions[i] == gameCode){
				list.add(getKeyName(i));
			}
		}
		
		for(int i=0; i<mouseActions.length; i++){
			if(mouseActions[i] == gameCode){
				list.add(getMouseName(i));
			}
		}
		return list;
	}
	
	public static String getKeyName(int KeyCode){
		return KeyEvent.getKeyText(KeyCode);
	}
	
	public static String getMouseName(int mouseCode){
		switch(mouseCode){
			case MOUSE_MOVE_LEFT: return "Mouse Left";
			case MOUSE_MOVE_RIGHT: return "Mouse Right";
			case MOUSE_MOVE_UP: return "Mouse Up";
			case MOUSE_MOVE_DOWN: return "Mouse Down";
			case MOUSE_WHEEL_UP: return "Mouse Wheel Up";
			case MOUSE_WHEEL_DOWN: return "Mouse Wheel Down";
			case MOUSE_BUTTON_1: return "Mouse Button 1: left";
			case MOUSE_BUTTON_2: return "Mouse Button 2: center";
			case MOUSE_BUTTON_3: return "Mouse Button 3: right";
			default: return "Unknown mouse code "+ mouseCode;
		}
	}
	
	public int getMouseX(){
		return mouseLocation.x;		
	}
	
	public int getMouseY(){
		return mouseLocation.y;
	}
	
	private synchronized void recenterMouse(){
		if(robo != null && comp.isShowing()){
			centerLocation.x = comp.getWidth()/2;
			centerLocation.y = comp.getHeight()/2;
			SwingUtilities.convertPointToScreen(centerLocation, comp);
			isRecentering = true;
			robo.mouseMove(centerLocation.x, centerLocation.y);
		}
	}
	
	private GameAction getKeyAction(KeyEvent e){
		int keyCode = e.getKeyCode();
		if(keyCode < keyActions.length){
			return keyActions[keyCode];
		}else{
			return null;
		}
	}
	
	public static int getMouseButtonCode(MouseEvent e){
		switch(e.getButton()){
			case MouseEvent.BUTTON1:
				return MOUSE_BUTTON_1;
			case MouseEvent.BUTTON2:
				return MOUSE_BUTTON_2;
			case MouseEvent.BUTTON3:
				return MOUSE_BUTTON_3;
			default:
				return -1;
		}
	}
	
	private GameAction getMouseButtonAction(MouseEvent e){
		int mouseCode = getMouseButtonCode(e);
		if(mouseCode != -1){
			return mouseActions[mouseCode];
		}else{
			return null;
		}
	}
	
	//da interface keyListener
	public void keyPressed(KeyEvent e){
		GameAction action = getKeyAction(e);
		if(action != null){
			action.press();
		}
		e.consume(); // para a key nao ser processada para mais nada
	}
	
	//da interface keyListener
	public void keyReleased(KeyEvent e){
		GameAction action = getKeyAction(e);
		if(action != null){
			action.release();
		}
		e.consume();
	}
	
	//da interface keyListener
	public void keyTyped(KeyEvent e){
		e.consume();
	}
	
	//da interface mouseListener
	public void mousePressed(MouseEvent e){
		GameAction action = getMouseButtonAction(e);
		if(action != null){
			action.press();
		}
	}
	
	//da interface mouseListener
	public void mouseReleased(MouseEvent e){
		GameAction action = getMouseButtonAction(e);
		if(action != null){
			action.release();
		}
	}
	
	//da interface mouseListener
	public void mouseClicked(MouseEvent e){
		//nada
	}
	
	//da interface mouseListener
	public void mouseEntered(MouseEvent e){
		mouseMoved(e);
	}
	
	//da interface mouseListener
	public void mouseExited(MouseEvent e){
		mouseMoved(e);
	}
	
	//da interface mouseListener
	public void mouseDragged(MouseEvent e){
		mouseMoved(e);
	}
	
	//da interface mouseMotionListener
	public void mouseMoved(MouseEvent e){
		if(isRecentering && centerLocation.x == e.getX() && centerLocation.y == e.getY()){
			isRecentering = false;
		}else{
			int dx = e.getX() - centerLocation.x;
			int dy = e.getY() - centerLocation.y;
			mouseHelper(MOUSE_MOVE_LEFT, MOUSE_MOVE_RIGHT, dx);
			mouseHelper(MOUSE_MOVE_UP, MOUSE_MOVE_DOWN, dy);
			
			if(isRelativeMouseMode()){
				recenterMouse();
			}
		}
		
		mouseLocation.x = e.getX();
		mouseLocation.y = e.getY();
	}
	
	//da interface mouseWheelListener
	public void mouseWheelMoved(MouseWheelEvent e){
		mouseHelper(MOUSE_WHEEL_UP, MOUSE_WHEEL_DOWN, e.getWheelRotation());
	}
	
	private void mouseHelper(int cnegative, int cpositive, int amount){
		GameAction action;
		if(amount < 0){
			action = mouseActions[cnegative];
		}else{
			action = mouseActions[cpositive];
		}
		if(action != null){
			action.press(Math.abs(amount));
			action.release();
		}
	}
}
