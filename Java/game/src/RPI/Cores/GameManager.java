package RPI.Cores;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Window;

public abstract class GameManager {
	long currTime = 0, startTime = 0;
	protected ScreenManager screen;
	private boolean Running;
	protected final int DEFAULT_FONT_SIZE = 24;
	protected final Font DEFAULT_FONT = new Font("Dialog",Font.PLAIN,DEFAULT_FONT_SIZE);
	
	public GameManager(){
	}
	
	public void stop(){
		Running = false;
	}
	
	public void run(){
		try{
			init();
			gameLoop();
		}finally{
			screen.restoreScreen();
		}
	}
	
	private void init(){
		screen = new ScreenManager();
		screen.setFullScreen(screen.getCompatibleMode());
		
		Window w = screen.getScreen();
		w.setFont(new Font("Dialog",Font.PLAIN,DEFAULT_FONT_SIZE));
		w.setBackground(Color.black);
		w.setForeground(Color.green);
		
		//initialize pros gauchos
		initialize();
		
		Running = true;
	}
	
	private void gameLoop(){
		//timers
		startTime = System.currentTimeMillis();
		currTime = startTime;
    
		while(Running){
			
			long elapsedTime = System.currentTimeMillis() - currTime;
			currTime += elapsedTime;
			
			//update the game
			update(elapsedTime);
			
			//draw to the screen
			Graphics2D g = screen.getGraphics();
			draw(g);
			g.dispose();
			screen.update();
			
			//libera tempo
			try{
				Thread.sleep(10);
			}catch(InterruptedException e){}
		}
		
	}
	
	//Override
	public abstract void update(long elapsedTime);
	
	//Override
	public abstract void draw(Graphics g);
	
	//Override
	public abstract void initialize();
}
