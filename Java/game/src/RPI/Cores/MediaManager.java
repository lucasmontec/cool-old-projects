package RPI.Cores;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class MediaManager {
	
	//construtor
	public MediaManager(){
		
	}
	
	//imageloader
	//Image
	public Image loadImage(String filepath){
		try{
			return new ImageIcon(filepath).getImage();
		} catch(Exception e){
			return null;
		}
	}
	//Buffered Image
	public BufferedImage loadBufferedImage(String arquivo) {
		try {
			return ImageIO.read( new File( arquivo ) );
		} catch (Exception e) {
			return null;
		}
	}
	
	//salva imagem
	public int saveImage(BufferedImage img, String format, String fileName){
		// BufferedImage to File
		try {
			ImageIO.write( img, format /* "png" "jpeg" "jpg" "gif" */,new File ( fileName+"."+format ) /* target */ );
			return 0;
		} catch (Exception e) {
			return 1;
		}
	}
	
	//--------filtering images(BufferedOnly)
	
	//Tint:
	public BufferedImage tint(BufferedImage img, float r,float g,float b){
		BufferedImage dest = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		int px_color = 0;
		for(int x=0;x<img.getWidth();x++){
			for(int y=0;y<img.getWidth();y++){
				px_color = img.getRGB(x, y);
			    int a = ((px_color >> 24) & 0xff); 
			    int red = ((px_color >> 16) & 0xff); 
			    int green = ((px_color >> 8) & 0xff); 
			    int blue = ((px_color) & 0xff); 

			    red = (int) (red + r > 255 ? 255 : red + r);
			    green = (int) (green + g > 255 ? 255 : green + g);
			    blue = (int) (blue + b > 255 ? 255 : blue + b);

			    px_color = (a << 24) | (red << 16) | (green << 8) | blue;
				dest.setRGB(x, y, px_color);
			}
		}
		return dest;
	}
	
	//Curves
	public BufferedImage curves(BufferedImage img, float r,float g,float b){
		BufferedImage dest = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		int px_color = 0;
		for(int x=0;x<img.getWidth();x++){
			for(int y=0;y<img.getWidth();y++){
				px_color = img.getRGB(x, y);
			    int a = ((px_color >> 24) & 0xff); 
			    int red = ((px_color >> 16) & 0xff); 
			    int green = ((px_color >> 8) & 0xff); 
			    int blue = ((px_color) & 0xff); 

			    red = (int) (red * r > 255 ? 255 : red * r);
			    green = (int) (green * g > 255 ? 255 : green * g);
			    blue = (int) (blue * b > 255 ? 255 : blue * b);

			    px_color = (a << 24) | (red << 16) | (green << 8) | blue;
				dest.setRGB(x, y, px_color);
			}
		}
		return dest;
	}
	
	//Blur:
	public BufferedImage blur(BufferedImage img, float radius){
		BufferedImage dest =  new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		float n = 1.0f / radius;
		//float n = 1.0f / 9.0f;
		float [] blurKernel =
		{
		   n, n, n,
		   n, n, n,
		   n, n, n
		} ;
        Kernel kernel = new Kernel(3, 3, blurKernel);
        ConvolveOp op = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP,null);
        op.filter(img, dest);
        return dest;
	}
}
