package RPI.Cores;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;


public class ScreenManager {
	
	private GraphicsDevice device;
	
	public static final DisplayMode DefaultDisplaySet[]={
		//640x480
		new DisplayMode(640,480,32,DisplayMode.REFRESH_RATE_UNKNOWN),
		new DisplayMode(640,480,16,DisplayMode.REFRESH_RATE_UNKNOWN),
		//800x600
		new DisplayMode(800,600,32,DisplayMode.REFRESH_RATE_UNKNOWN),
		new DisplayMode(800,600,16,DisplayMode.REFRESH_RATE_UNKNOWN),
		//1024x768
		new DisplayMode(1024,768,32,DisplayMode.REFRESH_RATE_UNKNOWN),
		new DisplayMode(1024,768,16,DisplayMode.REFRESH_RATE_UNKNOWN),
		//1280x1024
		new DisplayMode(1024,768,32,DisplayMode.REFRESH_RATE_UNKNOWN),
		new DisplayMode(1024,768,16,DisplayMode.REFRESH_RATE_UNKNOWN),
		//1440x900
		new DisplayMode(1440,900,32,DisplayMode.REFRESH_RATE_UNKNOWN),
		new DisplayMode(1440,900,16,DisplayMode.REFRESH_RATE_UNKNOWN)
	};
	
	//construtor
	public ScreenManager(){
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		device = ge.getDefaultScreenDevice();
	}
	
	//lista de display modes possiveis
	public DisplayMode[] getCompatibleDisplaymodes(){
		return device.getDisplayModes();
	}
	
	//pega o mode atual
	public DisplayMode getDefaultDisplayMode(){
		return device.getDisplayMode();
	}
	
	//compara displays
	public boolean displayModesMatch(DisplayMode d1, DisplayMode d2){
		//checa padroes de refres rate e bitdepth
		if(d1.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN ||
		   d2.getRefreshRate() == DisplayMode.REFRESH_RATE_UNKNOWN){
			return false;
		}
		if(d1.getBitDepth() == DisplayMode.BIT_DEPTH_MULTI ||
		   d2.getBitDepth() == DisplayMode.BIT_DEPTH_MULTI ||
		   d1.getBitDepth() != d2.getBitDepth()){
			return false;
		}
		//compara o tamannho
		if(d1.getHeight() != d2.getHeight() || d1.getWidth() != d2.getWidth()){
			return false;
		}
		
		return true;
	}
	
	//pega o primeiro displaymode compativel da lista padrao
	public DisplayMode getFirstCompatibleDisplayMode(){
		DisplayMode[] compModes = this.getCompatibleDisplaymodes();
		for(int i=0;i<compModes.length;i++){
			if(displayModesMatch(DefaultDisplaySet[i],compModes[i])){
				return DefaultDisplaySet[i];
			}
		}
		return null;
	}
	
	//checa a compatibilidade de um displaymode
	public boolean isCompatibleMode(DisplayMode mode){
		DisplayMode[] compModes = this.getCompatibleDisplaymodes();
		for(int i=0;i<compModes.length;i++){
			if(displayModesMatch(mode,compModes[i])){
				return true;
			}
		}
		return false;
	}
	
	//pega um display mode legal
	public DisplayMode getCompatibleMode(){
		//prioridade para o displaymode default sempre!
		DisplayMode defaultMode = this.getDefaultDisplayMode();
		
		//checa se o default vai servir
		if(defaultMode != null && isCompatibleMode(defaultMode)){
			return defaultMode;
		}else{
			//caso nao sirva pega um display mode compat�vel da lista dos padr�es
			return getFirstCompatibleDisplayMode();
		}
	}
	
	//seta a frame fullscreen
	public void setFullScreen(DisplayMode dm){
		JFrame frame = new JFrame();
		frame.setUndecorated(true);
		frame.setIgnoreRepaint(true);
		frame.setResizable(false);
		
		device.setFullScreenWindow(frame);
		if(dm != null && device.isDisplayChangeSupported()){
			try{
				device.setDisplayMode(dm);
			}catch(Exception e){}
		}
		
		if(frame.getBufferStrategy() == null){
			frame.createBufferStrategy(2);
		}
	}
	
	//retorna os graficos 2d sendo renderizados
	public Graphics2D getGraphics(){
		Window w = device.getFullScreenWindow();
		if(w != null){
			BufferStrategy bf = w.getBufferStrategy();
			return (Graphics2D)bf.getDrawGraphics();
		}else{
			return null;
		}
	}
	
	//metodo da atualiza��o da tela
	public void update(){
		Window window = device.getFullScreenWindow();

		if(window != null){
			BufferStrategy bs = window.getBufferStrategy();
			if(!bs.contentsLost()){
				bs.show();
			}
		}
		
		//segundo meu incrivel livro o linux tem uns bugs aqui...
		//vamos corrigilos
		Toolkit.getDefaultToolkit().sync();
	}
	
	//retornando informa��es �teis
	public int getWidth(){
		Window w = device.getFullScreenWindow();
		if(w != null){
			return w.getWidth();
		}else{
			return 0;
		}
	}
	
	public int getHeight(){
		Window w = device.getFullScreenWindow();
		if(w != null){
			return w.getHeight();
		}else{
			return 0;
		}
	}
	
	//destroi a tela fullscreen
	public void restoreScreen(){
		Window w = device.getFullScreenWindow();
		if(w != null){
			w.dispose();
		}
		device.setFullScreenWindow(null);
	}
	
	//pega full screen
	public Window getScreen(){
		if(device.getFullScreenWindow() != null){
			return device.getFullScreenWindow();
		}else{
			return null;
		}
	}
	
	//extra image
	public BufferedImage createCompatibleImage(int w, int h, int transparency){
		Window frame = device.getFullScreenWindow();
		if(frame != null){
			GraphicsConfiguration gc = frame.getGraphicsConfiguration();
			return gc.createCompatibleImage(w, h, transparency);
		}
		return null;
	}
}
