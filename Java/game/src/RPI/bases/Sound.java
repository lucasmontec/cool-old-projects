package RPI.bases;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import RPI.Cores.SoundManager;

public class Sound extends SoundManager{

	private int currentFrame = 0;
	Clip sound;
	
	public Sound(){
	}
	
	public Sound(String path){
		sound = loadFromFile(new File(path));  
	}
	
	public Clip loadSound(String path){
		sound = loadFromFile(new File(path));
		return null;
	}
	
	private Clip loadFromFile(File f){
		Clip snd;
		try { 
            File audioFile = f;
            soundStream = AudioSystem.getAudioInputStream( audioFile );
            AudioFormat streamFormat = soundStream.getFormat( );
            DataLine.Info clipInfo = new DataLine.Info( Clip.class, streamFormat );
 
            snd = ( Clip ) AudioSystem.getLine( clipInfo );
            snd.open( soundStream );
            return snd;
        }
        catch ( UnsupportedAudioFileException e ) { return null; }
        catch ( IOException e ) { return null; }
        catch ( LineUnavailableException e ) { return null; }
	}
	
	public void start(){
		sound.setFramePosition(0);
		sound.start();
	}
	
	public void play(){
		sound.setFramePosition(currentFrame);
		sound.start();
	}
	
	public void pause(){
		currentFrame = sound.getFramePosition();
		sound.stop();
	}
	
	public void stop(){
		currentFrame = 0;
		sound.stop();
	}
	
	public void loop(int times){
		sound.loop(times);
	}
	
	public void loopInf(){
		sound.loop(Clip.LOOP_CONTINUOUSLY);
	}
}
