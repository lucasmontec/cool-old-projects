import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;


public class janela {
	static JFrame frame = new JFrame("Image analyzer");
	static webcam wc = new webcam("vfw://0");
	static ImagePanel ip = new ImagePanel();
	static motionAnalyzer ma = new motionAnalyzer();
	
	public static void main(String args[]){
		ma.DrawCenter(true);
		ma.DrawBoundaries(true);
		frame.setSize(600, 500);
		frame.setLayout(new FlowLayout());
		frame.add(ip);
		frame.setVisible(true);
		
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				wc.close();
				System.exit(0);
			}
		});
		
		while(true){
			try{
				ip.setImage(ma.analyzeImage(wc.getImage()));
			}catch(Exception e) {}
		}
	}
}
