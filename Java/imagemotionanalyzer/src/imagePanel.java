import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

class ImagePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	public BufferedImage img = null;

	public void setImage(BufferedImage img)
	{
		this.img = img;
	    if(img != null && (this.getWidth() != img.getWidth() || this.getHeight() != img.getHeight())){
			Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		    setPreferredSize(size);
		    setSize(size);
	    }
		this.repaint();
		this.revalidate();
	}

	public void paintComponent(Graphics g)
	{
		if (img != null)
		{
			g.drawImage(img, 0, 0, this);
		}
	}
}