import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class motionAnalyzer {
	private static final long serialVersionUID = 1L;

	private ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
	private BufferedImage img;
	private double motionFactor = 0.4;
	private Color bgColor = Color.black;
	private Color motionColor = Color.white;
	private boolean drawCenter = false;
	private boolean drawBoundaries = false;
	
	public BufferedImage analyzeImage(BufferedImage i){
		if(i != null){
			if(images.size() < 2){
				images.add(i);
			}else{
				images.remove(0);
			}
		    if(!images.isEmpty() && images.size() > 1 && images.get(0) != null && images.get(1) != null){
		    	img = compareImages(images.get(0),images.get(1));
		    }
		    return img;
		}else{
			return null;
		}
	}
	
	//expand info
	private BufferedImage compareImages(BufferedImage imga,BufferedImage imgb){
		if(imga == null || imgb == null){return null;}
		if(imga.getWidth(null) != imgb.getWidth(null) || imga.getHeight(null) != imga.getHeight(null)){return null;}
		
		int w = imga.getWidth(null);
		int h = imga.getHeight(null);
		
		int px_colora = 0, px_colorb =0, px_color;
		BufferedImage newimg = new BufferedImage(w, h, imga.getType());
		int lx=w,ly=h,hx=0,hy=0;
		int[] center = new int[2];
		
		for(int x=0;x<w;x++){
			for(int y=0;y<h;y++){
				px_colora = imga.getRGB(x, y);
				px_colorb = imgb.getRGB(x, y);
			    
				px_colora = (((px_colora >> 16) & 0xff) + ((px_colora >> 8) & 0xff) + ((px_colora) & 0xff))/3;
			    px_colorb = (((px_colorb >> 16) & 0xff) + ((px_colorb >> 8) & 0xff) + ((px_colorb) & 0xff))/3;
			    
			    if(Math.abs((float)px_colorb-(float)px_colora)/255 > this.motionFactor){
			    	px_color = this.motionColor.getRGB();
			    	//grab the motion edges
					if(x < lx){
						lx = x;
					}
					if(x > hx){
						hx = x;
					}
					if(y < ly){
						ly = y;
					}
					if(y > hy){
						hy = y;
					}
			    }else{
			    	px_color = this.bgColor.getRGB();
			    }
			    
				newimg.setRGB(x, y, px_color);
			}
		}
		
		if(drawCenter){
			center[0] = (lx+hx)/2;
			center[1] = (ly+hy)/2;
			//draw center
			for(int i =-6;i<6;i++){
				newimg.setRGB(center[0]+i, center[1]+i, Color.green.getRGB());
			}
			for(int i =-6;i<6;i++){
				newimg.setRGB(center[0]-i, center[1]+i, Color.green.getRGB());
			}	
		}
		
		if(drawBoundaries){
			for(int i=lx;i<hx;i++){
				newimg.setRGB(i, ly, Color.green.getRGB());
			}
			for(int i=ly;i<hy;i++){
				newimg.setRGB(lx, i, Color.green.getRGB());
			}
			for(int i=lx;i<hx;i++){
				newimg.setRGB(i, hy, Color.green.getRGB());
			}
			for(int i=ly;i<hy;i++){
				newimg.setRGB(hx, i, Color.green.getRGB());
			}
		}
		
		return newimg;
	}

	public void DrawCenter(boolean draw){
		drawCenter = draw;
	}
	
	public void setMotionFactor(double motionFactor) {
		this.motionFactor = motionFactor;
	}

	public double getMotionFactor() {
		return motionFactor;
	}

	public void setBackgroundColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	public void setMotionColor(Color motionColor) {
		this.motionColor = motionColor;
	}

	public void DrawBoundaries(boolean drawBoundaries) {
		this.drawBoundaries = drawBoundaries;
	}

}