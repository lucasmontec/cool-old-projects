
public class Feromone {
	public enum FeromoneKind{ FOOD, WORK, WAR };
	
	private FeromoneKind kind;
	private int intensity;
	
	public void setIntensity(int intensity) {
		this.intensity = intensity;
	}
	public int getIntensity() {
		return intensity;
	}
	public void setKind(FeromoneKind kind) {
		this.kind = kind;
	}
	public FeromoneKind getKind() {
		return kind;
	}
	
	
}