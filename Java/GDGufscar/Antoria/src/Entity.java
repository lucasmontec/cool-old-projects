import java.awt.Color;
import java.awt.Point;

import com.lucas.SimpleGame.GameObject;


public class Entity extends GameObject {

	protected Point position;
	protected int size;
	protected Color color;
	
	public void setColor(Color c){
		color = c;
	}
	
	public void setColor(int r, int g, int b){
		color = new Color(r,g,b);
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setSize(int s){
		if(s > 0){
			size = s;
		}
		s = 1;
	}
	
	public int getSize(){
		return size;
	}
	
	public void setPosition(int x, int y){
		position = new Point(x,y);
	}
	
	public void setPosition(Point p){
		if(p != null){
			position = p;
		}
	}
	
	public void move(int x, int y){
		position.x += x;
		position.y += y;
	}
	
	public Point getPosition(){
		return position;
	}
	
	public int getDistanceTo(Entity e){
		if(e != null){
			int dist;
			dist = (int) Math.sqrt(
					Math.pow((e.getPosition().x-position.x), 2)+
					Math.pow((e.getPosition().y-position.y), 2)
			);
			return dist;
		}else{
			return 0;
		}
	}
	
}
