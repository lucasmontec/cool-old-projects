package com.lucas.SimpleGame;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

import javax.swing.JOptionPane;

public abstract class DoubleBufferedCanvas extends Canvas {
	private static final long serialVersionUID = 8503349736297787253L;
	
	public DoubleBufferedCanvas(){
		this.setIgnoreRepaint(true);
	}
	
	public void prepareCanvas(){
		this.createBufferStrategy(2);
	}
	
	public void innerDraw(){
		//Grab this component buffer strategy
		BufferStrategy bufferStr = this.getBufferStrategy();
		//Prepare a graphics to use
		Graphics g = null;
		
		//Try to get the graphics
		try{
			//Get it
			g = bufferStr.getDrawGraphics();
			
			//Set the default color
			g.setColor(Color.black);
			
			//Clear the background
			g.clearRect(0, 0, this.getWidth(), this.getHeight());
			
			//Draw stuff on the graphics
			draw(g);
			
		}catch(Exception e){
			//Error if buffer strategy fails
			JOptionPane.showMessageDialog(null, "Error on creating double buffer","ERROR",JOptionPane.ERROR_MESSAGE);
		}finally{
			//Dispose our graphics for drawing
			g.dispose();
		}
		
		//Switch buffers
		if(!bufferStr.contentsLost())
			bufferStr.show();
		
		//Sync the drawing with the system to avoid delays
		Toolkit.getDefaultToolkit().sync();
	}
	
	//Override this function to draw stuff
	public abstract void draw(Graphics g);
}
