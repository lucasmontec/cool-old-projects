package com.lucas.SimpleGame;

import java.awt.Graphics;

public abstract class Game extends DoubleBufferedCanvas implements Runnable{
	private static final long serialVersionUID = 1L;

	private boolean running;
	
	public Game(){
		running = true;
	}
	
	public abstract void initialize();
	
	public abstract void update(GameTime time);
	
	@Override
	public abstract void draw(Graphics g);

	public void stop(){
		running = false;
	}
	
	@Override
	public void run( ) {
		//A variable to store how much time passed between iterations
		GameTime gtime = new GameTime();
		
		//Call the initialization
		initialize();
		
		//Prepare the drawing canvas
		prepareCanvas();
		
		//Run the game
		while(running){
			
			//Get the time before the iteration
			gtime.calculateElapsedTimeInMillis();
			
			//Update our logic
			update(gtime);
			
			//Draw our game
			innerDraw();
		}
		
	}

}
