package com.lucas.SimpleGame;

import java.util.Calendar;

public class GameTime {

	Calendar cal;
	private long elpasedTime;
	
	public GameTime(){
		cal = Calendar.getInstance();
		elpasedTime = System.currentTimeMillis();
	}
	
	public long calculateElapsedTimeInMillis(){
		elpasedTime = System.currentTimeMillis() - elpasedTime;
		return elpasedTime;
	}
	
	public long nanoTime(){
		return System.nanoTime();
	}
	
	public long millis(){
		return System.currentTimeMillis();
	}
	
	public long seconds(){
		return millis()/1000;
	}
	
	public long getCurrentSecond(){
		return cal.get(Calendar.SECOND);
	}
	
	public long getCurrentMinute(){
		return cal.get(Calendar.MINUTE);
	}
	
	public long getCurrentHour(){
		return cal.get(Calendar.HOUR);
	}
}
