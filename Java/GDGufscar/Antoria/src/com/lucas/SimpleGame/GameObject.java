package com.lucas.SimpleGame;

enum GameObjectState{ NORMAL , IGNORE_ON_DRAW , IGNORE_ON_UPDATE, DELETE_WHEN_POSSIBLE };

public class GameObject {
	private GameObjectState gstate = GameObjectState.NORMAL;
	private String OBJECTDESCRIPTION;
	
	public GameObject(){
	}
	
	public GameObject(GameObjectState state){
		if(state != null)
			gstate = state;
	}
	
	public GameObject(String desc){
		if(desc != null)
			OBJECTDESCRIPTION = desc;
	}
	
	public GameObject(GameObjectState state, String desc){
		if(state != null)
			gstate = state;
		if(desc != null)
			OBJECTDESCRIPTION = desc;
	}
	
	public GameObjectState getState(){
		return gstate;
	}
	
	public String getGameObjectDescription(){
		return OBJECTDESCRIPTION;
	}
	
	public void setGameObjectDescription(String s){
		if(s != null)
			OBJECTDESCRIPTION = s;
	}
	
	public String toString(){
		if(gstate.equals(GameObjectState.NORMAL)){
			return "GAMEOBJECT: "+OBJECTDESCRIPTION+" : NORMAL";
		}
		if(gstate.equals(GameObjectState.IGNORE_ON_UPDATE)){
			return "GAMEOBJECT: "+OBJECTDESCRIPTION+" : IGNORE_ON_UPDATE";
		}
		if(gstate.equals(GameObjectState.IGNORE_ON_DRAW)){
			return "GAMEOBJECT: "+OBJECTDESCRIPTION+" : IGNORE_ON_DRAW";
		}
		if(gstate.equals(GameObjectState.DELETE_WHEN_POSSIBLE)){
			return "GAMEOBJECT: "+OBJECTDESCRIPTION+" : DELETE_WHEN_POSSIBLE";
		}
		return null;
	}
}
