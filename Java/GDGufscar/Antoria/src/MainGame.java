import javax.swing.JFrame;


public class MainGame {

	static JFrame mainWindow = new JFrame("Antoria");
	
	public static void main(String... args){
		mainWindow.setSize(500,500);
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		AntsGame game = new AntsGame();
		game.setSize(mainWindow.getSize());
		mainWindow.add(game);
		
		mainWindow.setVisible(true);
		
		Thread gameThread = new Thread(game);
		gameThread.start();
	}
}
