import java.awt.Color;
import java.awt.Graphics;

import com.lucas.SimpleGame.Game;
import com.lucas.SimpleGame.GameTime;


public class AntsGame extends Game{
	private static final long serialVersionUID = 1L;

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.green);
		g.drawLine(0, 0, this.getWidth(), this.getHeight());
	}

	@Override
	public void update(GameTime time) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

}
