import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;


public class ServerMain implements Runnable{

	//Variables
	LinkedList<SocketChannel> clients = new LinkedList<SocketChannel>();
	ByteBuffer writeBuffer = ByteBuffer.allocateDirect(255);
	ByteBuffer readBuffer = ByteBuffer.allocateDirect(255);
	
	ServerSocketChannel ssc;
	Selector readSelector;
	
	public static final int PORT = 10996;
	
	private void initializeServer(){
		try{
			ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			
			InetAddress addr = InetAddress.getLocalHost();
			ssc.socket().bind(new InetSocketAddress(addr,PORT));
			
			readSelector = Selector.open();
		}catch(Exception e){
			
		}
		
	}
	
	private void acceptNewConnections(){
		try{
			
			SocketChannel clientChannel;
			while((clientChannel = ssc.accept()) != null){
				addNewClient(clientChannel);
				Broadcast("Login["+clientChannel.socket().getInetAddress()+"]",clientChannel);
				
				sendMessage(clientChannel, "Welcome!");
			}
		}catch(IOException e){}
		catch(Exception e){}
	}
	
	private void addNewClient(SocketChannel client){
		clients.add(client);
		
		try{
			
			client.configureBlocking(false);
			SelectionKey readKey = client.register(readSelector,
					SelectionKey.OP_READ, new StringBuffer());
			
		}catch(ClosedChannelException cce){}
		catch(IOException e){}
	}
	
	private void sendMessage(SocketChannel chan, String msg){
		prepareWriteBuffer(msg);
		channelWrite(chan,writeBuffer);
	}
	
	private void Broadcast(String msg, SocketChannel origin){
		prepareWriteBuffer(msg);
		Iterator<SocketChannel> i = clients.iterator();
		while(i.hasNext()){
			SocketChannel channel = (SocketChannel)i.next();
			if(channel != origin)
				channelWrite(channel,writeBuffer);
		}
	}
	
	private void prepareWriteBuffer(String msg){
		writeBuffer.clear();
		writeBuffer.put(msg.getBytes());
		writeBuffer.putChar('\n');
		writeBuffer.flip();
	}
	
	private void channelWrite(SocketChannel channel, ByteBuffer writeBuff){
		long nbytes = 0;
		long toWrite = writeBuff.remaining();
		
		try{
			
			while(nbytes != toWrite){
				nbytes += channel.write(writeBuff);
				
				try{
					Thread.sleep(100);
				}
				catch(InterruptedException ie){}
			}
			
		}
		catch(ClosedChannelException cce){}
		catch(Exception e){}
		
		writeBuff.rewind();
	}
	
	private void readIncomeMessages(){
		try{
			readSelector.selectNow();
			Set<SelectionKey> readyKeys = readSelector.selectedKeys();
			Iterator<SelectionKey> i = readyKeys.iterator();
			while(i.hasNext()){
				SelectionKey key = (SelectionKey)i.next();
				i.remove();
				SocketChannel channel = (SocketChannel)key.channel();
				readBuffer.clear();
				long nbytes = channel.read(readBuffer);
				if(nbytes == -1){
					Broadcast("IP["+
							channel.socket().getInetAddress()+
							"]: Lost connection", channel);
					channel.close();
					clients.remove(channel);
				}else{
					StringBuffer sb = (StringBuffer)key.attachment();
					readBuffer.flip();
					String str = readBuffer.toString();
					readBuffer.clear();
					sb.append(str);
					
					String line = sb.toString();
					if((line.indexOf("\n") != -1) || (line.indexOf("\r") != -1)){
						line.trim();
					}
					
					if(line.startsWith("quit")){
						channel.close();
						clients.remove(channel);
						Broadcast("IP["+
								channel.socket().getInetAddress()+
								"]: Lost connection", channel);
					}else{
						Broadcast("IP["+channel.socket().getInetAddress()+"]: "+line,channel);
						sb.delete(0, sb.length());
					}
					
				}
			}
		}catch(IOException ioe){}
		catch(Exception e){}
	}
	
	public static void main(String... args){
		
		ServerMain smain = new ServerMain();
		smain.initializeServer();
		smain.run();
	}
	
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
