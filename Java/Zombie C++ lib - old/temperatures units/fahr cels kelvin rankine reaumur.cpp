#include <stdio.h>

//1
double FToCels(double fahr)
{
if (fahr)
{
return((fahr - 32)/1.8);       
}else{
return 0;      
}
}

//2
double CToFahr(double cels)
{
if (cels)
{
return((cels*1.8)+32);       
}else{
return 0;      
}
}

//3
double FToKel(double fahr)
{
if (fahr)
{
return((fahr+459.67)/1.8);       
}else{
return 0;      
}
}

//4
double KToFahr(double kel)
{
if (kel)
{
return((kel*1.8)-459.67);       
}else{
return 0;      
}
}

//5
double RToFahr(double rank)
{
if (rank)
{
return(rank-459.67);       
}else{
return 0;      
}
}

//6
double FToRank(double fahr)
{
if (fahr)
{
return(fahr+459.67);       
}else{
return 0;      
}
}

//7
double FToRea(double fahr)
{
if (fahr)
{
return((fahr-32)/2.25);       
}else{
return 0;      
}
}

//8
double RToFahr(double rea)
{
if (rea)
{
return((rea*2.25)+32);       
}else{
return 0;      
}
}
