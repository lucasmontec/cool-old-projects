#include <stdio.h>
#include <conio.h>
double number;

double GToRad(double ang)
{
 double f;
 if (ang){
 f = ang*(3.1415926/200);
 return f; 
}else{
return 0;
     }   
}

double DToRad(double ang)
{
 double f;
 if (ang){
 f = ang*(3.1415926/180);
 return f; 
}else{
return 0;
     }   
}

double RToDeg(double ang)
{
  double f;
  if (ang){
   f = (ang*180)/3.1415926;      
   return f;        
  }else{
   return 0;     
  }     
}

double GToDeg(double ang)
{
  double f;
  if (ang){
   f = (ang/400)*360;      
   return f;        
  }else{
   return 0;     
  }     
}

double DToGrad(double ang)
{
 double f;
 if (ang){
   f = (ang/360)*400;
   return f;       
}else{
    return 0;            
     }      
}

double RToGrad(double ang)
{
 double f;
 if (ang){
   f = (ang*200)/3.1415926;
   return f;       
}else{
    return 0;            
     }         
}

int main(void)
{
 puts("Digite o angulo em graus:");
 scanf("%lf", &number);
 printf("\n -funcao: DToRad(double angle) \"De graus para radianos\": %1.5lf \n\n",DToRad(number));
 printf(" -funcao: RToDeg(double angle) \"De rad para graus\": %1.5lf \n\n",RToDeg(DToRad(number)));
 printf(" -funcao: DToGrad(double angle) \"De graus para grados\": %1.5lf \n\n",DToGrad(number));
 printf(" -funcao: RToGrad(double angle) \"De rad para grados\": %1.5lf \n\n",RToGrad(DToRad(number)));
 printf(" -funcao: GToDeg(double angle) \"De grados para graus\": %1.5lf \n\n",GToDeg(DToGrad(number)));
 printf(" -funcao: GToRad(double angle) \"De grados para graus\": %1.5lf \n\n",GToRad(DToGrad(number)));
 puts("Todas as conversoes foram feitas apartir do seu angulo\n    de entrada assumindo o mesmo em GRAUS.\n     As conversoes de numeros diferentes\n     de graus como Radianos para Grados\n       foram RE-CONVERSOES do tipo:\n\n        \"RToGrad(DToRad(number))\"");
 getch();
}
