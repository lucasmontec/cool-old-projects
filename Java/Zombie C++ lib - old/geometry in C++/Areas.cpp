#include <stdio.h>
#include <conio.h>

double SqrArea(double side)
{
   if(side){
   return side*side;       
   }else{
   return 0;      
   }    
}

double RectArea(double base, double hight)
{
   double Base, Hight;
   if(base && hight)
   {
   if (base<0){Base = -base;}else{Base = base;}
   if (hight<0){Hight = -hight;}else{Hight = hight;}
    return(Base*Hight);    
   }else{
   return 0;   
   }       
}

double TriArea(double base, double hight)
{
   double Base, Hight;
   if(base && hight)
   {
   if (base<0){Base = -base;}else{Base = base;}
   if (hight<0){Hight = -hight;}else{Hight = hight;}
   return((Base*Hight)/2);       
   }else{
   return 0;      
   }      
}

double TrapzArea(double bbase, double sbase, double hight)
{
   double Bbase, Sbase, Hight;
   if(bbase && sbase && hight)
   {
   //module
   if (bbase<0){Bbase = -bbase;}else{Bbase = bbase;}
   if (sbase<0){Sbase = -sbase;}else{Sbase = sbase;}
   if (hight<0){Hight = -hight;}else{Hight = hight;}
   
   return(((Bbase+Sbase)*Hight)/2);
   }else{
    return 0;     
   }
}

double CircArea(double R)
{
   if (R){
     if (R<0){
     return((3.1415926*(-R))*(3.1415926*(-R)));        
     }else{
     return((3.1415926*R)*(3.1415926*R));
     }   
   }else{
     return 0;    
   }      
}

int main(void)
{
    double raio, lado1, lado2, sbase;
 puts("declare um raio:");
 scanf("%lf", &raio);
 puts("declare um lado1 (ou altura):");
 scanf("%lf", &lado1);
 puts("declare um lado2 (ou base e no trapesio base maior):");
 scanf("%lf", &lado2);
 puts("declare uma base menor do caso do trapézio:");
 scanf("%lf", &sbase);
 
 printf("-funcao: CircArea(double R) Area da circunferencia de raio R: %lf\n\n", CircArea(raio));
 printf("-funcao: SqrArea(double side) Area do quadrado de lado1(side): %lf\n\n", SqrArea(lado1));
 printf("-funcao: RectArea(double base, double hight) Area do retangulo de base lado2 e hight lado1: %lf\n\n", RectArea(lado2,lado1));
 printf("-funcao: TriArea(double base, double hight) Area do triangulo de base lado2 e hight lado1: %lf\n\n", TriArea(lado2,lado1));
 printf("-funcao: TrapzArea(double bbase, double sbase, double hight) Area do trapezio de base maior lado2 e hight lado1: %lf\n\n", TrapzArea(lado2,sbase,lado1));
 
 puts("A biblioteca por tratar de valores geométricos \"reais\" poe em modulo todos as entradas. EX: CircArea(-5.32) -> CircArea(5.32)");
 getch();   
}
