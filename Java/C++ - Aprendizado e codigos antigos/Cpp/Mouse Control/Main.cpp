#define _WIN32_WINNT 0x0500
#include<windows.h>
#
#include<cmath>

LRESULT CALLBACK mouseHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
// Get event information
PMSLLHOOKSTRUCT p = (PMSLLHOOKSTRUCT) lParam;

bool eat = false;

// Screen resolution
static float screen_cx = static_cast<float>( GetSystemMetrics(SM_CXSCREEN) );
static float screen_cy = static_cast<float>( GetSystemMetrics(SM_CYSCREEN) );

// Centre of screen
static float screen_centre_x = screen_cx / 2.0f;
static float screen_centre_y = screen_cy / 2.0f;

// Calculate distance away from centre of screen
float dx = p->pt.x - screen_centre_x;
float dy = p->pt.y - screen_centre_y;

float dist = sqrt(dx * dx + dy * dy);

// Check if cursor is more than 300px away from centre of screen
if (dist > 300) {
float angle = atan2(dy, dx);

// Trap cursor
SetCursorPos(
/* X */ int( screen_centre_x + cos(angle) * 300 ),
/* Y */ int( screen_centre_y + sin(angle) * 300 )
);

// Stop windows handling event
eat = true;
}

return (eat ? 1 : CallNextHookEx(NULL, nCode, wParam, lParam));
}

int WINAPI WinMain(HINSTANCE hInstance,
HINSTANCE hPrevInstance,
LPSTR lpCmdLine,
int nShowCmd) {

// Set mouse hook
HHOOK mouseHook = SetWindowsHookEx(
WH_MOUSE_LL, /* Type of hook */
mouseHookProc, /* Hook process */
hInstance, /* Instance */
NULL);

// Wait for user to exit
MessageBox(NULL, "Press OK to close.", "", MB_OK);
return 0;
}
