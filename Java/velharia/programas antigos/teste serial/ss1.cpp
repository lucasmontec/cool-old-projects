#include <stdio.h>   //printf() entre outras.
#include <conio.h>  //getch().
#include <windows.h>  //Necess�rio para: LoadLibrary(), GetProcAddress() e HINSTANCE.

//Declara��o dos ponteiros para fun��o.
typedef short _stdcall (*PtrInp)(short EndPorta);
typedef void _stdcall (*PtrOut)(short EndPorta, short valor);

int main(void)
{
   HINSTANCE hLib; //Inst�ncia para a DLL inpout32.dll.
   PtrInp inportb;     //Inst�ncia para a fun��o Imp32().
   PtrOut outportb;  //Inst�ncia para a fun��o Out32().

   //Carrega a DLL na mem�ria.
   hLib = LoadLibrary("inpout32.dll");

   if(hLib == NULL) //Verifica se houve erro.
   {
      printf("Erro. O arquivo inpout32.dll n�o foi encontrado.\n");
      getch();
      return -1;
   }

   //Obt�m o endere�o da fun��o Inp32 contida na DLL.
   inportb = (PtrInp) GetProcAddress(hLib, "Inp32");

   if(inportb == NULL) //Verifica se houve erro.
   {
      printf("Erro. A fun��o Inp32 n�o foi encontrada.\n");
      getch();
      return -1;
   }

   //Obt�m o endere�o da fun��o Out32 contida na DLL.
   outportb = (PtrOut) GetProcAddress(hLib, "Out32");

   if(outportb == NULL) //Verifica se houve erro.
   {
       printf("Erro. A fun��o Out32 n�o foi encontrada.\n");
       getch();
       return -1;
   }

   //-------------------------------------------------------------------------------------------------------------------------  
   //Uso das fun��es outportb() e inportb():
   printf("Pressione uma tecla para ligar o pino D0.\n");
   getch();
   outportb(0x378,1); //Ativa o pino D0 do Registro de Dados da porta paralela.
   printf("Pressione uma tecla para desligar o pino D0.\n");
   getch();
   outportb(0x378,0); //Desativa os pino do Registro de Dados da porta paralela.

   FreeLibrary(hLib); //Libera mem�ria alocada pela DLL.
   return(0);
} 
