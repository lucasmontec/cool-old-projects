//09/09/07 14:00
/*
  Name: School files organizer
  Copyright: none
  Author: Lucas M.C.
  Date: 09/09/07 14:01
  Description: organize school sports files.
*/
#include <radc++.h>
using namespace std;
int x=0;
//tray ico
Icon fotoi(IDI_APPLICATION);
Icon ico(IDI_EXCLAMATION);
//formul�rio
string cadastro;
string txtfinal;
//cria janelas
Form datab("dbase load.",0,0,500,500);
Form buscador("Buscador em fase de constru��o.",0,0,500,200);
Form controle("Gerenciador de fichas.",0,0,400,400);
Form splash("x",0,0,500,200,RCP_NONE);
Form form1("Programa-Esporte.");
Form passo2("Passo-2 jogo.");
Form passo3("Passo-3 jogo-final.");
Form form2("Jogo.");
//menu
TableView  	busc("1",AUTO_ID,0,70,100,100,buscador);
Menu menu(controle);
MenuItem __File,__abrir,__salvar,__sair;
//pbar
ProgressBar pgb1(-1,100,10,250,20,splash,RCP_HORIZONTAL);
//timer
String timer1 = "Splash";

ImageBox ibox(AUTO_ID,0,0,0,500,200,splash);
//buttoms
Button prox1("pr�ximo", AUTO_ID, 320, 200, 50, 30, form2);
Button cdat("criar data", AUTO_ID, 430, 420, 50, 30, datab);
Button prox2("pr�ximo", AUTO_ID, 320, 225, 50, 30, passo2);
Button prox3("salvar", AUTO_ID, 335, 225, 50, 30, passo3);
//cria tool bar
ToolBar tools(AUTO_ID,form1);
//txtboxcompleta
RichTextBox txt2("", AUTO_ID, 10, 20, 370, 220, controle);
RichTextBox cad("cadastro de alunos.\nPara cadastrar um aluno siga o exemplo:\n\nNome.Idade.Gols.AV's\n\n(ATEN�AO! NAO ESQUE�A DOS PONTOS ENTRE OS DADOS!!!)", AUTO_ID, 10, 20, 470, 380, datab);
RichTextBox placares("--Placares do jogo--\n-OBS-Observa��es-OBS-\nEXEMPLO:\n1�tempo 0x0", AUTO_ID, 120, 35, 210, 220, passo3);
RichTextBox txt("escreva aqui a escala��o.\n-exemplo\n-exemplo", AUTO_ID, 10, 20, 370, 200, passo2);
//txtbox
TextBox text1("Data aqui", AUTO_ID, 10, 40, 200, 25, form2);
TextBox text2("Local aqui", AUTO_ID, 10, 70, 200, 25, form2);
TextBox text3("Modalidade aqui", AUTO_ID, 10, 100, 200, 25, form2);
TextBox text4("Hor�rio aqui", AUTO_ID, 10, 130, 200, 25, form2);
TextBox text5("Torneio aqui", AUTO_ID, 10, 160, 200, 25, form2);
TextBox text6("Categoria aqui", AUTO_ID, 10, 190, 200, 25, form2);
TextBox arbitro1("1-arbitro", AUTO_ID, 10, 35, 100, 25, passo3);
TextBox arbitro2("2-arbitro", AUTO_ID, 10, 65, 100, 25, passo3);
TextBox time1("Time 1", AUTO_ID, 10, 180, 100, 25, passo3);
TextBox time2("Time 2", AUTO_ID, 10, 230, 100, 25, passo3);
//cria os bot�es da tool bar
ToolBarItem btn_sair, btn_sobre, btn_jogo, btn_busca, btn_controle;
//cria icones aos bot�es
IconList icl(RCP_SMALL); 
//pega os icones do sistema e prepara pra uso
Icon icon1(IDI_APPLICATION);
Icon icon2(IDI_APPLICATION);
Icon icon3(IDI_HAND);
Icon icon4(IDI_QUESTION);
Icon icon5(IDI_HAND);
//tip of days
TipOfDay tod("","",form1,false);
TipOfDay t("msg.rct","Dicas",form1);
//splash
//busca
ReadOnlyBox combo("Selecionar aluno.", AUTO_ID, 120, 10, 260, 25, buscador);
Button buscar("Buscar", AUTO_ID, 120,40, 260, 25, buscador);
ComboBox avdef("avdef", AUTO_ID, 10, 10, 100, 200, buscador);
ComboBox av("av", AUTO_ID, 10, 40, 100, 200, buscador);
//extras
bool exist;
//dbase progs
void dbaseini()
{
     FILE *p;
     if (!(p=fopen("dbase.dat","r+")))
     {
     form2.errorBox ("N�o existe data base padr�o. Por favor insira informa��o!");
     datab.show();
     }
     
}
int atualiza()
{
    cadastro = cad.text; 
    return 0;
}
//inicio dos procs
FormProcedure dataProc(FormProcArgs) {
ON_COMMAND_BY(cdat) {
FILE *p;
atualiza();
//cadastro==""?exist=true:exist=false;
cadastro=="cadastro de alunos.\nPara cadastrar um aluno siga o exemplo:\n\nNome.Idade.Gols.AV's\n\n(ATEN�AO! NAO ESQUE�A DOS PONTOS ENTRE OS DADOS!!!)"?exist=true:exist=false;
if(exist){
     if (!(p=fopen("dbase.dat","a+")))
     {
     datab.errorBox ("Erro na abertura ou cria��o do arquivo!");
     exit(1);
     }
     }else{
     //datab.errorBox ("informa��o vazia!");
     datab.errorBox ("informa��o Par�o!");
}
}    
ON_CLOSE(){
           datab.hide();
           }
           return 0;
}
FormProcedure buscadorProc(FormProcArgs) {
ON_CLOSE(){
           buscador.hide();
           }
ON_COMMAND_BY(buscar) {
dbaseini();
}      
ON_RESIZE() {
		busc.fitToWidth();
}
return 0;
}
FormProcedure splashProc(FormProcArgs) {
    ON_TIMER_NOTIFICATION(timer1) { 
    splash.caption = str(x++);
    pgb1.percent=x*20;
if(x==5)
{
 splash.removeTimer(timer1);
 {
 splash.hide();
 form1.show(); 
}
}
    }
return 0;
}
int passo3str()
{
  passo3.show();
  placares.text = ("--Placares do jogo--\n-OBS-Observa��es-OBS-\nEXEMPLO:\n1�tempo 0x0");
  
}
void busca()
{
     buscador.show();
}
int controledoform()
{
    controle.show();
    txt2.text =("");
}
int passo2str()
{
   passo2.show();
 txt.text = ("escreva aqui a escala��o.\n-exemplo\n-exemplo");
}
//func da janela de jogo
void janelajogo()
{
    form2.show(); 
    form2.msgBox ("Em Constru��o...");
}
int criarficha()
{
 txtfinal = "--Ficha de controle--\n\n--Informa��es:\nTorneio:\n"+ text5.text +"\nModalidade:\n"+ text3.text +"\nCategoria:\n"+ text6.text +"\nData:\n"+ text1.text +"\nLocal:\n"+ text2.text +"\nHor�rio:\n"+ text4.text +"\n\n--Times:\nTime1:\n"+ time1.text +"\nTime2:\n"+ time2.text +"\n\n--Arbitragem:\nArbitro-1:\n"+ arbitro1.text +"\nArbitro-2:\n"+ arbitro2.text +"\n\nEscala��o:\n"+ txt.text +"\nPlacares:\n"+ placares.text +"\n\n-final-" ;
 return 0;
}
FormProcedure childProc3(FormProcArgs) {
 ON_COMMAND_BY ( prox3 ) {
  passo3.msgBox("Gerando...pressione ok");
 criarficha();
  if(passo3.save()) {
			BinaryData bin;
			String data = txtfinal;
			bin.add((UCHAR *)data.c_str(),data.length);
			bin.writeFile(passo3.fileName);                
			bin.clear();
			passo3.msgBox("Ficha salva. Acesse a atrav�s do bot�o Gerenciador.");
        }
        else{
   passo3.errorBox("Cancelamento ou erro.");
 }
}
 ON_CLOSE() {
 passo3.hide();
}
 return 0;
}
FormProcedure childProc2(FormProcArgs) {
 ON_COMMAND_BY ( prox2 ) {
// passo2.msgBox("Passo3");
 passo2.hide();
 passo3str();              
 }           
 ON_CLOSE() {
 passo2.hide();
   }
 return 0;
}
FormProcedure childProc(FormProcArgs) {
    ON_COMMAND_BY ( prox1 ) {
//    form2.msgBox("Passo2");
    form2.hide();
    passo2str();
    }
    ON_CLOSE() {
           {
           form2.hide();
           } 
}
return 0;
}
//comando dos eventos dos tools e do form1
FormProcedure form1Proc(FormProcArgs) {
    ON_TRAYICON_CLICK(form1) {
    form1.msgBox ("Atalho r�pido para buscador.");                        
    busca();                 
    }
    ON_CLOSE()
    Application.close();
    ON_COMMAND_BY(btn_controle)
    controledoform();
	ON_COMMAND_BY(btn_jogo)
    janelajogo();
	ON_COMMAND_BY(btn_busca)
	busca();
	ON_COMMAND_BY(btn_sair)
		Application.close();
	ON_COMMAND_BY(btn_sobre) 
		form1.infoBox("Programa gerenciador de fichas dos alunos.\nCriado por lucas M.C.");
			ON_RESIZE() 
		tools.adjust(); 
		//retorno de producure AWAYS zerado
			return 0;
}
  FormProcedure ctrProc(FormProcArgs) {
  ON_COMMAND_BY (__abrir) {
  if(controle.open()) {
			BinaryData bin;
			bin.loadFile(controle.fileName);  
			txt2.text = bin.c_str();        
			bin.clear();
		} 
    }
    ON_COMMAND_BY (__salvar) {
		if(controle.save()) {
			BinaryData bin;
			String data = txt2.text;
			bin.add((UCHAR *)data.c_str(),data.length);
			bin.writeFile(controle.fileName);                
			bin.clear();
        }
  }          
    ON_CLOSE() {
  controle.removeTrayIcon();
  controle.hide();
  } 
  ON_COMMAND_BY(__sair) controle.hide();
  return 0;             
  }
rad_main()
//funcs
datab.hide();
buscador.hide();
controle.hide();
passo2.hide();
passo3.hide();
form1.hide();
form2.hide();
//buscador
avdef.add("Av.atk");
avdef.add("*****");
avdef.add("****");
avdef.add("***");
avdef.add("**");
avdef.add("*");
av.add("Av.def");
av.add("*****");
av.add("****");
av.add("***");
av.add("**");
av.add("*");
busc.addColumn("Nome");
busc.addColumn("Classifica��o");
busc.addColumn("idade");
busc.addColumn("Gols");
busc.addRow("Exemplos");
busc.addRow("Abcd");
//main do rad(extras)
/*tod.addTip(" Dica 1: ", " Crie fichas de alunos facilmente! <B> \n Clicando no bot�o de Jogo � possivel criar uma ficha escolar completa com facilidade! </B> ");
tod.addTip(" Dica 2: ", " O bot�o controle: <B> \n Clicando em controle voc� pode controlar facilmente as fichas de alunos criadas. </B> ");
tod.addTip(" Dica 3: ", " O arquivo read-me: <B> \n � importante pois ensina tudo sobre o programa com exemplos. </B> ");
tod.addTip(" Dica 4: ", " O Tray icon: <B> \n O tray icon se localiza pr�ximo ao relogio do Windows no canto direito inferior do monitor. Ao clicar nele existe um atalho para o sistema de busca. </B> ");
tod.addTip(" Novidades: ", " O bot�o busca: <B> \n Estar� sendo implementado um sistema de busca muito em breve que facilitar� o controle das fichas dos alunos. N�o funcional,ainda.</B> ");
tod.addTip(" Info: ", " Inven��o: <B> \n Cri��o e idealiza��o do programa: \n Lucas M. Carvalhaes e Z� </B> ");
tod.saveFile("msg.rct");*/
ibox.setExStyle(0);
busc.fitToWidth();
//form1.opacity = 80; <<<quando sair a vers�o 1.2.4 que ta em teste
txt2.fitExact();
form1.icon = fotoi;
//tray
form1.setTrayIcon(ico,"Programa de gerenciamento");
//imagem splash
ibox.loadImage(Application.path + "\\ali.png");
//inicializa o form
datab.procedure = dataProc;
buscador.procedure = buscadorProc;
controle.procedure = ctrProc;
form1.procedure = form1Proc;
form2.procedure = childProc;
passo2.procedure = childProc2;
passo3.procedure = childProc3;
splash.procedure = splashProc;
//timer
splash.setTimer(timer1, 1 * SECONDS);
//chama a lista de icones
        icl.add(icon1);
        icl.add(icon2);
        icl.add(icon3);
        icl.add(icon4);
        icl.add(icon4);
//liga icones com a tool bar
        tools.setIconList(icl);
        tools.captions=true;
//menu
__File = menu.add("&a��es");
__abrir = __File.add("&abrir",AUTO_ID);
__salvar = __File.add("&salvar",AUTO_ID);
__File.addSeparator();
__sair = __File.add("S&air",AUTO_ID);
//chama os buttons
    btn_jogo =tools.add("Jogo",0,AUTO_ID);
	btn_busca =tools.add("Busca",1,AUTO_ID);
	btn_sair =tools.add("Sair"  ,2,AUTO_ID);
	btn_sobre =tools.add("Sobre" ,3,AUTO_ID);
	btn_controle =tools.add("controle" ,4,AUTO_ID);
Label label("Bem vindo ao programa de gerenciamento de alunos.",-1,100,120,200,40,form1);
Label label3("X",-1,55,212,10,14,passo3);
Label label4("Arbitragem",-1,10,15,50,14,passo3);
Label label2("Vers�o 1.0",-1,100,100,200,20,form1);
//txt ai em cima
rad_end()
