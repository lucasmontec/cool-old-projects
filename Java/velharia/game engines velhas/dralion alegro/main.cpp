#include <allegro.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <ctime>

void init();
void deinit();

int scr_width = 1024, scr_height = 768;
int tx,ty,etx,ety=30,pontos=0,ctx,etn=1,etimer;
float vell=0,velr=0,nx=602,ny=630;
bool t = 0,vai=0,mat=0,l=0,r=0,bld=0;
volatile long counter=0;
void icounter(){if(vell>0){vell-=0.1;};if(velr>0){velr-=0.1;};if(bld){etimer++;}};
int random(int max)
{
      time_t t = time(0);
      int hora = localtime(&t)->tm_hour;
      srand(time(0));
      if (max > 0){
      int x = rand() % max;
      return (x);
      }else{
      return (0);      
      }
}
void initcrap(bool fsc)
{
     allegro_init();
  install_keyboard();
  install_timer();
  install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT,0);   
  set_color_depth(32);
  if (fsc)
  set_gfx_mode(GFX_AUTODETECT,scr_width,scr_height,0,0);
  else
  set_gfx_mode(GFX_AUTODETECT_WINDOWED,scr_width,scr_height,0,0);
}

int main() {
	bool fsc = false;
	initcrap(fsc);
	set_config_file("jogo.rec");
    LOCK_VARIABLE(counter);
    LOCK_FUNCTION(icounter);
    install_int_ex(icounter, BPS_TO_TIMER(10));
    BITMAP *buffer = create_bitmap(scr_width, scr_height);
    BITMAP *nave = load_bitmap("nave.bmp", 0);
    BITMAP *tiro = load_bitmap("tiro.bmp", 0);
    BITMAP *et = load_bitmap("et.bmp", 0);
    BITMAP *sg = load_bitmap("sangue.bmp", 0);
	while (!key[KEY_ESC]) {
		ctx=tx+32;
		nx+=velr,nx-=vell;
		if (key[KEY_LEFT]){vell=1.5;};
		if (key[KEY_RIGHT]){velr=1.5;};
		if(etimer>0){draw_sprite(buffer, sg, etx, ety);if (etimer>5){etimer=0;bld=0;etx=0;ety=0;};}else{{draw_sprite(buffer, et , etx,ety);};}
		if (t==0) {tx=nx+38,ty=ny-28;}else{if(ty>0){ty-=10;}else{t=0;};};
		textprintf_centre_ex(buffer, font, 40, 755, 255, -1, "%2f",velr);
		textprintf_centre_ex(buffer, font, 40, 745, 255, -1, "%2f",vell);
		textprintf_centre_ex(buffer, font, 40, 735, 255, -1, "Motores:");
		textprintf_centre_ex(buffer, font, 55, 10, 255, -1, "Dinheiro: %i",pontos);
		 draw_sprite(buffer, nave, nx, ny);
		 if (vai ==0 && etx<950){ etx +=1;}else{vai=1;};
		 if (vai ==1 && etx>10){etx-=1;}else{vai=0;};
		 if (nx>1020 && key[KEY_RIGHT]){nx=-127;}else{if(nx<-125 && key[KEY_LEFT]){nx=1021;};};
		 if (key[KEY_SPACE]&& t==0) {t=1;};
		 if (t==1){draw_sprite(buffer, tiro, tx, ty);nave = load_bitmap("navet.bmp", 0);}else{nave = load_bitmap("nave.bmp", 0);};
               		if (key[KEY_F5])
               		{ if (fsc){set_gfx_mode(GFX_AUTODETECT_WINDOWED,scr_width,scr_height,0,0),fsc=false;} else {set_gfx_mode(GFX_AUTODETECT,scr_width,scr_height,0,0),fsc=true;};}
		  if (ctx<etx+80&&ctx>etx&&ty<ety+80) { 
   /* bala bateu no et*/
   pontos+=rand()%50;
   t=0;
   bld=1;
   };
		 blit(buffer, screen, 0, 0, 0, 0, buffer->w, buffer->h);
 		clear(buffer);
	}
  destroy_bitmap(buffer);
  destroy_bitmap(nave);
  destroy_bitmap(tiro);
  destroy_bitmap(et);
  destroy_bitmap(sg);
    remove_mouse();
	deinit();
	fade_out(4);
	return 0;
}
END_OF_MAIN()

void deinit() {
	clear_keybuf();
	/* add other deinitializations here */
}
