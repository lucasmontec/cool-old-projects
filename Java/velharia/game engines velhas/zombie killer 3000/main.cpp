#include <irrlicht.h>
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <ctime>
using namespace irr;

#ifdef _MSC_VER
#pragma comment(lib, "Irrlicht.lib")
#endif

bool mous = false;
int distx = 0;
int disty = 0;
float incx = 0.f;
float incy = 0.f;
float vida = 50.f;
int escudo = 3;
bool gameover = false;
int counter = 0;
bool passou = false;
int arma = 0;
int maxtr = 10;
bool collr = false;
bool colll = false;
bool collu = false;
bool colld = false;

int random(int max)
{
      time_t t = time(0);
      int hora = localtime(&t)->tm_hour;
      srand(time(0));
      if (max > 0){
      int x = rand() % max;
      return (x);
      }else{
      return (0);      
      }
}


class MyEventReceiver : public IEventReceiver
{
public:
     //mouse
	struct SMouseState
	{
		core::position2di Position;
		bool LeftButtonDown;
		SMouseState() : LeftButtonDown(false) { }
	} MouseState;
	
	// prescisa ser implementado teclado
	virtual bool OnEvent(const SEvent& event)
	{		
		//mouse event
			// Guarda o estado do mouse
		if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
		{
			switch(event.MouseInput.Event)
			{
			case EMIE_LMOUSE_PRESSED_DOWN:
				MouseState.LeftButtonDown = true;
				break;

			case EMIE_LMOUSE_LEFT_UP:
				MouseState.LeftButtonDown = false;
				break;

			case EMIE_MOUSE_MOVED:
				MouseState.Position.X = event.MouseInput.X;
				MouseState.Position.Y = event.MouseInput.Y;
				break;

			default:
				// We won't use the wheel
				break;
			}
		}
		
		// detecta se as teclas estao para baixo ou para cima
		if (event.EventType == irr::EET_KEY_INPUT_EVENT)
			KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
		return false;
	}
	//mouse
    const SMouseState & GetMouseState(void) const
	{
		return MouseState;
	}  
	// checa qual key ta sendo apertada
	virtual bool IsKeyDown(EKEY_CODE keyCode) const
	{
		return KeyIsDown[keyCode];
	}

	MyEventReceiver()
	{
		for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i)
			KeyIsDown[i] = false;
    } 
private:
	// guarda o estado das teclas
	bool KeyIsDown[KEY_KEY_CODES_COUNT];
};
          
int main()
{
  // desativado...openGL melhor op��o
/*  video::E_DRIVER_TYPE driverType;

  printf("Please select the driver you want for this example:\n"\
		" (a) Direct3D 9.0c\n (b) OpenGL 1.5\n");

   char i;
   std::cin >> i;

   switch(i)
   {
      case 'a': driverType = video::EDT_DIRECT3D9;break;
      case 'b': driverType = video::EDT_OPENGL;break;
      default: return 0;
   }	*/

    // create device
   MyEventReceiver receiver;

	IrrlichtDevice* device = createDevice(video::EDT_OPENGL, core::dimension2d<s32>(800, 600),
		32, false, false, false, &receiver);

  if (device == 0)
      return 1;
 
  device->setWindowCaption(L"Zombie Killer 3000");

  //core::dimension2d<int> size = device->getVideoDriver()->getScreenSize();

	device->getCursorControl()->setVisible(false);

  video::IVideoDriver* driver = device->getVideoDriver();
  
  //texturas
  video::ITexture* personagemfaca = driver->getTexture("media/carinha_faca.png");
  video::ITexture* personagempistola = driver->getTexture("media/carinha_pistola.png");
  video::ITexture* personagemuzi = driver->getTexture("media/carinha_uzi.png");
  video::ITexture* tile1 = driver->getTexture("media/tiledirt1.jpg");
  video::ITexture* tile2 = driver->getTexture("media/tiledirt2.jpg");
  video::ITexture* mira = driver->getTexture("media/mira.png");
  video::ITexture* escudoimg = driver->getTexture("media/escudo.png");
  video::ITexture* facada1 = driver->getTexture("media/facada1.png");
  video::ITexture* facada2 = driver->getTexture("media/facada2.png");
  video::ITexture* facada3 = driver->getTexture("media/facada3.png");
  video::ITexture* overlay = driver->getTexture("media/over.png");
  video::ITexture* pistola = driver->getTexture("media/pistola.png");
  video::ITexture* uzi = driver->getTexture("media/uzi.png");
  video::ITexture* ak47 = driver->getTexture("media/ak47.png");
  video::ITexture* bazzoka = driver->getTexture("media/bazzoka.png");
  video::ITexture* tree1 = driver->getTexture("media/tree1.png");
  video::ITexture* tree2 = driver->getTexture("media/tree2.png");
  
//define a textura do personagem principal		
video::ITexture* personagem = personagemfaca;
video::ITexture* tile = tile1;
video::ITexture* facada = facada1;
//fontes basicas
gui::IGUIFont* font = device->getGUIEnvironment()->getBuiltInFont();
gui::IGUIFont* font2 = device->getGUIEnvironment()->getFont("media/fonthaettenschweiler.bmp");
//draw loop
	while(device->run() && driver)
	{
		if (device->isWindowActive())
		{
			u32 time = device->getTimer()->getTime();

			driver->beginScene(true, true, video::SColor(255,0,0,0));
         if (vida > 0 && !gameover && !passou){
         //WASD
    	 if(receiver.IsKeyDown(irr::KEY_KEY_W))
         {
            incy += (vida > 20? vida/4000 : 0.003);                  
         }
       	 if(receiver.IsKeyDown(irr::KEY_KEY_S))
         {
            incy -= (vida > 20? vida/4000 : 0.003);                       
         }
       	 if(receiver.IsKeyDown(irr::KEY_KEY_D) & !collr)
         {
            incx -= (vida > 20? vida/4000 : 0.003);                       
         }
       	 if(receiver.IsKeyDown(irr::KEY_KEY_A))
         {
            incx += (vida > 20? vida/4000 : 0.003);                       
         }
         }
         
         //tile driver
         for (int b =-incy/50-1; b < 12-incy/50; b++){
         for (int i =-incx/50-1; i < 16-incx/50; i++){
         driver->draw2DImage(tile, core::position2d<s32>(i*50+incx,b*50+incy),
         core::rect<s32>(0,0,50,50), 0,
         video::SColor(255,255,255,255), true);
         }
         }
         
         //ARMA:
           if (arma == 0){
           personagem = personagemfaca;         
           } 
         //ataque faca
         if (time/4 % 4 == 1){
             facada = facada1;     
         }else{
         if (time/4 % 4 == 2){
                  facada = facada2;
                  }else{
                  if (time/4 % 4 == 3){
                        facada = facada3;
                                     }
                        }        
         }     
         if (arma == 0 && receiver.GetMouseState().LeftButtonDown){
         driver->draw2DImage(facada, core::position2d<s32>(385,255),
         core::rect<s32>(0,0,30,30), 0,
         video::SColor(time/4 % 200,255,255,255), true);      
         }   
           
         //CARINHA:
         if (!gameover){
         //retangulo dos escudos
         driver->draw2DRectangle(video::SColor(60,255,255,255),
         core::rect<s32>(761, 10, 795, 31));
         //for dos escudos
         for (int i = 0; i < escudo; i++){
         driver->draw2DImage(escudoimg, core::position2d<s32>(760+(i*8),10),
         core::rect<s32>(0,0,20,20), 0,
         video::SColor(255,255,255,255), true);   
         } 
         }
         
         if (vida > 0 && !gameover){   
         //desenha o personagem vivo
         driver->draw2DImage(personagem, core::position2d<s32>(375,275),
         core::rect<s32>(0,0,50,50), 0,
         video::SColor(255,255,255,255), true);
         }else{
         if (escudo > 0 && !passou){
         escudo -= 1;
         passou = true;                      
         }else{    
         if (!passou){
         //personagem morto
         driver->draw2DImage(personagem, core::position2d<s32>(375,275),
         core::rect<s32>(0,0,50,50), 0,
         video::SColor(100,255,50,50), true);
         //gameover
         gameover = true;
         vida = 0.f;
         }     
         }        
         }
         if (passou && vida < 50.f){
         //desenha o personagem machucado
         driver->draw2DImage(personagem, core::position2d<s32>(375,275),
         core::rect<s32>(0,0,50,50), 0,
         video::SColor(190,90,40,40), true);
         vida += 0.005f;                  
         }else{
         passou = false;     
         }        
         
         
         //vida do personagem
         driver->draw2DRectangle(video::SColor(150,255,255*((vida*2)/100),10),
         core::rect<s32>(375, 330, 375 + vida, 335));
         
         //posi��o do mouse
         core::position2d<s32> mpos = device->getCursorControl()->getPosition();   
         distx = -(400 - mpos.X);
         disty = -(300 - mpos.Y);
         
         //gameover
         if (font2 && gameover)
         {
         //retangulo branco
         driver->draw2DRectangle(video::SColor(80,255,255,255),
         core::rect<s32>(373, 330, 431, 345));
         //texto "gameover"
         font2->draw(L"GAME OVER!",
         core::rect<s32>(375,330,0,0),
         video::SColor(255,0,0,0));
         }
         //tree tile driver
         for (int b = -incy/600-1; b < maxtr-incy/600; b++){
         for (int i = -incx/600-1; i < maxtr-incx/600; i++){
         driver->draw2DImage(tree1, core::position2d<s32>(i*600+incx,b*600+incy),
         core::rect<s32>(0,0,140,140), 0,
         video::SColor(255,255,255,255), true);
         //retangulo de detec��o de colis�o
         core::rect<s32> colisionrect(i*600+incx+50, b*600+incy+50, i*600+incx+80, b*600+incy+80);
         //collision right
         if (i*600+incx+50 <= 425 & b*600+incy+80 <= 325 & b*600+incy+50 >= 275){
           collr = true;
            driver->draw2DRectangle(video::SColor(170,255,0,0),
            core::rect<s32>(10,10, 50, 50));            
         }else{
           collr = false;
         };
         driver->draw2DRectangle(video::SColor(80,255,255,255),
         colisionrect);
         }
         }
         if (receiver.GetMouseState().LeftButtonDown){
         vida -= 0.005;
         //atirando
         driver->draw2DImage(mira,core::position2d<s32>(mpos.X-15,mpos.Y-15),
         core::rect<s32>(0,0,30,30), 0,
         video::SColor(time % 255,255,255,255), true);                                           
         }else{    
         //desenha a mira
         driver->draw2DImage(mira,core::position2d<s32>(mpos.X-15,mpos.Y-15),
         core::rect<s32>(0,0,30,30), 0,
         video::SColor(255,255,255,255), true); 
         }
         //desenha o overlay
         driver->draw2DImage(overlay,core::position2d<s32>(0,0),
         core::rect<s32>(0,0,800,600), 0,
         video::SColor(255-(255*((vida*2)/100)),255,255,255), true);     
         driver->endScene();
         }
   }
	device->drop();

	return 0;
}
