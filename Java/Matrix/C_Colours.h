#ifndef C_COLOURS_H_INCLUDED
#define C_COLOURS_H_INCLUDED

#define black 0
#define blue 1
#define green 2
#define watergreen 3
#define red 4
#define purple 5
#define yellow 6
#define white 7
#define grey 8
#define lightblue 9
#define lightgreen 10
#define lightwatergreen 11
#define lightred 12
#define lilac 13
#define lightyellow 14
#define lightwhite 15

int Color(int foreGround, int backGround){
    if(foreGround > 15){
         foreGround = 15;
    }

    if(backGround > 15){
         backGround = 15;
    }

    return foreGround+backGround*16;
}

#endif // C_COLOURS_H_INCLUDED
