#include <stdio.h>

#include <string.h>
#include <time.h>
#include <cmath>
#include <ctime>
#include <windows.h>

#define black 0
#define blue 1
#define green 2
#define watergreen 3
#define red 4
#define purple 5
#define yellow 6
#define white 7
#define grey 8
#define lightblue 9
#define lightgreen 10
#define lightwatergreen 11
#define lightred 12
#define lilac 13
#define lightyellow 14
#define lightwhite 15

#define SIZEX 15
#define SIZEY 40
#define LINES 15

using namespace std;

void delay(long double milliseconds)
{
  long double timeout = clock() + milliseconds;
  while( clock() < timeout ) continue;
}

int Color(int foreGround, int backGround);

char randomChar(){
    int rnd = 92+rand()*50;
    return (char)rnd;
}

main()
{
srand(time(NULL));

//A hancle to our console
HANDLE Console;

//Set our console
Console = GetStdHandle(STD_OUTPUT_HANDLE);

int tela[SIZEX][SIZEY];

for(int x=0;x<SIZEX;x++){
        for(int y=0;y<SIZEY;y++){
           tela[x][y] = 0;
        }
}

puts("Iniciando a matrix");
delay(200);
system("cls");
puts("Iniciando a matrix.");
delay(200);
system("cls");
puts("Iniciando a matrix..");
delay(200);
system("cls");
puts("Iniciando a matrix...");
delay(200);
system("cls");
printf("Iniciando a matrix...");
SetConsoleTextAttribute(Console,Color(lightgreen,black));
printf("[READY]");
delay(200);
system("cls");

int y = 0;
int x = 0;
int droppingLinesX[LINES];
int droppingLinesY[LINES];
int lineDelays[LINES];
int lineDelayCounters[LINES];

for(int i=0;i<LINES;i++){
    droppingLinesX[i] = (int)(rand()%(SIZEY-1));
    droppingLinesY[i] = 0;
    lineDelayCounters[i] = 0;
    lineDelays[i] = rand() % 6;
}

while(1){  ///////////////////Screen loop

 for(x=0;x<SIZEX;x++){
   if (GetAsyncKeyState(0x1b)){
     exit(0);
   }
   for(y=0;y<SIZEY;y++){
       if (tela[x][y] == 1){
              SetConsoleTextAttribute(Console,Color(lightwhite,black));
              printf("c");
        }else if(tela[x][y] >= 2 && tela[x][y] < 4){
              SetConsoleTextAttribute(Console,Color(lightyellow,black));
              printf("%c",randomChar());
        }else if(tela[x][y] >= 4 && tela[x][y] < 5){
              SetConsoleTextAttribute(Console,Color(lightgreen,black));
              printf("%c",randomChar());
        }else if(tela[x][y] >= 5){
              SetConsoleTextAttribute(Console,Color(green,black));
              printf("%c",randomChar());
        }else
              printf(" ");
   }
    printf("\n");
    y=0;
 }

 //apaga a tela e seta x = 0
 COORD pos = {0,0};
 SetConsoleCursorPosition(Console, pos);
 x = 0;
 y = 0;

 //zera a matriz
 for(int x=0;x<SIZEX;x++){
    for(int y=0;y<SIZEY;y++){
        if(tela[x][y] > 8)
            tela[x][y] = 0;
        else if (tela[x][y] > 0)
            tela[x][y]++;
    }
 }

   //aplica 1 e 0
   for(int i=0;i<5;i++){
     tela[(int)droppingLinesY[i]][droppingLinesX[i]] = 1;

     if(droppingLinesY[i] > SIZEX-1)
        droppingLinesY[i] = 0;
     else
        if(lineDelayCounters[i]++ > lineDelays[i])
            droppingLinesY[i] += 1;
   }
 }
}

int Color(int foreGround, int backGround){
    if(foreGround > 15){
         foreGround = 15;
    }

    if(backGround > 15){
         backGround = 15;
    }

    return foreGround+backGround*16;
}
