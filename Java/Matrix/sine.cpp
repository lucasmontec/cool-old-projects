#include <stdio.h>

#include <string.h>
#include <time.h>
#include <cmath>
#include <ctime>
#include <windows.h>

#define black 0
#define blue 1
#define green 2
#define watergreen 3
#define red 4
#define purple 5
#define yellow 6
#define white 7
#define grey 8
#define lightblue 9
#define lightgreen 10
#define lightwatergreen 11
#define lightred 12
#define lilac 13
#define lightyellow 14
#define lightwhite 15

#define SIZEX 15
#define SIZEY 40
#define PI 3.14159265

using namespace std;

void delay(long double milliseconds)
{
  long double timeout = clock() + milliseconds;
  while( clock() < timeout ) continue;
}

int Color(int foreGround, int backGround);

main()
{
srand(time(NULL));

//A hancle to our console
HANDLE Console;

//Set our console
Console = GetStdHandle(STD_OUTPUT_HANDLE);

int tela[SIZEX][SIZEY];
int k = 0;

for(int x=0;x<SIZEX;x++){
        for(int y=0;y<SIZEY;y++){
           tela[x][y] = 0;
        }
}

puts("Iniciando SINE");
delay(200);
system("cls");
puts("Iniciando SINE.");
delay(200);
system("cls");
puts("Iniciando SINE..");
delay(200);
system("cls");
puts("Iniciando SINE...");
delay(200);
system("cls");
printf("Iniciando SINE...");
SetConsoleTextAttribute(Console,Color(lightgreen,black));
printf("[READY]");
delay(200);
system("cls");

int y = 0;
int x = 0;
int shift = 0;
while(1){  ///////////////////Screen loop

 for(x=0;x<SIZEX;x++){
   if (GetAsyncKeyState(0x1b)){
     exit(1);
   }
   for(y=0;y<SIZEY-1;y++){
       if (tela[x][y] == 1){
              SetConsoleTextAttribute(Console,Color(lightgreen,black));
              printf("%c",(char)219);
        }else if(tela[x][y] >= 2 && tela[x][y] < 4){
              SetConsoleTextAttribute(Console,Color(watergreen,black));
              printf("%c",(char)219);
        }else if(tela[x][y] >= 4 && tela[x][y] < 6){
              SetConsoleTextAttribute(Console,Color(lightblue,black));
              printf("%c",(char)178);
        }else if(tela[x][y] >= 6){
              SetConsoleTextAttribute(Console,Color(blue,black));
              printf("%c",(char)177);
        }else{
              SetConsoleTextAttribute(Console,Color(grey,black));
              printf(" ");
        }
   }
    printf("\n");
    y=0;
 }

 //apaga a tela e seta x = 0
 COORD pos = {0,0};
 SetConsoleCursorPosition(Console, pos);
 x = 0;
 y = 0;
 if (k<580){k+=15;}else{k=0;}
 if (shift<360){shift+=5;}else{
     shift=0;
 }
 //zera a matriz
 for(int x=0;x<SIZEX;x++){
    for(int y=0;y<SIZEY;y++){
        if(tela[x][y] > 8)
            tela[x][y] = 0;
        else if (tela[x][y] > 0)
            tela[x][y]++;
    }
 }

 //aplica 1 e 0
 tela[5+((int)(sin((shift+k)*(PI/180))*4))][k/15] = 1;
 tela[10+((int)(cos((shift+k)*(PI/180))*4))][k/15] = 1;

 }
}

int Color(int foreGround, int backGround){
    if(foreGround > 15){
         foreGround = 15;
    }

    if(backGround > 15){
         backGround = 15;
    }

    return foreGround+backGround*16;
}
