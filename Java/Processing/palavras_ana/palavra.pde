class palavra{
 
  //variaveis
 public float x = 0; // indice de posição
 public float y = 0; // indice de posição
 private float factor; // "velocidade"
 private float px,py; // pontos destino
 private String palv = "0"; //palavra padrao
 
 //construtores
 palavra(float xi, float yi){
   x = xi;
   y = yi;
   //escolhe um fator de velocidade - diferenciação de obj
   factor = random(0.01,0.1);
 }
 
 palavra(){
   x = random(width);
   y = random(height);
   factor = random(0.01,0.1);
 }
 
 //OBJETIVO -> LISTA DE PALAVRAS
 //LER UMA ARRAY (ou tabela) COM AS PALAVRAS
 //ESSA FUNÇÃO DEVE RECEBER A PALAVRA DA TABELA
 //E ARMAZENAR O TEXTO
 /*void setText(String inp){
   palv = inp;
 }*/ //fazer depois
 
 //Palavras aleatórias - APAGAR E SUBSTITUIR ACIMA
 void setText(){
   palv = ""+char(int(random(65,122)))+char(int(random(65,122)))+char(int(random(65,122)));
 }
 //Palavras aleatórias - APAGAR E SUBSTITUIR ACIMA
 
 //Função de posição
 void setPos(float xi, float yi){
   x = xi;
   y = yi;
 }
 
 //Fução de movimentação
 void Move(float dx, float dy){
   x += dx; //adiciona ao x
   y += dy; //adiciona ao y
 }
 
 //Função para "fugir" de um ponto
 //somar x na direção ("meu X" - x) na velocidade factor
 //somar y na direção ("meu Y" - y) na velocidade factor
 void Scape(float sx, float sy){
   this.Move((this.x-sx)*factor,(this.y-sy)*factor);
   //restrição de posição - MANTEM UNIDADES VISIVEIS
   if(this.x > width+5){
     this.x = -2;
   }
   if(this.x < -5){
     this.x = width+2;
   }
   if(this.y > height+5){
     this.y = -2;
   }
   if(this.y < -5){
     this.y = height+2;
   }
 }
 
 //Faz as particulas IREM para uma posição pelo
 //inverso da funão fugir.
 //px e py são pontos para armazenar uma posição destino
 void Back(){
   this.Move((px-this.x)*factor,(py-this.y)*factor);
 }
 
 //escolhe uma posição destino aleatória na tela
 void MoveRandom(){
   px = random(width);
   py = random(height);
 }
 
 //desenha effetivamente o texto na posição x e y da classe
 void show(){
   fill(190);
   text(palv,x,y);
   //strokeWeight(1.2);
   //stroke(255);
   //fill(140);
   //ellipse(x,y,12,12);
 }
}
