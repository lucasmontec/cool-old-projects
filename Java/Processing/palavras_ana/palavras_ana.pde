static int n = 50;
palavra p[] = new palavra[n];
int timer;
PFont fonte;

void setup(){
  size(500,500);
  background(0);
  smooth();
  
  fonte = loadFont("ComicSansMS-18.vlw");
  textFont(fonte, 18);
  
  for(int i=0;i<n;i++){
    p[i] = new palavra();
    p[i].setText();
    p[i].MoveRandom();
  }
}

void draw(){
  background(0);
  timer++;
  if(timer>500){
    timer = 0;
  }
  
  for(int i=0;i<n;i++){
    p[i].show();
    if(dist(mouseX,mouseY,p[i].x, p[i].y) > 81){
       p[i].Back(); 
    }
    if(timer == 400){
      p[i].MoveRandom();
    }
    if(dist(mouseX,mouseY,p[i].x, p[i].y) < 80){
    p[i].Scape(mouseX,mouseY);
    }
  }
}
