import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class game_of_life extends PApplet {

// Game of Life
lifeMatrix lm;

public void setup(){
  size(500,500);
  smooth();
  
  lm = new lifeMatrix(27,27);
  lm.randomize();
}

public void draw(){
  background(0);
  int old_matrix[][] = lm.matrix;
  lm.calcGame();
  
  if(mousePressed){
    lm.randomize();
  }
  
  for(int x=0;x<lm._width;x++){
   for(int y=0;y<lm._height;y++){
    if(lm.matrix[x][y]==1){
      fill(180,160,0);
    }else{
      fill(30);
    }
    ellipse(18+x*18,18+y*18,14,14);
   } 
  }
  
}
class lifeMatrix{
  
 int _width, _height;
 int matrix[][];
 
 lifeMatrix(int w, int h){
   matrix = new int[w][h];
   _width = w;
   _height = h;
 }
 
 public void randomize(){
  for(int i=0;i<_width;i++){
   for(int j=0;j<_height;j++){
     matrix[i][j] = PApplet.parseInt(random(0,2));
   }
  } 
 }
 
 public void calcGame(){
  //save a new matrix based on the last to apply later
  int nmatrix[][] = new int[_width][_height];
   
  for(int x=0;x<_width;x++){
   for(int y=0;y<_height;y++){
     int neighbors = 0;
     //top left
     if(x-1 >= 0 && y-1 >= 0){
       neighbors += matrix[x-1][y-1];
     }
     
     //top center
     if(y-1 >= 0){
       neighbors += matrix[x][y-1];
     }
     
     //top right
     if(y-1 >= 0 && x+1 < _width){
       neighbors += matrix[x+1][y-1];
     }
     
     //center left
     if(x-1 >= 0){
       neighbors += matrix[x-1][y];
     }
     
     //center center
       neighbors += matrix[x][y];
     
     //center right
     if(x+1 < _width){
       neighbors += matrix[x+1][y];
     }
     
     //bottom left
     if(x-1 >= 0 && y+1 < _height){
       neighbors += matrix[x-1][y+1];
     }
     
     //bottom center
     if(y+1 < _height){
       neighbors += matrix[x][y+1];
     }
     
     //bottom right
     if(x+1 < _width && y+1 < _height){
       neighbors += matrix[x+1][y+1];
     }
     
     if(neighbors == 3 && matrix[x][y] == 0){
       nmatrix[x][y] = 1;
     }
     if(neighbors == 3 || neighbors == 4 && matrix[x][y] == 1){
       nmatrix[x][y] = 1;
     }else{
       nmatrix[x][y] = 0; 
     }
     
   }
  }
  matrix = nmatrix;
  
 }
 
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "game_of_life" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
