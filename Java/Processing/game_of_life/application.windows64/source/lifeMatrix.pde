class lifeMatrix{
  
 int _width, _height;
 int matrix[][];
 
 lifeMatrix(int w, int h){
   matrix = new int[w][h];
   _width = w;
   _height = h;
 }
 
 void randomize(){
  for(int i=0;i<_width;i++){
   for(int j=0;j<_height;j++){
     matrix[i][j] = int(random(0,2));
   }
  } 
 }
 
 void calcGame(){
  //save a new matrix based on the last to apply later
  int nmatrix[][] = new int[_width][_height];
   
  for(int x=0;x<_width;x++){
   for(int y=0;y<_height;y++){
     int neighbors = 0;
     //top left
     if(x-1 >= 0 && y-1 >= 0){
       neighbors += matrix[x-1][y-1];
     }
     
     //top center
     if(y-1 >= 0){
       neighbors += matrix[x][y-1];
     }
     
     //top right
     if(y-1 >= 0 && x+1 < _width){
       neighbors += matrix[x+1][y-1];
     }
     
     //center left
     if(x-1 >= 0){
       neighbors += matrix[x-1][y];
     }
     
     //center center
       neighbors += matrix[x][y];
     
     //center right
     if(x+1 < _width){
       neighbors += matrix[x+1][y];
     }
     
     //bottom left
     if(x-1 >= 0 && y+1 < _height){
       neighbors += matrix[x-1][y+1];
     }
     
     //bottom center
     if(y+1 < _height){
       neighbors += matrix[x][y+1];
     }
     
     //bottom right
     if(x+1 < _width && y+1 < _height){
       neighbors += matrix[x+1][y+1];
     }
     
     if(neighbors == 3 && matrix[x][y] == 0){
       nmatrix[x][y] = 1;
     }
     if(neighbors == 3 || neighbors == 4 && matrix[x][y] == 1){
       nmatrix[x][y] = 1;
     }else{
       nmatrix[x][y] = 0; 
     }
     
   }
  }
  matrix = nmatrix;
  
 }
 
}
