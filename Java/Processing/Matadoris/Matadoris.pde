void setup(){
  size(500,500);
  background(0);
  smooth();

}

int i=0;

void draw(){
  background(0);

  nave(mouseX,mouseY,i++);
}

void nave(int x, int y, int angle){
  pushMatrix();
  translate(x, y);
  rotate(radians(angle));
  strokeWeight(2);
  stroke(120,234,255);
  fill(100,50,120);
  triangle(-40,20,0,-20,40,20);
  line(-15,0,-15,-25);
  line(15,0,15,-25);
  line(-7,-5,-7,-30);
  line(7,-5,7,-30);
  popMatrix();
}

