class laser{
 
  private emiter e;
  private float grossura = 5;
  private float x1;
  private float x2;
  private float y1;
  private float y2;
  private color cola;
  private color colb;
  private float transp;
  
 laser(){
   x1 = random(0,width);
   x2 = random(0,width);
   y1 = random(0,height);
   y2 = random(0,height);
   cola = color(random(255),random(255),random(255));
   colb = color(random(255),random(255),random(255));
   transp = random(100,255);
   
   e = new emiter(x2,y2,2,2);
   e.setColorA(cola);
   e.setColorB(colb);
   e.setTransp(transp,transp/2);
   e.setAmmount(200);
   e.setSpeed(0.4);
   e.loadParticles();
 }
 
 void setO(float x, float y){
  x1 = x;
  y1 = y;
 }
 
 void setD(float x, float y){
   x2 = x;
   y2 = y;
 }
 
 void setD(int x, int y){
   x2 = (float)x;
   y2 = (float)y;
 }
  
 void setColor(color a, color b){
   cola = a;
   colb = b;
 }
 
 void setTransp(float transp){
   this.transp = transp;
 }
 
 void setThick(float th){
   grossura = th;
 }
 
 void load(){
   e.setColorA(colb);
   e.setColorB(cola);
   e.setTransp(transp,transp/2); 
 }
 
 void beam(){
   //linha 1
   stroke(cola,transp);
   strokeWeight(grossura*1.4 + (sin(radians(millis()))*grossura/4));
   line(x1,y1,x2,y2);
   
   //linha 2
   stroke(colb,transp);
   strokeWeight(grossura/2 + (cos(radians(millis()*2))*grossura/6));
   line(x1,y1,x2,y2);
   
   //emiter
   e.setPos(x2,y2);
   e.sprayRadialColorAB(grossura*4,grossura);
 }
 
 }
