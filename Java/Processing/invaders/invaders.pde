
nave nv;
starfield sf;
PFont fontA;
char kBuff;

void setup(){
  size(600,400);
  background(0);
  smooth();
  
  fontA = loadFont("CourierNew36.vlw");
  textFont(fontA, 14);
  
  sf = new starfield();
  sf.loadParticles();
  sf.setVel(2);
  
  nv = new nave("nave_frente.jpg","nave_frente_mask.jpg","nave_dir.jpg","nave_dir_mask.jpg","nave_esq.jpg","nave_esq_mask.jpg");
  nv.x(width/2);
  nv.y(height - nv.ht - 10);
}

void draw(){
  background(0);
  
  sf.show();
  nv.show();
  nv.screenLock();
}

void keyPressed(){
  switch((char)key){
  case 'a':
  nv.moveLeft();
  break;
  case 'w':
  nv.moveUp();
  break;
  case 'd':
  nv.moveRight();
  break;
  case 's':
  nv.moveDown();
  break;
 }
}
