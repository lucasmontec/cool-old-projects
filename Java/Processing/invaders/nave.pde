class nave{
  
  float wh;
  float ht;
  private float DRAG = 0.5;
  private float x;
  private float y;
  private float engine;
  private float thrust;
  private float uthrust;
  private PImage nave_frente;
  private PImage nave_frente_msk;
  private PImage nave_dir;
  private PImage nave_dir_msk;
  private PImage nave_esq;
  private PImage nave_esq_msk;
  private PImage state;
  private emiter motor;
  
  nave(String nvf, String nvfm, String nvd, String nvdm, String nve, String nvem){
    x = 0;
    y = 0;
    engine = 1;
    nave_frente = loadImage(nvf);
    nave_frente_msk = loadImage(nvfm);
    nave_frente.mask(nave_frente_msk);
    
    nave_dir = loadImage(nvd);
    nave_dir_msk = loadImage(nvdm);
    nave_dir.mask(nave_dir_msk);
    
    nave_esq = loadImage(nve);
    nave_esq_msk = loadImage(nvem);
    nave_esq.mask(nave_esq_msk);
    
    state = nave_frente;
    wh = nave_frente.width;
    ht = nave_frente.height;
    
    motor = new emiter();
    
    motor.setColorA(180,190,250);
    motor.setColorB(30,0,240);
    motor.setAmmount(340);
    motor.setSpeed(0.4);
    motor.setTransp(120,40);
    motor.loadParticles();
    motor.setWidth(40);
  }
  
  void x(float x){
    this.x = x - nave_frente.width/2;
  }
  
  void y(float y){
    this.y = y + nave_frente.height/2;
  }
  
  float cx(){
    if (nave_frente != null){
    return (float)(x + nave_frente.width/2);
    }else{
    return 0; 
    }
  }
  
  float cy(){
    if (nave_frente != null){
    return (float)(y + nave_frente.height/2);
    }else{
    return 0; 
    }
  }
  
  void moveRight(){
    state = nave_dir;
    thrust = engine*7;
  }
  
  void moveLeft(){
    state = nave_esq;
    thrust = -engine*7;
  }
  
  void moveUp(){
    uthrust = -engine*7;
  }
  
  void moveDown(){
    uthrust = engine*7;
  }
  
  void show(){
    this.x += thrust;
    this.y += uthrust;
    
    if (thrust == 0){
      state = nave_frente;
    }
    
    if (thrust > 0){
      thrust-= DRAG;
    }
    if (thrust < 0){
      thrust+= DRAG;
    }
    if (uthrust > 0){ 
      uthrust-= DRAG;
    }
    if (uthrust < 0){
      uthrust+= DRAG;
    }
    
    motor.setPos(this.cx()-18,this.cy()+5);
    
   image(state,x,y,state.width*0.7,state.height*0.7); 
   motor.sprayDirectionalAcolorAB(270,2,0.1,60); 
  }
  
  void screenLock(){
   if (x + state.width > width + state.width && key == 'd'){
    x = - state.width;
   } 
   
   if (x < -state.width && key == 'a'){
    x = width + state.width;
   }
  } 
}
