void setup(){
  background(0);
  size(400,400);
  smooth();
}

float i;

void draw(){
  background(0);
  
  noStroke();
  fill(50,50,50);
  ellipse(200,200,400,400);
  stroke(0,255,0);
  
  i+=0.01;
  
  for (int k=0;k<40;k++){
  float x1 = 200+ (sin(90+i+(i+k))*200);
  float y1 = 200+ (cos(90+i+(i+k))*200);
  float x0 = 200+ (sin(90-i-(k))*200);
  float y0 = 200+ (cos(90-i-(k))*200);
  line(x0,y0,x1,y1);
  }
  
  for (int k=0;k<40;k++){
  float x0 = 200+ (sin(i+(i+k))*200);
  float y0 = 200+ (cos(i+(i+k))*200);
  float x1 = 200+ (sin(i-(k))*200);
  float y1 = 200+ (cos(i-(k))*200);
  line(x0,y0,x1,y1);
  }
}
