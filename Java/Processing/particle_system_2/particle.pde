class particle{
 
  //variaveis
 private float _max_vel = 100;
 private PImage _image;
 private PImage _image_msk;
 public float _start_size, _end_size, _size;
 public float _x = 0, _y = 0, _spawn_x, _spawn_y;
 public color _start_color, _end_color, _color;
 public float _angle;
 public float _ang_vel;
 public float _vel_x, _vel_y;
 public float _air_resist;
 public float _gravity;
 private float _lifeTimer, _start_time, _lifeTime, _lifePercent = 0;
 public boolean _alive;
 
  //construtores
 particle(){
   _lifeTime = 1000;
   _start_time = millis();
   _alive = true;
   _image = null;
   _image_msk = null;
   this._size = random(5,30);
   this._gravity = 0.4;
   this._vel_x = 0;
   this._vel_y = 0;
   this._air_resist = 0.2;
   this._angle = 0;
   this._color = color(255,255,255,random(255));
 }
 
  particle(String img){
   _lifeTime = 1000;
   _start_time = millis();
   _alive = true;
   _image = requestImage(img);
   _image_msk = null;
   this._size = random(5,10);
   this._gravity = 0.4;
   this._vel_x = 0;
   this._vel_y = 0;
   this._air_resist = 0.2;
   this._angle = 0;
   this._color = color(255,255,255,random(255));
 }
 
 particle(String img, String img_msk){
   _lifeTime = 1000;
   _start_time = millis();
   _alive = true;
   _image = requestImage(img);
   _image_msk = requestImage(img_msk);
   this._size = random(5,10);
   this._gravity = 0.4;
   this._vel_x = 0;
   this._vel_y = 0;
   this._air_resist = 0.2;
   this._angle = 0;
   this._color = color(255,255,255,random(255));
   if(_image != null && _image_msk != null){
   _image.mask(_image_msk);
   }
 }
 
  //funçoes
 
 public void setLifeTime(float lt){
    _lifeTime = lt;
 }
 
 public void setImage(String img){
   _image = requestImage(img);
 }
 
 public void setImage(PImage img){
   _image = img;
 }
 
 public void setMask(PImage msk){
  if(_image != null && msk != null){
   _image_msk = msk;
   _image.mask(_image_msk);
  } 
 }
 
 public void setMask(String msk){
  if(_image != null){
   _image_msk = requestImage(msk);
   _image.mask(_image_msk);
  } 
 }
 
 public void setSize(float s){
    _size = s;
 }
 
 public void setSizes(float start, float end){
    _start_size = start;
    _size = start;
    _end_size = end;
 }
 
 public void setColor(color col){
   _color = col;
 }
 
 public void setColors(color start, color end){
    _start_color = start;
    _end_color = end;
 }

 public void setAngle(float ang){
   _angle = ang;
 }
 
 public void setAngVel(float v){
   _ang_vel = v;
 }
 
 public void setVel(float vel_x, float vel_y){
   _vel_x = vel_x;
   _vel_y = vel_y;
 }
 
 public void setAirResist(float res){
   _air_resist = res;
 }
 
 public void setGravity(float g){
   _gravity = g;
 }
 
 public void setPos(float x, float y){
   _x = x;
   _y = y;
 }
 
 public void setStart(){
  _start_time = millis(); 
 }
 
  //funções de ação
 public void move(float x, float y){
   this.setPos(_x + x, _y + y);
 }
 
 public void addAngle(float a){
   this.setAngle(_angle + a);
 }
 
 public void addAngVel(float av){
   this.setAngVel(_ang_vel + av);
 }
 
 public void addVel(float x, float y){
   _vel_x += x;
   _vel_y += y;
 }
 
 public void scape(float x, float y){
   this.setVel((_x-x)*0.01,(_y-y)*0.01);
 }
 
 public void follow(float x, float y){
   this.setVel((x-_x)*0.07,(y-_y)*0.07);
 }
 
  //life time func
 public void lifeTimeUpdate(){
   if(_lifeTime == -1){
     _alive = true;
     _color = _start_color;
     _size = _start_size;
   }
   if(_lifeTime != -1 && _lifePercent < 1){
     _lifeTimer = millis() - _start_time;
     _lifePercent = _lifeTimer/_lifeTime;
     _size = lerp(_start_size,_end_size,_lifePercent);
     _color = lerpColor(_start_color, _end_color,_lifePercent);
   }
   if(_lifePercent >=1){
     _alive = false;
   }
 }
  //fisica
 public void pUpdate(){
   //ang resistence
   if(_ang_vel > 0){
     this.addAngVel(-_ang_vel*_air_resist);
   }
   if(_ang_vel < 0){
     this.addAngVel(-_ang_vel*_air_resist);
   }
   //air resistence
   if(_vel_x > 0){
     this.addVel(-_vel_x*_air_resist,0);
   }
   if(_vel_x < 0){
     this.addVel(-_vel_x*_air_resist,0);
   }
   if(_vel_y > 0){
     this.addVel(0,-_vel_y*_air_resist);
   }
   if(_vel_y < 0){
     this.addVel(0,-_vel_y*_air_resist);
   }
   //gravity
   if(abs(_vel_x) < _max_vel && abs(_vel_y) < _max_vel){
     this.addVel(0,_gravity);
   }
   //apply
   this.move(_vel_x,_vel_y);
   this.addAngle(_ang_vel);
 }
 
  //desenha
 public void show(){
   imageMode(CENTER);
   this.pUpdate();
   this.lifeTimeUpdate();
   if(_image != null){
     pushMatrix();
     translate(_x,_y);
     rotate(_angle*(PI/180));
     tint(_color);
     image(_image,0,0,_size,_size);
     popMatrix();
   }else{
     noStroke();
     fill(_color);
     println("."+alpha(_color));
     ellipse(_x,_y,_size,_size);
   }
 }
}
