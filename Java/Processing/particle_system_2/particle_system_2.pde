
system s;
//particle p;

void setup(){
  background(0);
  size(500,500);
  smooth();
  
  /*p = new particle();
  p.setSize(10);
  p.setLifeTime(-1);
  p.setGravity(0);
  p.setColor(color(0,255,0));*/
  
  s = new system(100);
  s.setSystemSize(0.6,0.6);
  s.setImage("image.JPG");
  s.setLifeTime(-1);
  s.setMask("image.JPG");
  s.setPos(width/2, height/2);
  s.setSizes(30,1,1);
  s.setGravity(0.2);
  s.setRoll(50);
  s.setAirResist(0.05);
  s.setColors(color(255,120,0,255),color(0,255,0,255));
  s.setColorVar(0.2);
}

void draw(){
  background(0);
  s.setPos(mouseX, mouseY);
  
  if(mousePressed){
    s.spray(100);
  }
  s.show();
}
