class emiter{
 
 private particle pArray[] = new particle[4000];
 private float x;
 private float y;
 private float wh;
 private float ht;
 private int amt;
 private float p_ang;
 private double speed;
 private float sz_mn;
 private float sz_mx;
 private float sz;
 private color col_a;
 private color col_b;
 private float transp;
 
 //-----------------------------constructors--------------------------
 
 emiter(){
  x = random(0,width-wh);
  y = random(0,height-ht);
  wh = 4;
  ht = 4;
  speed = 10;
  amt = 400;
  sz_mn = 1;
  sz_mx = 5;
  col_a = color(10,40,150);
  col_b = color(80,40,150);
  transp = random(40,100);
  for (int i=0;i<4000;i++){
    pArray[i] = new particle();
  }
  }
 
  emiter(float x, float y){
  this.x = x;
  this.y = y;
  wh = 4;
  ht = 4;
  speed = 10;
  amt = 400;
  sz_mn = 1;
  sz_mx = 5;
  col_a = color(10,40,150);
  col_b = color(80,40,150);
  transp = random(40,100);
  for (int i=0;i<4000;i++){
    pArray[i] = new particle();
  }
 }
 
  emiter(float x, float y,float wh, float ht){
  this.x = x;
  this.y = y;
  this.wh = wh;
  this.ht = ht;
  speed = 10;
  amt = 400;
  sz_mn = 1;
  sz_mx = 5;
  col_a = color(10,40,150);
  col_b = color(80,40,150);
  transp = random(40,100);
  for (int i=0;i<4000;i++){
    pArray[i] = new particle();
  }
 }
 
  emiter(float x, float y,float wh, float ht, int amt){
  this.x = x;
  this.y = y;
  this.wh = wh;
  this.ht = ht;
  speed = 10;
  this.amt = amt;
  sz_mn = 1;
  sz_mx = 5;
  col_a = color(10,40,150);
  col_b = color(80,40,150);
  transp = random(40,100);
  for (int i=0;i<amt;i++){
    pArray[i] = new particle();
  }
 }
 //------------------------functions---------------------------------
 
 void setAmmount(int amt){
   if (amt < 4000){
   this.amt = amt;
   }
 }
 
 void setAmmount(){
   amt = 600;
 }
 
 void setPAng(float a){
   p_ang = a;
 }
 
 void setSize(float sz_mn, float sz_mx){
   if (sz_mn < sz_mx){
     this.sz_mn = sz_mn;
     this.sz_mx = sz_mx;
   }else{
     if(sz_mn == sz_mx){
       sz = sz_mn;
     }
   }
 }
 
 void setSize(float sz){
   this.sz = sz;
 }
 
 void setPos(float x, float y){
   this.x = x;
   this.y = y;
 }
 
 void x(float x){
   this.x = x;
 }
 
 void y(float y){
   this.y = y;
 }
 
 void setWidth(float wh){
   this.wh = wh;
 }
 
 void setHeight(float ht){
   this.ht = ht;
 }
 
 void setEmiterSize(float wh, float ht){
   this.wh = wh;
   this.ht = ht;
 }
 
 void setSpeed(double sp){
   speed = sp;  
 }
 
 void setColors(color a, color b){
   col_a = a;
   col_b = b;
 }
 
  void setColors(float r1, float g1, float b1, float r2, float g2, float b2){
   col_a = color(r1,g1,b1);
   col_b = color(r2,g2,b2);
 }
 
 void setColorA(color a){
   col_a = a;
 }
 
 void setColorA(float r, float g ,float b){
   col_a = color(r,g,b);
 }
 
 void setColorB(color b){
   col_b = b;
 }
 
 void setColorB(float r, float g ,float b){
   col_b = color(r,g,b);
 }
 
 void setTransp(float transp){
   this.transp = random(transp-4,transp+4);
 }
 
 void setTransp(float transp, float var){
   if (transp > var){
   this.transp = random(transp-var,transp+var);
   }else{
    this.setTransp(transp); 
   }
 }
 
 //-------------------------------------------------------------
 
 float x(){
  return x; 
 }
 
 float y(){
  return y; 
 }
 
 float getWid(){
   return wh;
 }
 
 float getHei(){
  return ht; 
 }
 
 private color calculateColor(color b, color a, float val, float ma){
   float MAX = ma * 0.5;
    return color(
   ( (val/MAX)*red(a) + ((MAX-val)/MAX)*red(b) ),
   ( (val/MAX)*green(a) + ((MAX-val)/MAX)*green(b) ),
   ( (val/MAX)*blue(a) + ((MAX-val)/MAX)*blue(b) )
   );
 }
 
 //-------------------------------------------------------------
 
 void moveX(float v){
   this.x(this.x()+v);
 }
 
 void moveY(float v){
   this.y(this.y()+v);
 }
 
 void move(float x, float y){
   this.moveX(x);
   this.moveY(y);
 }
 
 void moveRnd(){
   this.move(random(-1,1),random(-1,1));
 }
 
 void moveRnd(float k){
   this.move(random(-k,k),random(-k,k));
 }
 
 //-------------------------------------------------------------
 
 void showEmiter(){
  fill(color(255,0,0),80);
  rectMode(CENTER);
  rect(x,y,wh,ht); 
 }
 
 void loadParticles(){
   for(int i=0;i<amt;i++){
   pArray[i].setSize(sz_mn,sz_mx);
   pArray[i].setAngle(p_ang);
   pArray[i].setTransp(transp);
   //pArray[i].setPos(random( (x-wh/2) , (x+wh/2) ),random( (y-ht/2) , (y+ht/2) ));
   pArray[i].setPos(random(0,width), random(0,height));
   pArray[i].eX(x);
   pArray[i].eY(y);
   pArray[i].setColor(col_a);
  } 
 }

 private void loadParticle(particle p){
   p.setSize(sz_mn,sz_mx);
   p.setColor(col_a);
   p.setAngle(p_ang);
   p.setTransp(transp);
   p.setPos(random( (x-wh/2) , (x+wh/2) ),random( (y-ht/2) , (y+ht/2) ));
   p.eX(x);
   p.eY(y);
   p.setColor(col_a);
 }

 void sprayRadial(float radius){
  for(int i=0;i<amt;i++){
     float nx = pArray[i].eX();
     float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/4),radius+(radius/4))){
    
    float dx = (nx - pArray[i].x())*(float)speed;
    float dy = (ny - pArray[i].y())*(float)speed;
    pArray[i].setPos( pArray[i].x() - dx , pArray[i].y() - dy );
    
    if(transp - pArray[i].getDistance(nx,ny) > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny) );
    }
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
 
 void sprayRadial(float radius, float pr){
  for(int i=0;i<amt;i++){
    float nx = pArray[i].eX();
    float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/pr),radius+(radius/pr))){
    
    float dx = (nx - pArray[i].x())*(float)speed;
    float dy = (ny - pArray[i].y())*(float)speed;
    pArray[i].setPos( pArray[i].x() - dx , pArray[i].y() - dy );
    
    if(transp - pArray[i].getDistance(nx,ny) > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny) );
    }
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
 
 void sprayRadialColorAB(float radius, float pr){
  for(int i=0;i<amt;i++){
    float nx = pArray[i].eX();
    float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/pr),radius+(radius/pr))){
    
    float dx = (nx - pArray[i].x())*(float)speed;
    float dy = (ny - pArray[i].y())*(float)speed;
    pArray[i].setPos( pArray[i].x() - dx , pArray[i].y() - dy );
    
    if(transp - pArray[i].getDistance(nx,ny)*0.2 > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny)*0.2 );
    }
    
    pArray[i].setColor(calculateColor(col_a,col_b,pArray[i].getDistance(nx,ny),radius));
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
 
  void sprayDirectional(float x, float y, float spr, float radius){
   float pr = random(4,10);
  for(int i=0;i<amt;i++){
    float nx = pArray[i].eX();
    float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/pr),radius+(radius/pr))){
    
    float dx = (nx - pArray[i].x())*(float)speed;
    float dy = (ny - pArray[i].y())*(float)speed;
    pArray[i].setPos( pArray[i].x() +x+random(-spr,spr)-(dx*0.1) , pArray[i].y() + y+random(-spr,spr)-(dy*0.1) );
    
    if(transp - pArray[i].getDistance(nx,ny)*random(0.01,0.4) > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny)*random(0.01,0.4) );
    }
    
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
 
 void sprayDirectionalA(float angle, float force, float spr, float radius){
   float pr = random(4,10);
   angle = radians(angle);
  for(int i=0;i<amt;i++){
    float nx = pArray[i].eX();
    float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/pr),radius+(radius/pr))){
    
    float dx = (nx - pArray[i].x())*(float)speed;
    float dy = (ny - pArray[i].y())*(float)speed;
    pArray[i].setPos( pArray[i].x() +(cos(angle)*force)+random(-spr,spr)-(dx*0.1) , pArray[i].y() +(sin(-angle)*force)+random(-spr,spr)-(dy*0.1) );
    
    if(transp - pArray[i].getDistance(nx,ny)*random(0.01,0.4) > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny)*random(0.01,0.4) );
    }
    
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
 
 
  void sprayDirectionalAcolorAB(float angle, float force, float spr, float radius){
   float pr = random(4,10);
   angle = radians(angle);
  for(int i=0;i<amt;i++){
    float nx = pArray[i].eX();
    float ny = pArray[i].eY();
    
    if (pArray[i].getDistance(nx,ny) < random(radius-(radius/pr),radius+(radius/pr))){
    
    float dx = (nx - pArray[i].x())*(float)this.speed;
    float dy = (ny - pArray[i].y())*(float)this.speed;
    pArray[i].setPos( pArray[i].x() +(cos(angle)*force)+random(-spr,spr)-(dx*0.1) , pArray[i].y() +(sin(-angle)*force)+random(-spr,spr)-(dy*0.1) );
    
    if(transp - pArray[i].getDistance(nx,ny)*random(0.01,0.4) > 0){
    pArray[i].dimm( pArray[i].getDistance(nx,ny)*random(0.01,0.4) );
    }
    
    pArray[i].setColor(calculateColor(col_a,col_b,pArray[i].getDistance(nx,ny),radius));
    pArray[i].show();
    
    }else{
     loadParticle(pArray[i]);
    }
  }
 }
  
}
