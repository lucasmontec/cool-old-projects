emiter e, f, a;


void setup(){
  size(400,400);
  background(0);
  smooth();
  
  f = new emiter(width/2 + 60,height/2,6,6); 
  a = new emiter(width/2 - 60,height/2 - 60,6,6);
  e = new emiter(width/2,height/2,6,6); 
  
  e.setColorA(255,255,0);
  e.setColorB(255,0,0);
  e.setAmmount(400);
  e.setSpeed(0.001);
  e.setTransp(80,20);
  e.loadParticles();
  
  f.setColorA(color(10));
  f.setColorB(color(100));
  f.setSpeed(0.02);
  f.setTransp(100,50);
  f.setSize(2,9);
  f.loadParticles();
  
  a.setColorA(color(4,40,170));
  a.setColorB(color(140,160,190));
  a.setSpeed(0.6);
  a.setAmmount(250);
  a.setTransp(80,40);
  a.setSize(1,4);
  a.loadParticles();
}

float rotator;

void draw(){
  background(0);
  //rotator += 1.8;
  //e.setPos(mouseX,mouseY);
  f.sprayDirectionalAcolorAB(90,0.4,1,120);
  a.sprayDirectionalAcolorAB(270,0.8,0.4,120);
  e.sprayDirectionalAcolorAB(90,0.9,0.01,90);
}

