import java.awt.*;

class button{
  
  boolean state;
  Rectangle bounds;
  Rectangle inner;
  color _col_off = color(255,0,0);
  color _col_on = color(0,255,0);
  float time = 0f;
  
  public button(int x, int y, int w, int h){
    bounds = new Rectangle(x,y,w,h);
    inner = new Rectangle(x,y,w,h/2);
  }
  
  public void setBounds(int x, int y, int w, int h){
    bounds.x = x;
    bounds.y = y;
    bounds.width = w;
    bounds.height = h;
    
    inner.x = x;
    inner.y = y;
    inner.width = w;
    inner.height = h/2;
  }
  
  void click(){
    if(bounds.contains(mouseX, mouseY))
     state = !state;
  }
  
  void update(){
    if(state){
      if(time < 1f)
        time += 0.1f;
    }else{
      if(time > 0f)
        time -= 0.1f; 
    }
  }
  
  void draw(){
    //Draw bounds
    fill((state?_col_on:_col_off),100);
    noStroke();
    rect(bounds.x, bounds.y, bounds.width, bounds.height);
    
    //Draw switch
    fill((state?_col_on:_col_off));
    rect(inner.x, inner.y+ (time*inner.height), inner.width, inner.height);
  }
  
}
