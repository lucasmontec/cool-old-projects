class gameButton extends button{
  
  private State win_state;
  
  public gameButton(State s, int x, int y, int w, int h){
    super(x,y,w,h);
    win_state = s;
  }
  
  public gameButton(){
    super(0,0,0,0);
  }
  
  public gameButton(int s_id, int x, int y, int w, int h){
    super(x,y,w,h);
    win_state = new State(s_id);
  }
  
  public void setWState(State s){
    win_state = s;
  }
  
  public boolean isComplete(ArrayList<gameButton> buttons){
    //local state vitories
    if(win_state.state_id == State.S_ZERO)
      return !state;
      
    if(win_state.state_id == State.S_ONE)
      return state;
      
    if(win_state.state_id == State.S_NOTUSEFULL)
      return true;
      
    //Check non local victories
    if(win_state.state_id == State.S_INVERSE)
      if(buttons.get(win_state.metadata) != null)
        return (buttons.get(win_state.metadata).state != state);
      else return false;
      
    if(win_state.state_id == State.S_EQUAL)
      if(buttons.get(win_state.metadata) != null)
        return (buttons.get(win_state.metadata).state == state);
      else return false;
      
    return false;
  }
}
