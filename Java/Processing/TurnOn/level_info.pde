class level_info{
  
  public int level_id = 0;
  public int level_buttons = 1;
  //The state id is equivalent to the slot id on this vector, so the state with ID 0 will get slot 0 here
  public int amount_of_states[];
  
  public level_info(int id, int btns, int amtos[]){
    level_id = id;
    level_buttons = btns;
    amount_of_states = amtos;
  }
}
