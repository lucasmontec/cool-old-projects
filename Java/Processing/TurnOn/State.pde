//More like Win State of a gameButton
class State{
  
  public int state_id = 0;
  public int metadata = 0;
  
  public State(int id, int meta){
    state_id = id;
    metadata = meta;
  }
  
  public State(int id){
    this(id,0);
  }
  
  //Keep this IDs from 0 and growing
  public static final int S_ZERO = 0;
  public static final int S_ONE = 1;
  public static final int S_NOTUSEFULL = 2;
  
  //require medatada to be the other button ID
  public static final int S_INVERSE = 3;
  public static final int S_EQUAL = 4;
  
}
