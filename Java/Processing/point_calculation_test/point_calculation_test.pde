import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;

AudioPlayer bg_player;
AudioSample tone;
AudioSample tone2;
AudioSample tone3;
Minim minim;
int samp_timer = 0;

static int PT_N = 800;
Point points[] = new Point[PT_N];

void setup(){
  smooth();
  size(800,600);
  background(0);
  
  minim = new Minim(this);
  bg_player = minim.loadFile("bg.mp3", 2048);
  tone = minim.loadSample("tone.mp3", 2048);
  tone2 = minim.loadSample("tone2.mp3", 2048);
  tone3 = minim.loadSample("tone3.mp3", 2048);
  
  for(int i=0;i<PT_N;i++){
  points[i] = new Point();
  points[i].setColorB(color(255,0,0,80));
  }
  
  bg_player.loop();
}

void draw(){
  background(0);
  
  float MSpeed = abs(0.5*((pmouseX - mouseX)+(pmouseY - mouseY)));
  //print(MSpeed + "\n");
  
  samp_timer++;
  if(samp_timer >= 60){samp_timer = 0;}
  if (mousePressed & samp_timer < 1){
    if (MSpeed <= 2){tone3.trigger();}
    if (MSpeed > 2 & MSpeed < 14){tone.trigger();}
    if (MSpeed >= 14){tone2.trigger();}
  }
  
  for(int i=0;i<PT_N;i++){
  points[i].Draw();
  if (mousePressed & dist(mouseX,mouseY,points[i].x,points[i].y)<points[i].act_dist){
  points[i].Scape(mouseX,mouseY);
  points[i].setSize(dist(mouseX,mouseY,points[i].x,points[i].y)-points[i].act_dist);
  }else{
  points[i].Back();
  }
  points[i].MoveBase(random(-1,1),random(-1,1));
  points[i].lerpCol((points[i].act_dist-dist(points[i].init_pos[0],points[i].init_pos[1],points[i].x,points[i].y))/points[i].act_dist);
  }
}

void stop()
{
  bg_player.close();
  tone.close();
  tone2.close();
  tone3.close();
  minim.stop();
  
  super.stop();
}
