abstract class gui_text{
  
  //vars
  public PFont fonte;
  public color txt_color;
  public String txt;
  public float txt_size = 16;
  public float x = 0;
  public float y = 0;
  
  //construtores
  gui_text(){
    fonte = loadFont("ArialNarrow-14.vlw");
  }
  
  gui_text(String font){
    fonte = loadFont(font);
  }
  
  gui_text(PFont font){
    fonte = font;
  }
  
  //funçoes
  
  void setText(String txtx){
    txt = txtx;
  }
  
  void setFont(){
    fonte = loadFont("ArialNarrow-14.vlw");
  }
  
  void setFont(String font){
    fonte = loadFont(font);
  }
  
  void setFont(PFont font){
    fonte = font;
  }
  
  void setColor(color a){
    txt_color = a;
  }
  
  void setColor(float r, float g, float b){
    txt_color = color(r,g,b);
  }
  
  void setColor(float all){
    txt_color = color(all,all,all);
  }
  
  void setSize(float s){
    txt_size = s;
  }
  
  void setPos(float x, float y){
    this.x = x;
    this.y = y;
  }
}
