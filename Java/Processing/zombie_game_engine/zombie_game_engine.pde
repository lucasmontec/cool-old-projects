simple_info t;
Button b;
Hint n;

void setup(){
  background(0);
  size(400,400);
  smooth();
  
  b = new Button(50,50);
  b.setSize(20,25);
  b.setColorOver(255,0,0);
  b.setColorDown(0,255,0);
  b.setColorTxt(0,0,0);
  b.setText("lol");
  
  n = new Hint();
  n.setText("R$ 900");
  n.setSize(50,20);
  n.setColor(0,255,0);
  n.setAlpha(190);
  
  t = new simple_info();
  t.setText("info");
  t.setSize(16);
  t.setColor(255);
}

void draw(){
  background(0);
  
  t.setPos(mouseX,mouseY);
  
  if (b.Pressed()){
    n.setPos(mouseX,mouseY);
    n.Pop();
  }
  
  b.Draw();
  n.Draw();
  t.Draw();
}

