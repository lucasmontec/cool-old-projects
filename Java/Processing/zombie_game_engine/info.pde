public class simple_info extends gui_text{
  
  simple_info(){
    this.txt = "";
    this.txt_color = color(0,0,0,255);
  }
  
  simple_info(String t){
    this.txt = t;
    this.txt_color = color(0,0,0,255);
  }
  
  simple_info(String t, color a){
    this.txt = t;
    this.txt_color = a;
  }
  
  //funções de açao
  void Draw(){
    fill(txt_color);
    textFont(fonte, txt_size);
    text(this.txt,this.x,this.y);
  }
}
