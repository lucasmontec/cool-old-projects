public class Hint extends gui{
 
 //vars
 private float init_y = 200;
 private float init_x = 200;
 private String txt = "";
 private float max_alpha = 255;
 private int txt_sz = 16;
 private color col_txt = color(255,255,255);
 private float desloc = 15;
 private float speed = 0.4;
 public boolean active = false;
 private int timer=0;
 private int time = 100;
 PFont a;
 
 //construtores
 Hint(){
  a = loadFont("ArialNarrow-14.vlw");
  this.y -= desloc;
  init_y = this.y;
  init_x = this.x;
  this.my_alpha = 0;
  this.my_bd_alpha = 0;
 }
 
 Hint(float x,float y){
   this.x = x;
   this.y = y-desloc;
   init_y = this.y;
   init_x = this.x;
   this.my_alpha = 0;
   this.my_bd_alpha = 0;
   a = loadFont("ArialNarrow-14.vlw");
 }
 
 Hint(float x,float y, float wd, float ht){
    this.x = x;
    this.y = y-desloc;
    init_y = this.y;
    init_x = this.x;
    this.wd = wd;
    this.ht = ht;
    this.my_alpha = 0;
    this.my_bd_alpha = 0;
    a = loadFont("ArialNarrow-14.vlw");
 }
 
 //overrides
 void setAlpha(float a){
    max_alpha = a;
 }
  
 void setPos(float x, float y){
    this.x = x;
    this.y = y;
    init_y = this.y;
    init_x = this.x;
 }
 
 //funcoes sem retorno
 
  void setSpeed(int s){
    this.speed = s;
  }
 
  void setTime(int t){
    this.time = t;
  }
 
  void setColorTxt(color tt){
    col_txt = tt;
  }

  void setColorTxt(float r,float g,float b){
    col_txt = color(r,g,b);
  }
 
  void setText(String txt){
    this.txt = txt;
  }

  void setTextSize(int sz){
    this.txt_sz = sz;
  }

  void setFont(PFont n){
    this.a = n;
  }
 
 private void playTimer(){
   if (timer < time){
   //timer
   timer++;
   }
 }
 
 private void resetTimer(){
   timer=0;
 }
 
 //funçoes de acao
 void Pop(){
   if(this.my_alpha <= 0){
   this.active = true;
   resetTimer();
   this.my_alpha = 1;
   this.y = init_y;
   this.x = init_x;
   }
 }
 
 void Draw(){
   if(this.my_alpha > 0){
   playTimer();
   
   if (timer < time){
   this.FullDimTo(time*(speed/20),max_alpha);
   //movimenta
   this.Move(0,-speed);
   }else{
   this.FullFade(time*(speed/20));
   //movimenta
   this.Move(0,-speed);
   }
   
   //desenha a hint
   if (my_border > 0){
   stroke(my_bd_col,my_bd_alpha);
   strokeWeight(my_border);
   }else{
   noStroke();
   }
   fill(my_col,my_alpha);
   rect(x,y,wd,ht);
   
   //desenha o texto
   if (txt != "" && a != null){
    fill(col_txt,my_alpha+50);
    textAlign(CENTER);
    textFont(a, txt_sz);
    text(txt, x+1+(wd/2), y+6+(ht/2));
   }
   }else{
    this.active = false; 
   }
   
 }
 
 
}
