public class color_system{
 
 //vars
 public color col_1 = color(255,255,255);
 public color col_2 = color(0,0,0);
 public color col_3 = color(0,255,0);
 public color col_4 = color(0,0,255);
 
 //construtores
 color_system(){
   //nada 
 }
 
 color_system(color a, color b, color c, color d){
  col_1 = a;
  col_2 = b;
  col_3 = c;
  col_4 = d;
 }
 
 //funçoes
 
 void setColorA(color a){
   col_1 = a;
 }
 
 void setColorA(float r, float g, float b){
   col_1 = color(r,g,b);
 }
 
 void setColorA(float a){
   col_1 = color(a,a,a);
 }
 
 void setColorB(color a){
   col_2 = a;
 }
 
 void setColorB(float r, float g, float b){
   col_2 = color(r,g,b);
 }
 
 void setColorB(float a){
   col_2 = color(a,a,a);
 }
 
  void setColorC(color a){
   col_3 = a;
 }
 
 void setColorC(float r, float g, float b){
   col_3 = color(r,g,b);
 }
 
 void setColorC(float a){
   col_3 = color(a,a,a);
 }
 
 void setColorD(color a){
   col_4 = a;
 }
 
 void setColorD(float r, float g, float b){
   col_4 = color(r,g,b);
 }
 
 void setColorD(float a){
   col_4 = color(a,a,a);
 }
 
  void setColors(color a,color b, color c, color d){
   col_1 = a;
   col_2 = b;
   col_3 = c;
   col_4 = d;
 }
 
 void setColors(float a,float b, float c, float d){
   col_1 = color(a,a,a);
   col_2 = color(b,b,b);
   col_3 = color(c,c,c);
   col_4 = color(d,d,d);
 }
 
 void setColors(float a){
   col_1 = color(a,a,a);
   col_2 = color(a,a,a);
   col_3 = color(a,a,a);
   col_4 = color(a,a,a);
 }
 
// void setAlphaAll(float a){
//   alpha(col_1) = a;
//   alpha(col_2) = a;
//   alpha(col_3) = a;
//   alpha(col_4) = a;
// }
 
 void FadeToBlackAll(float amt){
   col_1 = lerpColor(col_1,color(0),amt);
   col_2 = lerpColor(col_1,color(0),amt);
   col_3 = lerpColor(col_1,color(0),amt);
   col_4 = lerpColor(col_1,color(0),amt);
 }
 
 void FadeToBlack(int cc,float amt){
   if (cc == 1){col_1 = lerpColor(col_1,color(0),amt);}
   if (cc == 2){col_2 = lerpColor(col_1,color(0),amt);}
   if (cc == 3){col_3 = lerpColor(col_1,color(0),amt);}
   if (cc == 4){col_4 = lerpColor(col_1,color(0),amt);}
 }
 
 
 //funçoes com retorno
 
 color Wobble2(){
   return lerpColor(col_1,col_2,sin(millis()));
 }
 
 }
