class greenMonster extends monster{
  
   public greenMonster(int lvl, float ox, float oy, ship t){
     super(lvl, ox, oy, t);
     
     dmg = lvl*4;
     score = 8*((lvl*2)+(lvl/10)^2);
     sz = 10 + lvl*0.5;
     hp = floor(lvl*4.8f);
     hpMax = hp;
     money = lvl*2;
    
     col = color(random(160,190),0,0);
     position = new PVector(ox, oy);
     velocity = new PVector(t.position.x-ox, t.position.y-oy);
     velocity.normalize();
     velocity.mult(0.3);
     
     thinkTime = 1000;
  }
  
  public void think(){
    if(millis()-lastThink > thinkTime){
      
      //Go to player
      velocity = new PVector(target.position.x-position.x, target.position.y-position.y);
      velocity.normalize();
      velocity.mult(0.3);
      
      lastThink= millis();
    }
  }
  
  public void draw(){
    //The mosnter
    stroke(0,0,0,0);
    fill(20,242,30);
    ellipse(position.x-(sz/2),position.y-(sz/2),(sin(millis()/180f)*1.6f)+sz,(cos(millis()/180f)*1.6f)+sz);
    
    //The life bar
    if(hp < hpMax){
      stroke(20,250,20);
      fill(20,250,20);
      rect(position.x-10,position.y-20,(hp*1f/hpMax*1f)*20,2);
    }
  }
}
