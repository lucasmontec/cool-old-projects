class monster{
  
  public int hp;
  public int hpMax;
  public int dmg;
  public float sz = 8;
  public float score;
  public int money = 5;
  public PVector position;
  public PVector velocity;
  public final ship target;
  public long thinkTime = 2500;
  public long lastThink = 0;
  public boolean alive = true;
  public color col;
  
  public monster(int lvl, float ox, float oy, ship t){
    dmg = lvl*3;
    score = 5*((lvl*2)+(lvl/10)^2);
    target = t;
    sz = 8 + lvl*0.5;
    hp = floor(lvl*2.5f);
    hpMax = hp;
    money = lvl;
    
    col = color(random(160,190),0,0);
    position = new PVector(ox, oy);
    velocity = new PVector(t.position.x-ox, t.position.y-oy);
    velocity.normalize();
    velocity.mult(1.2);
  }
  
  public void think(){
    if(millis()-lastThink > thinkTime){
      
      //Go to player
      velocity = new PVector(target.position.x-position.x, target.position.y-position.y);
      velocity.normalize();
      velocity.mult(0.8);
      
      lastThink= millis();
    }
  }
  
  public void takeDamage(int dmg){
    if(hp-dmg <= 0){
      alive = false;
      hp = 0;
    }else{
      hp -= dmg; 
    }
  }
  
  public void update(){
    //Move
    position.add(velocity);
    
    //Think
    think();
    
    //Hit player
    if(position.dist(target.position) < 10){
      target.takeDamage(dmg);
      alive = false;
    }
    
    //Get hit by bullets
    if(worldBullets.bullets.size() > 0){
      for(bullet b : worldBullets.bullets){
        if(b.alive){
          if(position.dist(b.position) < sz){
            takeDamage(b.dmg);
            b.alive = false;
          }
        }
      }
    }
    
    //Autokill
    /*if(position.x < 0 || position.x > width
    || position.y < 0 || position.y > height){
      alive = false;
    }*/
  }
  
  public void draw(){
    //The mosnter
    stroke(0,0,0,0);
    fill(col);
    ellipse(position.x-(sz/2),position.y-(sz/2),(sin(millis()/100f)*1.4f)+sz,(cos(millis()/100f)*1.4f)+sz);
    
    //The life bar
    if(hp < hpMax){
      stroke(20,250,20);
      fill(20,250,20);
      rect(position.x-10,position.y-20,(hp*1f/hpMax*1f)*20,2);
    }
  }
}
