class ship{
  
  public PVector position;
  public PVector velocity;
  public float rotation;
  public turret t;
  public int hull = 100;
  public int maxHull = 100;
  public boolean alive = true;
  public int money = 0;
  
  //delays
  public long lastRepair = 0;
  public int repairDelay = 4000;
  public long lastAction = 0;
  public int actionDelay = 1000;
  public boolean validAction = false;
  
  public ship(){
    position = new PVector(width/2,height/2);
    velocity = new PVector();
    rotation = radians(90);
    t = new turret(this);
  }
  
  public void takeDamage(int dmg){
    if(hull-dmg <= 0){
      alive = false;
      hull = 0;
    }else{
      hull -= dmg; 
    }
  }
  
  public void addVelForward(float v){
    velocity.add(sin(rotation+PI/2)*v, cos(rotation+PI/2)*v,0);
  }
  
  public void move(){
     position.add(velocity);
  }
  
  public void rotate(float side){
    rotation += radians(side);
  }
  
  public void shot(){
   t.shot(); 
  }
  
  public void update(){
    move();
    t.update();
    
    //screen lock
    if(position.x < -40){
      position.x = width;
    }
    if(position.x > width+40){
      position.x = -10;
    }
    if(position.y < -40){
      position.y = height;
    }
    if(position.y > height+40){
      position.y = -10;
    }
  }
  
  public void keyPressed(char k){
    //Movement
    if(k == 'a'){
      rotate(5);
    }
    if(k == 'd'){
      rotate(-5);
    }
    if(k == 'w'){
      addVelForward(0.02f);
    }
    if(k == 's'){
      addVelForward(-0.02f);
    }
    if(k == ' '){
      velocity.mult(0.96f);
    }
    
    //Repair - cost: 50
    if(k == 'r'){
      if(millis() - lastRepair > repairDelay){
        if(money >= 50){
          if(hull + 30 < maxHull){
            hull = hull+30;
          }else{
            hull = maxHull;
          }
          
          //Pay
          money -= 50;
          
          //MSG
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $50",1600));
        }else{
           popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600)); 
        }
        
        lastRepair = millis();
      }
    }
    
    //Upgrades
    if(millis() - lastAction > actionDelay){
      
      //Update hull +100
      if(k == 'h'){
        if(money >= 900){
          //Upgrade the hull
          maxHull += 100;
          
          //Pay
          money -= 900;
          
          //Message
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $900: Hull update!",1600));
        }else{
          popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600));
        }
      }
      
      //Update dmg +2
      if(k == 'p'){
        if(money >= 1000){
          //Upgrade the dmg
          t.setDamage(t.damage+2);
          
          //Pay
          money -= 1000;
          
          //Message
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $1000: Power update!",1600));
        }else{
          popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600));
        }
      }
      
      lastAction = millis();
    }
  }
  
  public void draw(){
    
    if(alive){
      //The ship body
      stroke(255);
      fill(255);
      triangle(position.x+sin(rotation+PI/2)*10,position.y+cos(rotation+PI/2)*10,
      position.x+sin(rotation+((5*PI)/4))*10,position.y+cos(rotation+((5*PI)/4))*10,
      position.x+sin(rotation+((7*PI)/4))*10,position.y+cos(rotation+((7*PI)/4))*10);
      
      //The cabin
      stroke(100,100,200);
      fill(100,100,200);
      triangle(position.x+sin(rotation+PI/2)*6,position.y+cos(rotation+PI/2)*6,
      position.x+sin(rotation+((5*PI)/4))*6,position.y+cos(rotation+((5*PI)/4))*6,
      position.x+sin(rotation+((7*PI)/4))*6,position.y+cos(rotation+((7*PI)/4))*6);
      
      //The turret
      t.draw();
      
      //Life rect bg
      stroke(0,0,0,0);
      fill(0,200,40,120);
      rect(position.x-15, position.y-30, 30,5);
      //Life rect w = 30
      stroke(0,0,0,0);
      fill(0,255,40);
      rect(position.x-15, position.y-29, 30*(hull*1f/maxHull*1f),3);
      
      //Draw action cool down for repair
      if(millis() - lastRepair < repairDelay){
        stroke(0,0,0,0);
        fill(150,190,240,100);
        rect(position.x-15, position.y, 30*((millis() - lastRepair)*1f/repairDelay*1f),5);
      }
    }else{
      stroke(200,250,60);
      fill(255,255,60);
      ellipse(position.x-10,position.y-10,20,20);
    }
  }
  
}
