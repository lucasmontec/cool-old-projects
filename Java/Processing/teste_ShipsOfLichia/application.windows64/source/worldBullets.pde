static class worldBullets{
  public static ArrayList<bullet> bullets = new ArrayList<bullet>();
  public static ArrayList<bullet> rbullets = new ArrayList<bullet>();
  
  public static void updateBullets(){
    //update bullets
    for(bullet b : bullets){
      if(b.alive){
        b.update();
        b.draw();
      }else{
        rbullets.add(b); 
      }
    }
    //remove bullets
    if(rbullets.size() > 0){
      for(bullet b : rbullets){
        bullets.remove(b);
        b = null;
      }
      rbullets.clear();
    }
  }
}
