static class popUpManager{
  public static ArrayList<popUp> pops = new ArrayList<popUp>();
  public static ArrayList<popUp> rpops = new ArrayList<popUp>();
  
  public static void draw(){
    //update popUps
    if(pops.size() > 0){
      for(popUp b : pops){
        if(b.alive){
          b.draw();
        }else{
          rpops.add(b); 
        }
      }
    }
    
    //remove popUps
    if(rpops.size() > 0){
      for(popUp b : rpops){
        pops.remove(b);
        b = null;
      }
      rpops.clear();
    }
  }
}
