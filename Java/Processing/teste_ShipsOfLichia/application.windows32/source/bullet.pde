class bullet{
  public PVector position;
  public PVector velocity;
  public int dmg;
  public boolean alive;
  public long lastFlash = 0;
  public int flashInterval = 100;
  public boolean fToggle = false;
  
  public bullet(float ox, float oy, float tx, float ty, int d){
    dmg = d;
    position = new PVector(ox, oy);
    velocity = new PVector(tx-ox, ty-oy);
    velocity.normalize();
    velocity.mult(2.8);
    alive = true;
  }
  
  public void update(){
    position.add(velocity);
    
    //Autokill
    if(position.x < 0 || position.x > width
    || position.y < 0 || position.y > height){
      alive = false;
    }
  }
  
  public void draw(){
    if(millis() - lastFlash > flashInterval){
      fToggle = !fToggle;
      lastFlash = millis();
    }
    
    stroke(0,0,0,0);
    if(fToggle){
      fill(120,120,20);
    }else{
      fill(255,255,20);
    }
    ellipse(position.x-2,position.y-2,4,4);
  }
}
