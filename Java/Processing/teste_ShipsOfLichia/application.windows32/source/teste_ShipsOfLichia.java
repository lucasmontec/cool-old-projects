import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class teste_ShipsOfLichia extends PApplet {

ship s;
Level level;

public void setup(){
  size(1024,768);
  smooth();
  
  s = new ship();
  level = new Level(s);
}

public void draw(){
  background(0);
 
  if(s.alive){
    if(mousePressed){
      s.shot();
    }
    
    if(keyPressed){
      s.keyPressed(key); 
    }
    
    s.draw();
    s.update();
  }
  
  level.update();
  level.draw();
  
  worldBullets.updateBullets();
}
class Level{
  
  public int level = 1;
  public float playerScore = 0;
  public long levelDuration = 5000;
  public long levelStart = 0;
  public long mSpawnTime = 1000;
  public long mLastSpawn = 0;
  public long mSpawnTimeDec = 3;
  ArrayList<monster> monsters = new ArrayList<monster>();
  ArrayList<monster> rmonsters = new ArrayList<monster>();
  private ship player;
  public final int max_Monsters = 50;
  PFont fonte;
  
  public Level(ship s){
    player = s;
    fonte = loadFont("CourierNew36.vlw");
    textFont(fonte, 12);
  }
  
  public void resetLevel(){
    level = 1;
    playerScore = 0;
    monsters.clear();
  }
  
  public void update(){
    
    //Only update if the player is alive
    if(player.alive){
      
      //process lvel
      if(millis()-levelStart > levelDuration){
        //Increase level
        level++;
        
        //Calc new duration
        levelDuration = 10000 + level*500;
        
        //New spawn time
        mSpawnTime -= mSpawnTimeDec;
        
        //Store start
        levelStart = millis();
      }
      
      //Spawn monsters
      if(millis()-mLastSpawn > mSpawnTime && monsters.size() < max_Monsters){
        
        //Select x's and y's
        float x = 0;
        float y = 0;
        if(random(100) > 50){
          if(random(100) > 50){
            x = -10;
          }else{
            x = width+10;
          }
          y = random(0,height);
        }else{
          x = random(0,width);
          if(random(100) > 50){
            y = -10;
          }else{
            y = height+10;
          }
        }
        
        //Add some green monsters over time
        if(level > 15){
          if(random(100) > 85){
            monsters.add(
              //Spawn the basic monster
              new greenMonster(level,x,y,player)
            );
          }else{
            monsters.add(
            //Spawn the basic monster
            new monster(level,x,y,player)
            );
          }
        }else{
          //Spaw mosnter
          monsters.add(
            //Spawn the basic monster
            new monster(level,x,y,player)
          );
        }
        
        //store last
        mLastSpawn = millis();
      }
      
      //Update monstrs
      for(monster m : monsters){
        if(m.alive){
          m.update();
        }else{
          playerScore += m.score;
          player.money += m.money;
          popUpManager.pops.add(new popUp(m.position.x,m.position.y,color(240,240,30),"$"+m.money,570));
          rmonsters.add(m);
        }
      }
      
      if(rmonsters.size()>0){
        for(monster m : rmonsters){
          monsters.remove(m);
          m = null;
        }
        rmonsters.clear();
      }
    
    }
  }
  
  
  public void draw(){
    //Draw monstars
    for(monster m : monsters){
      m.draw(); 
    }
    
    //Draw level
    fill(255);
    text("Level: "+level,8,18);
    text("Score: "+ceil(playerScore),8,32);
    fill(50,240,50);
    text("Money: "+player.money,8,46);
    
    if(player.alive){
      stroke(0,0,0,0);
      fill(10,15,100);
      rect((width-150)/2,18,150,8);
      fill(60,20,250);
      rect((width-150)/2,18,150*((millis()-levelStart)*1f/levelDuration*1f),8);
      //text("Level percent: "+floor(((millis()-levelStart)*1f/levelDuration*1f)*100)+"%",8,64);
    }else{
      fill(255,40,60);
      text("Game over!",8,64);
    }
    
    //Draw popUps
    popUpManager.draw();
  }
}
class bullet{
  public PVector position;
  public PVector velocity;
  public int dmg;
  public boolean alive;
  public long lastFlash = 0;
  public int flashInterval = 100;
  public boolean fToggle = false;
  
  public bullet(float ox, float oy, float tx, float ty, int d){
    dmg = d;
    position = new PVector(ox, oy);
    velocity = new PVector(tx-ox, ty-oy);
    velocity.normalize();
    velocity.mult(2.8f);
    alive = true;
  }
  
  public void update(){
    position.add(velocity);
    
    //Autokill
    if(position.x < 0 || position.x > width
    || position.y < 0 || position.y > height){
      alive = false;
    }
  }
  
  public void draw(){
    if(millis() - lastFlash > flashInterval){
      fToggle = !fToggle;
      lastFlash = millis();
    }
    
    stroke(0,0,0,0);
    if(fToggle){
      fill(120,120,20);
    }else{
      fill(255,255,20);
    }
    ellipse(position.x-2,position.y-2,4,4);
  }
}
class greenMonster extends monster{
  
   public greenMonster(int lvl, float ox, float oy, ship t){
     super(lvl, ox, oy, t);
     
     dmg = lvl*4;
     score = 8*((lvl*2)+(lvl/10)^2);
     sz = 10 + lvl*0.5f;
     hp = floor(lvl*4.8f);
     hpMax = hp;
     money = lvl*2;
    
     col = color(random(160,190),0,0);
     position = new PVector(ox, oy);
     velocity = new PVector(t.position.x-ox, t.position.y-oy);
     velocity.normalize();
     velocity.mult(0.3f);
     
     thinkTime = 1000;
  }
  
  public void think(){
    if(millis()-lastThink > thinkTime){
      
      //Go to player
      velocity = new PVector(target.position.x-position.x, target.position.y-position.y);
      velocity.normalize();
      velocity.mult(0.3f);
      
      lastThink= millis();
    }
  }
  
  public void draw(){
    //The mosnter
    stroke(0,0,0,0);
    fill(20,242,30);
    ellipse(position.x-(sz/2),position.y-(sz/2),(sin(millis()/180f)*1.6f)+sz,(cos(millis()/180f)*1.6f)+sz);
    
    //The life bar
    if(hp < hpMax){
      stroke(20,250,20);
      fill(20,250,20);
      rect(position.x-10,position.y-20,(hp*1f/hpMax*1f)*20,2);
    }
  }
}
class monster{
  
  public int hp;
  public int hpMax;
  public int dmg;
  public float sz = 8;
  public float score;
  public int money = 5;
  public PVector position;
  public PVector velocity;
  public final ship target;
  public long thinkTime = 2500;
  public long lastThink = 0;
  public boolean alive = true;
  public int col;
  
  public monster(int lvl, float ox, float oy, ship t){
    dmg = lvl*3;
    score = 5*((lvl*2)+(lvl/10)^2);
    target = t;
    sz = 8 + lvl*0.5f;
    hp = floor(lvl*2.5f);
    hpMax = hp;
    money = lvl;
    
    col = color(random(160,190),0,0);
    position = new PVector(ox, oy);
    velocity = new PVector(t.position.x-ox, t.position.y-oy);
    velocity.normalize();
    velocity.mult(1.2f);
  }
  
  public void think(){
    if(millis()-lastThink > thinkTime){
      
      //Go to player
      velocity = new PVector(target.position.x-position.x, target.position.y-position.y);
      velocity.normalize();
      velocity.mult(0.8f);
      
      lastThink= millis();
    }
  }
  
  public void takeDamage(int dmg){
    if(hp-dmg <= 0){
      alive = false;
      hp = 0;
    }else{
      hp -= dmg; 
    }
  }
  
  public void update(){
    //Move
    position.add(velocity);
    
    //Think
    think();
    
    //Hit player
    if(position.dist(target.position) < 10){
      target.takeDamage(dmg);
      alive = false;
    }
    
    //Get hit by bullets
    if(worldBullets.bullets.size() > 0){
      for(bullet b : worldBullets.bullets){
        if(b.alive){
          if(position.dist(b.position) < sz){
            takeDamage(b.dmg);
            b.alive = false;
          }
        }
      }
    }
    
    //Autokill
    /*if(position.x < 0 || position.x > width
    || position.y < 0 || position.y > height){
      alive = false;
    }*/
  }
  
  public void draw(){
    //The mosnter
    stroke(0,0,0,0);
    fill(col);
    ellipse(position.x-(sz/2),position.y-(sz/2),(sin(millis()/100f)*1.4f)+sz,(cos(millis()/100f)*1.4f)+sz);
    
    //The life bar
    if(hp < hpMax){
      stroke(20,250,20);
      fill(20,250,20);
      rect(position.x-10,position.y-20,(hp*1f/hpMax*1f)*20,2);
    }
  }
}
class popUp{
  
  public String text;
  public int col;
  public float x,y;
  public boolean alive = true;
  private long startTime=0;
  private int duration;
  private float t;
  
  public popUp(float x, float y, int c, String txt, int dur){
    text = txt;
    col = c;
    this.x = x;
    this.y = y;
    startTime = millis();
    duration = dur;
  }
  
  public void draw(){
    //Only draw alive
    if(alive){
      //Calc time
      t = millis() - startTime;
      
      //Draw the tip
      fill(lerpColor(col,color(0,0,0,0),constrain(t*1f/duration*1f,0,1)));
      text(text,x,y); 
      
      //Up text
      y -= 0.001f*(2000-duration);
      
      //Time check
      if(t > duration){
        alive = false;
      }
    }
  }
  
}
static class popUpManager{
  public static ArrayList<popUp> pops = new ArrayList<popUp>();
  public static ArrayList<popUp> rpops = new ArrayList<popUp>();
  
  public static void draw(){
    //update popUps
    if(pops.size() > 0){
      for(popUp b : pops){
        if(b.alive){
          b.draw();
        }else{
          rpops.add(b); 
        }
      }
    }
    
    //remove popUps
    if(rpops.size() > 0){
      for(popUp b : rpops){
        pops.remove(b);
        b = null;
      }
      rpops.clear();
    }
  }
}
class ship{
  
  public PVector position;
  public PVector velocity;
  public float rotation;
  public turret t;
  public int hull = 100;
  public int maxHull = 100;
  public boolean alive = true;
  public int money = 0;
  
  //delays
  public long lastRepair = 0;
  public int repairDelay = 4000;
  public long lastAction = 0;
  public int actionDelay = 1000;
  public boolean validAction = false;
  
  public ship(){
    position = new PVector(width/2,height/2);
    velocity = new PVector();
    rotation = radians(90);
    t = new turret(this);
  }
  
  public void takeDamage(int dmg){
    if(hull-dmg <= 0){
      alive = false;
      hull = 0;
    }else{
      hull -= dmg; 
    }
  }
  
  public void addVelForward(float v){
    velocity.add(sin(rotation+PI/2)*v, cos(rotation+PI/2)*v,0);
  }
  
  public void move(){
     position.add(velocity);
  }
  
  public void rotate(float side){
    rotation += radians(side);
  }
  
  public void shot(){
   t.shot(); 
  }
  
  public void update(){
    move();
    t.update();
    
    //screen lock
    if(position.x < -40){
      position.x = width;
    }
    if(position.x > width+40){
      position.x = -10;
    }
    if(position.y < -40){
      position.y = height;
    }
    if(position.y > height+40){
      position.y = -10;
    }
  }
  
  public void keyPressed(char k){
    //Movement
    if(k == 'a'){
      rotate(5);
    }
    if(k == 'd'){
      rotate(-5);
    }
    if(k == 'w'){
      addVelForward(0.02f);
    }
    if(k == 's'){
      addVelForward(-0.02f);
    }
    if(k == ' '){
      velocity.mult(0.96f);
    }
    
    //Repair - cost: 50
    if(k == 'r'){
      if(millis() - lastRepair > repairDelay){
        if(money >= 50){
          if(hull + 30 < maxHull){
            hull = hull+30;
          }else{
            hull = maxHull;
          }
          
          //Pay
          money -= 50;
          
          //MSG
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $50",1600));
        }else{
           popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600)); 
        }
        
        lastRepair = millis();
      }
    }
    
    //Upgrades
    if(millis() - lastAction > actionDelay){
      
      //Update hull +100
      if(k == 'h'){
        if(money >= 900){
          //Upgrade the hull
          maxHull += 100;
          
          //Pay
          money -= 900;
          
          //Message
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $900: Hull update!",1600));
        }else{
          popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600));
        }
      }
      
      //Update dmg +2
      if(k == 'p'){
        if(money >= 1000){
          //Upgrade the dmg
          t.setDamage(t.damage+2);
          
          //Pay
          money -= 1000;
          
          //Message
          popUpManager.pops.add(new popUp(position.x,position.y,color(240,140,30),"- $1000: Power update!",1600));
        }else{
          popUpManager.pops.add(new popUp(position.x,position.y,color(250,30,30),"no money",1600));
        }
      }
      
      lastAction = millis();
    }
  }
  
  public void draw(){
    
    if(alive){
      //The ship body
      stroke(255);
      fill(255);
      triangle(position.x+sin(rotation+PI/2)*10,position.y+cos(rotation+PI/2)*10,
      position.x+sin(rotation+((5*PI)/4))*10,position.y+cos(rotation+((5*PI)/4))*10,
      position.x+sin(rotation+((7*PI)/4))*10,position.y+cos(rotation+((7*PI)/4))*10);
      
      //The cabin
      stroke(100,100,200);
      fill(100,100,200);
      triangle(position.x+sin(rotation+PI/2)*6,position.y+cos(rotation+PI/2)*6,
      position.x+sin(rotation+((5*PI)/4))*6,position.y+cos(rotation+((5*PI)/4))*6,
      position.x+sin(rotation+((7*PI)/4))*6,position.y+cos(rotation+((7*PI)/4))*6);
      
      //The turret
      t.draw();
      
      //Life rect bg
      stroke(0,0,0,0);
      fill(0,200,40,120);
      rect(position.x-15, position.y-30, 30,5);
      //Life rect w = 30
      stroke(0,0,0,0);
      fill(0,255,40);
      rect(position.x-15, position.y-29, 30*(hull*1f/maxHull*1f),3);
      
      //Draw action cool down for repair
      if(millis() - lastRepair < repairDelay){
        stroke(0,0,0,0);
        fill(150,190,240,100);
        rect(position.x-15, position.y, 30*((millis() - lastRepair)*1f/repairDelay*1f),5);
      }
    }else{
      stroke(200,250,60);
      fill(255,255,60);
      ellipse(position.x-10,position.y-10,20,20);
    }
  }
  
}
class turret{
  
  public float rotation;
  public ship owner;
  public final long fireDelay = 100;
  public long lastFired = 0;
  public final int maxAmmo = 27;
  public int damage = 5;
  
  public float ahelper = 0;
  
  public turret(ship o){
    owner = o;
    rotation = 0;
  }
  
  public void shot(){
    //Fire
    if(millis()-lastFired > fireDelay && worldBullets.bullets.size() < maxAmmo){
      //fire a bullet
      worldBullets.bullets.add(
      new bullet(
      owner.position.x+sin(rotation-PI/2)*12,
      owner.position.y+cos(rotation-PI/2)*12,
      owner.position.x+sin(rotation-PI/2)*20,
      owner.position.y+cos(rotation-PI/2)*20,
      damage)
      );
      
      lastFired = millis();
    }
  }
  
  public void setDamage(int d){
    damage = d; 
  }
  
  public float lerpAngle(float a, float fac){
    float delta = a-rotation;
    
    if(abs(delta) > PI){
       delta = (delta > 0 ? (delta - 2*PI) : (delta + 2*PI));
    }
    
    return delta*fac;
  }
  
  public void update(){
    ahelper = -atan2(owner.position.y-mouseY,owner.position.x-mouseX);
    rotation += lerpAngle(ahelper,.07f);
  }
  
  public void draw(){
    stroke(255,60,60);
    strokeWeight(4);
    line(owner.position.x,owner.position.y,
    owner.position.x+sin(rotation-PI/2)*12,owner.position.y+cos(rotation-PI/2)*12);
    strokeWeight(1);
  }
}
static class worldBullets{
  public static ArrayList<bullet> bullets = new ArrayList<bullet>();
  public static ArrayList<bullet> rbullets = new ArrayList<bullet>();
  
  public static void updateBullets(){
    //update bullets
    for(bullet b : bullets){
      if(b.alive){
        b.update();
        b.draw();
      }else{
        rbullets.add(b); 
      }
    }
    //remove bullets
    if(rbullets.size() > 0){
      for(bullet b : rbullets){
        bullets.remove(b);
        b = null;
      }
      rbullets.clear();
    }
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "teste_ShipsOfLichia" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
