//Physics Test

float GRAVIDADE = 0.981;
bola b;

void setup(){
 background(0);
 size(400,400);
 smooth();
 
 b = new bola(1);
 b.x = width/2;
 b.y = height/2 - height/4;
}

void draw(){
  background(0);
  
  b.pUpdate();
  b.show();
  
  if(mousePressed){
    //b.addVel(2);
    b.setVel(10);
  }
}

class bola{
  
  int massa = 10;
  float v = 0;
  float x = 0;
  float y = 0;
  
  bola(int m){
    massa = m;
  }
  
  bola(){
  }
  
  void setVel(float v){
   this.v = v; 
  }
  
  void addVel(float v){
    this.v += v;
  }
  
  void pUpdate(){
    if(y < height-10 & v<=0){
      v -= GRAVIDADE;
      y -= v;
    }
    if(v>=0){
      v -= GRAVIDADE;
      y -= v;
    }
    if(y >= height-10){
      this.y =  height-10;
      this.setVel(-this.v*0.9);
    }
  }
  
  void show(){
    ellipse(x,y,massa*10,massa*10);
  }
}
