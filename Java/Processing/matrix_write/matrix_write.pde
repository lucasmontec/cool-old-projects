String buff = "";
int margem = 5, sTime = 0, timeOut = 0;
float factor[] = new float[40];
ponto p[] = new ponto[40];
PFont f;

void setup(){
  size(800,400);
  background(0);
  
  f = loadFont("CourierNew36.vlw");
  textFont(f, 32);
  
  for (int i=0;i<40;i++){
    p[i] = new ponto();
    factor[i] = random(5,40);
  }
}

void draw(){
  background(0);
  
  if (sTime > 0){
    timeOut = (millis() - sTime)/10;
  }
  
  for(int i=0;i<buff.length();i++){
    //primary text
    fill(0,map(factor[i],2,40,80,255),0);
    p[i].setPos( margem+i*textWidth("a") , (timeOut/(factor[i]))+40 );
    text(buff.charAt(i),(float)p[i].getPos(0),(float)p[i].getPos(1));
    
    //more text behind
    if(timeOut>0){
      fill(0,map(factor[i],2,40,50,160),0);
      for(int r=0;r<20;r++){
        int umasim = r%2;
        p[i].setPos( margem+i*textWidth("a") , (timeOut/(factor[39-i]))+40+ - umasim*(r*3*factor[39-i]) );
        text(buff.charAt(i),(float)p[i].getPos(0),(float)p[i].getPos(1));
      }
    }
  }
  
}

void keyPressed()
{
  char k;
  k = (char)key;
  
  if (key != 8 && key != 10 && (textWidth(buff)+2*margem+textWidth("a")) < width){
    buff += k;
  }else{
   for(int i=0;i<40;i++){
     factor[i] = random(2,40);
   }
    if (key != 10){
    buff = "";
    sTime = 0;
    timeOut = 0;
    }else{
      sTime = millis();
    }
  }
}
