float sec = 0, hora = 0, minu = 0, mills = 0, divisor = 1;
float r, k=0, p=0, factor = 0.0001;
PFont font1;

void setup(){
  background(0);
  size(500,500);
  smooth();
  noStroke();
  textAlign(CENTER);
  font1 = loadFont("AgencyFB-Bold-14.vlw");
  textFont(font1, 14);
}

void draw(){
  background(0);
 
  sec = second();
  minu = minute();
  hora = hour();
  mills = millis()/100 % 100;
  
  fill(255,255,255);
  text(second(),250,250+7);
 
  //mili segundos
  for(float i=0;i<100;i++){
    if((i+1)== int(mills)){
      fill(0,255,20,255);
      r = 5;
    }else{
      fill(200,200,200,170);
      r = 2;
    }
    k = sin(millis()*factor);
    p = cos(millis()*factor);
    ellipse(250+(sin(p+i*(radians(360/50)))*30),250+(cos(k+i*(radians(360/50)))*30),r,r);
  }
  
  //segundos
  for(float i=0;i<60;i++){
    if((i+1)== sec){
      fill(90,90,255,255);
      r = 10;
    }else{
      fill(200,200,200,170);
      r = 3;
    }
    k = sin(millis()*factor);
    p = cos(millis()*factor);
    ellipse(250+(sin(p+radians((i/60)*360))*50),250+(cos(k+radians((i/60)*360))*50),r,r);
  }
  
  //minutos
  for(float i=0;i<60;i++){
    k = sin(millis()*factor);
    p = cos(millis()*factor);
    if((i+1)== minu){
      fill(180,160,0,255);
      r = 15;
      ellipse(250+(sin(p+i*radians(360/60))*70),250+(cos(k+i*radians(360/60))*70),r,r);
      fill(255,255,255);
      text(minute(),250+(sin(p+i*radians(360/60))*70),250+(cos(k+i*radians(360/60))*70)+7);
    }else{
      fill(200,200,200,150);
      r = 5;
      ellipse(250+(sin(p+i*radians(360/60))*70),250+(cos(k+i*radians(360/60))*70),r,r);
    }
  }
  
  //horas
  for(int i=0;i<24;i++){
    k = sin(millis()*factor);
    p = cos(millis()*factor);
    if((i+1)== hora){
      fill(255,0,0,255);
      r = 25;
      ellipse(250+(sin(p+i*radians(360/24))*100),250+(cos(k+i*radians(360/24))*100),r,r);
      fill(255,255,255);
      text(hour(),250+(sin(p+i*radians(360/24))*100),250+(cos(k+i*radians(360/24))*100)+7);
    }else{
      fill(255,255,255,150);
      r = 15;
       ellipse(250+(sin(p+i*radians(360/24))*100),250+(cos(k+i*radians(360/24))*100),r,r);
    }
  }
}
