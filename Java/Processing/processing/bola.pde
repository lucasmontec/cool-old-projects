class bola{
  
  private color my_col;
  private float transp = 80;
  private float my_size = 0;
  private ponto pos;
  private boolean is_connected;
  private bola connection;
  
 //----------------------construtores---------------------
   
 bola(){
  pos = new ponto();
  my_col = color(random(0,80),random(0,40),random(100,200));
  my_size = random(15,80);
  pos.setPos(random(my_size,width-my_size),random(my_size,height-my_size));
  transp = random(60,150);
 } 
  
 bola(float x, float y){
  pos = new ponto();
  my_col = color(random(0,80),random(0,40),random(100,200));
  my_size = random(15,80);
  pos.setPos(x,y);
  transp = random(60,150);
 }
  
 bola(float x, float y, float tm){
  pos = new ponto();
  my_col = color(random(0,80),random(0,40),random(100,200));
  my_size = tm;
  pos.setPos(x,y);
  transp = random(60,150);
 }
 
 bola(color col){
  pos = new ponto();
  my_col = col;
  my_size = random(5,40);
  pos.setPos(random(my_size,width-my_size),random(my_size,height+my_size));
  transp = random(60,150);
 }
 
 //--------------------------------------inicio das funções da classe-------------------------------
 
 ponto getCenter(){
   return pos;
 }
 
 color getColor(){
  return my_col; 
 }
 
 double getDistance(bola cmp){
   return cmp.getCenter().getDistance(pos);
 }
 
 bola getConnection(){
   if (is_connected){
    return connection;
   }else{
     return null;
   }
 }
 
 float x(){
  return (float)pos.getPos(0);  
 }
 
 float y(){
  return (float)pos.getPos(1); 
 }
 
 void disconnect(){
  is_connected = false;
  connection = null; 
 }
 
 void setConnection(bola target){
   is_connected = true;
   connection = target;
 }
 
 void setCenter(float x, float y){
   pos.setPos(x,y);
   
   if(is_connected){
     float drag = 20.0;
     float dx = (float) ( ( (this.x() + (random(-1,1)*my_size) ) - connection.x()) /drag );
     float dy = (float) ( ( (this.y() + (random(-1,1)*my_size) ) - connection.y()) /drag );
     connection.setCenter(connection.x()+dx,connection.y()+dy);
   }
 }
 
 void setSize(float sz){
   my_size = sz;
 }
 
 void setColor(color col){
   my_col = col;
 }
 
 void lineTo(float x, float y){
  stroke(255);
  strokeWeight(0.6);
  line((float)pos.getPos(0),(float)pos.getPos(1),x,y); 
 }
 
 void lineTo(float x, float y,color a,float sz){
  stroke(a);
  strokeWeight(sz);
  line((float)pos.getPos(0),(float)pos.getPos(1),x,y); 
 }
 
 void show(){
  noStroke();
  fill(color(255),255);
  ellipse((float)pos.getPos(0),(float)pos.getPos(1),4,4);
  fill(my_col,transp);
  ellipse((float)pos.getPos(0),(float)pos.getPos(1),my_size,my_size); 
  
  if (is_connected){
    this.lineTo(connection.x(),connection.y(),color(180,160,10),0.9);
  }
 }
}
