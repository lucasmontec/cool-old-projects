int MAX = 60;
bola bolas[] = new bola[MAX];

void setup(){
  size(400,400);
  background(0,10,40);
  smooth();
  
  for (int i=0;i<MAX;i++){
   bolas[i] = new bola(); 
  }
}

void draw(){
 background(0,10,40);
  
 for (int i=0;i<MAX;i++){
  if (bolas[i].x() > 0 && bolas[i].x() < width && bolas[i].y() > 0 && bolas[i].y() < height){
  bolas[i].setCenter(bolas[i].x()+random(-1.2,1.2),bolas[i].y()+random(-1.2,1.2));
  }else{
  bolas[i].setCenter(random(0,width),random(0,height));
  }
  bolas[i].show();
 }
 
 for (int b1 = 0;b1<MAX;b1++){
   for (int b2 = 0;b2<MAX;b2++){
       if (b1 != b2 && bolas[b1].getDistance(bolas[b2]) < 70){
        bolas[b1].lineTo(bolas[b2].x(),bolas[b2].y());
       }
   }
 }
}
