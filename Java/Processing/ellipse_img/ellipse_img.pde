
PImage im;

void setup(){
  size(400,200);
  noStroke();
  background(0);
  smooth();
  frameRate(10);
  
  im = loadImage("img.jpg");
}

void draw(){
  
  background(0);
  
  for(int x=0; x<im.width; x+=7){
      for(int y=0; y<im.height; y+=7){
        color col = im.get(x,y);
        ponto p, mp;
        p = new ponto();
        mp = new ponto();
        p.setPos(x+(width/2)-(im.width/2),y+(height/2)-(im.height/2));
        mp.setPos(mouseX,mouseY);
        float r = (float) p.getDistance(mp);
        fill(col,100);
        ellipse(x+(width/2)-(im.width/2),y+(height/2)-(im.height/2),80-r,80-r);
      }
  }
}
