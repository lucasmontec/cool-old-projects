class angle{
 //Uma classe para facilitar algumas contas mais harcore
 
 //Variáveis
 private float angle = 0.0f;
 
 //Construtores
 public angle(){
 }
 
 public angle(float a){
  angle = a; 
 }
 
 //Corpo funcional
 //pegadores
 public float pegaAng(){
  return angle; 
 }
  public float pegaRad(){
  return radians(angle); 
 }
 
 //setadores
 public void setaAng(float a){
   angle = a;
 }
 
 public void rotate(float a){
   angle += a;
 }
 
 //operações
 public void add(angle a){
   this.angle += a.pegaAng();
 }
 
 public void subtract(angle a){
   this.angle -= a.pegaAng();
 }
 
 public void multiply(angle a){
   this.angle *= a.pegaAng();
 }
 
 public void divide(angle a){
   this.angle /= a.pegaAng();
 }
 
}

static class angleCalc{
 //static operations
 public static float Delta(angle a, PVector posA, PVector posB, float smoothFactor){
   double angle = -degrees( atan2(posB.y - posA.y, posB.x - posA.x) );
   angle = (angle - a.pegaAng());
   if (abs((float)angle) > 180) {
     angle = angle > 0 ? angle - 360 : 360 + angle;
   }
   angle += 90;//fator de correção
   return (float) angle*smoothFactor;
 }
 
 public static float Diference(angle a, angle b){
   return (b.pegaAng() - a.pegaAng());
 }
 
 public static float Diference(angle a, PVector pa, PVector pb){
   return (degrees( atan2(pb.y - pa.y, pb.x - pa.x) ) - a.pegaAng() +180);
 } 
}
