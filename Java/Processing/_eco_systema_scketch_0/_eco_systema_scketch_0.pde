//declarações
populacao p = new populacao(20);

//pre processamento
void setup(){
  size(500,500);
  smooth();
  
  //fonte
  PFont font = loadFont("ArialMT-24.vlw");
  textAlign(CENTER);
  textFont(font, 12);
  
  background(0);
}

//processamento
void draw(){
  background(0);

  p.vaoPara(new PVector(mouseX,mouseY));
  p.draw();
}
