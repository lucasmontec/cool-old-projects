class populacao{
 
 //Variaveis de uma população
 criatura[] criaturas;
 private int tamanho_da_pop = 10;
 private float variancia = 0.9;
 
 //Construtores
 public populacao(int amt){
    tamanho_da_pop = amt;
    criaturas = new criatura[tamanho_da_pop];
    for(int i=0;i<tamanho_da_pop;i++){
        criaturas[i] = new criatura();
    }
 }
 
 public populacao(int amt, especie e){
    tamanho_da_pop = amt;
    criaturas = new criatura[tamanho_da_pop];
    for(int i=0;i<tamanho_da_pop;i++){
        criaturas[i] = new criatura(e);
    }
 }
 
 //corpo funcional
 public void setaVarianciaMov(float v){
   variancia = v;
 }
 
 public void vaoPara(PVector pos){
    for(int i=0;i<tamanho_da_pop;i++){
        criaturas[i].olhePara(pos);
        criaturas[i].moverFrente(variancia);
    }
 }
 
 public void draw(){
    for(int i=0;i<tamanho_da_pop;i++){
        criaturas[i].draw();
    }
 }
 
}
