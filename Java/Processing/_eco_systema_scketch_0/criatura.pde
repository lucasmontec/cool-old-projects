class criatura extends especie{
 
 //Variaveis da criatura
 PVector posicao = new PVector(0,0);
 public angle ang = new angle(0);
 private color col = color(0,0,0);
 private float sz = 10;
 private float rot_vel = 0.1f;
 
 //Variaveis dos fatores mutantes
 
 //Construtores
 public criatura(especie e){
   super(e);
   generateCreatureShape();
 }
 
 public criatura(){
   super();
   generateCreatureShape();
 }
 
 public void setaRotVel(float vel){
  rot_vel = vel; 
 }
 
 public float pegaRotVel(){
  return rot_vel; 
 }
 
 public void setaPosicao(float x, float y){
   posicao.x = x;
   posicao.y = y;
 }
 
 public void mover(float dx, float dy){
   posicao.x += dx;
   posicao.y += dy;
 }
 
 public void moverFrente(){
   posicao.x += sin(ang.pegaRad())*mov_rate;
   posicao.y += cos(ang.pegaRad())*mov_rate;
 }
 
 public void moverFrente(float rnd){
   posicao.x += sin(ang.pegaRad())*mov_rate + random(-rnd,rnd);
   posicao.y += cos(ang.pegaRad())*mov_rate + random(-rnd,rnd);
 }
  
 public void olhePara(PVector vec){
   this.ang.rotate( angleCalc.Delta(this.ang, this.posicao,vec, rot_vel) );
 }
  
 //metodos de renderização
 private void generateCreatureShape(){
   //seleciona uma cor
   if(tipo == "animal"){
     col = color(random(255-255*fator_mutante,255),random(60*(1-fator_mutante),60),random(130*(1-fator_mutante),140));
   }else{
     col = color(random(20*(1-fator_mutante),20),random(255-255*fator_mutante,255),random(60*(1-fator_mutante),150));
   }
   //seleciona um tamanho
   if(tipo == "animal"){
     sz = tamanho + random(60+(fator_mutante*20),80);
   }else{
     sz = tamanho + random(20+(fator_mutante*20),40);
   }
 }
 
 //renderiza a criatura
 public void draw(){
   fill(col);
   ellipse(posicao.x,posicao.y,sz,sz);
   stroke(0);
   strokeWeight(sz/8);
   line(posicao.x,posicao.y,posicao.x+sin(ang.pegaRad())*(sz/2),posicao.y+cos(ang.pegaRad())*(sz/2));
   fill(col);
   text(pegaNome(), posicao.x, posicao.y + (sz/2) + 14);
 }
}
