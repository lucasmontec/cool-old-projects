// Insects -by lucas m. carvalhaes

insect i;
insect p;

void setup(){
  smooth();
  background(0);
  size(700,700);
  
  i = new insect();
  p = new insect();
  p.Move(20,0);
}

void draw(){
  background(0);
  
  //i.MoveForward(1);
  //i.addAngle(0.01);
  i.show();
  i.IntersectAngle(p,0.1);
  p.show();
}
