float graphSize=10;
int screenSize=512;
int halfScreen=screenSize/2;
int density = 4;
formula2d XequalsY = new formula2d("x==y");

void setup(){
size(screenSize,screenSize);
smooth();  
}

float x=0,y=0;
void draw(){
 background(255);   
 
 //Stuff
 pushMatrix();
 translate(halfScreen,halfScreen);
 scale(1, -1);
 
 stroke(color(0,0,0,80));
 strokeWeight(1);
 for(int i=-(int)graphSize;i<=(int)graphSize;i++){
   x= toScreen(i);
   line(x,-halfScreen,x,halfScreen);
 }
 for(int i=-(int)graphSize;i<=(int)graphSize;i++){
   y= toScreen(i);
   line(-halfScreen,y,halfScreen,y);
 }
 
 stroke(color(0,0,0,255));
 strokeWeight(1);
 XequalsY.plot(halfScreen,density,graphSize);
 for(int ix=-halfScreen;ix<halfScreen;ix+=density){
    for(int iy=-halfScreen;iy<halfScreen;iy+=density){
      x = fromScreen(ix);
      y = fromScreen(iy);
      
      //Reta x==y
      /*if(XequalsY.check(x,y)){
        point(toScreen(x),toScreen(y));
      }*/
      
      //Circ (x-1)^2 + (y-1)^2 = 2
      /*if(pow(x-1,2) + pow(y-1,2) <= 2){
        point(toScreen(x),toScreen(y));
      }*/
    }
 } 
 popMatrix();
 
 //Descartes
 stroke(0);
 strokeWeight(2);
 line(halfScreen,0,halfScreen,screenSize);
 line(0,halfScreen,screenSize,halfScreen);
 
 //Zoom
 if(keyPressed){
    if (key == CODED) {
      if (keyCode == UP) {
          graphSize+=0.1f;
      }
      if (keyCode == DOWN) {
          graphSize-=0.1f;
      }
    }
 }
 
}

void keyPressed(){
  if (keyCode == LEFT) {
    density = (density<3?density:density-1);
  }
  if (keyCode == RIGHT) {
    density = (density>9?density:density+1);
  }
}

float fromScreen(float val){
   return (val/halfScreen)*graphSize;
}

float toScreen(float val){
  return (val/graphSize)*halfScreen;
}
