static class scriptParser{
   // create a script engine manager
  public static final ScriptEngineManager factory = new ScriptEngineManager();
  // create JavaScript engine
  public static final ScriptEngine engine = factory.getEngineByName("JavaScript");
  
  
}
