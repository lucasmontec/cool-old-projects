#include <string.h>
#include <time.h>
#include <cmath>
#include <ctime>
#include <windows.h>
#include <stdio.h>
#include <iostream>

#include "C_Colours.h"

#define SIZEY 20
#define SIZEX 52

int randomStrict(int mul){
    return (rand()%2)*mul;
}

int random(int lower=0, int top=100){
    int rnd = (rand()%(top-lower)) + lower;
    return rnd;
}

char randomChar(){
    return (char)random(92, 92+50);
}

void delay(long double milliseconds)
{
  long double timeout = clock() + milliseconds;
  while( clock() < timeout ) continue;
}

void playIntro(HANDLE console){
    puts("Iniciando a matrix");
    delay(200);
    system("cls");
    puts("Iniciando a matrix.");
    delay(200);
    system("cls");
    puts("Iniciando a matrix..");
    delay(200);
    system("cls");
    puts("Iniciando a matrix...");
    delay(200);
    system("cls");
    printf("Iniciando a matrix...");
    SetConsoleTextAttribute(console,Color(lightgreen,black));
    printf("[READY]");
    delay(400);
    system("cls");
}

/////////////////////////////////////////////////////////////////
main()
{
    //Set our random seed
    srand(time(NULL));

    //A hancle to our console
    HANDLE Console;

    //Set our console
    Console = GetStdHandle(STD_OUTPUT_HANDLE);

    //Create a screen
    int tela[SIZEX][SIZEY];
    char reminder[SIZEX][SIZEY];

    //Zero it
    for(int y=0;y<SIZEY;y++){
            for(int x=0;x<SIZEX;x++){
               tela[x][y] = 0;
            }
    }

    //Create a falling line generator
    int lineGenerator[SIZEX];

    //Prepare the first generator
    for(int x=0;x<SIZEX;x++){
        if(random() > 50)
        lineGenerator[x] = 0;
        else
        lineGenerator[x] = -1;
    }

    //End of prepare part
    //Now is the matrix loop
    //First the loop logic

    //Display intro
    playIntro(Console);

    //Run while user don't want to quit
    while(1){

        // ESC fecha o programa
        if (GetAsyncKeyState(0x1b)){
            break;
        }

        //Randomize the falling line gen
        for(int x=0;x<SIZEX;x++){
            if(lineGenerator[x] == -1 && random() > 70){
                if(random() > 50)
                lineGenerator[x] = 0;
                else
                lineGenerator[x] = -1;
            }
        }

        //Drop the line in the screen
        for(int y=0;y<SIZEY;y++){
            for(int x=0;x<SIZEX;x++){
               //From the line to the screen
               //Get a number from the gen and drop it to screen
               if(tela[x][y] > 0){
                    tela[x][y]--;
               }
               if(lineGenerator[x] == y){
                    tela[x][y] = 9;
               }
            }
        }

        //Move the line generator indexes
        for(int x=0;x<SIZEX;x++){
            if(lineGenerator[x] >= 0){
                    lineGenerator[x]++;
            }
            //Stop line generator values
            if(lineGenerator[x] > SIZEY){
                   lineGenerator[x] = -1;
            }
        }

        //Print the screen buffer
        for(int y=0;y<SIZEY;y++){
            for(int x=0;x<SIZEX;x++){
                if(tela[x][y] == 9){
                    //Set color for gen
                    SetConsoleTextAttribute(Console,Color(lightwhite,black));

                    //Gen a char
                    reminder[x][y] = randomChar();

                    //Draw the generator
                    std:: cout << reminder[x][y];
                }else if(tela[x][y] > 0){
                    //Set the colors
                    SetConsoleTextAttribute(Console,Color(green,black));
                    if(tela[x][y] >= 8)
                        SetConsoleTextAttribute(Console,Color(lightyellow,black));
                    if(tela[x][y] >= 4 && tela[x][y] < 8)
                        SetConsoleTextAttribute(Console,Color(lightgreen,black));

                    //Draw gen char
                    std:: cout << reminder[x][y];
                }else
                    std:: cout << " ";
            }
            std:: cout << std::endl;
        }

        //Clear the cursor pos
        COORD pos = {0,0};
        SetConsoleCursorPosition(Console, pos);

    }
}
