#ifndef ZOMBIELIB_H_INCLUDED
#define ZOMBIELIB_H_INCLUDED

#include <time.h>
#include <cmath>
#include <ctime>
#include <windows.h>
#include <iostream>

#define black 0
#define blue 1
#define green 2
#define watergreen 3
#define red 4
#define purple 5
#define yellow 6
#define white 7
#define grey 8
#define lightblue 9
#define lightgreen 10
#define lightwatergreen 11
#define lightred 12
#define lilac 13
#define lightyellow 14
#define lightwhite 15

int Color(int foreGround, int backGround){
    if(foreGround > 15){
         foreGround = 15;
    }

    if(backGround > 15){
         backGround = 15;
    }

    return foreGround+backGround*16;
}

int randomStrict(int mul){
    return (rand()%2)*mul;
}

int random(int lower=0, int top=100){
    int rnd = (rand()%(top-lower)) + lower;
    return rnd;
}

char randomChar(){
    return (char)random(92, 92+50);
}

void delay(long double milliseconds)
{
  long double timeout = clock() + milliseconds;
  while( clock() < timeout ) continue;
}

void randomSeed(){
    srand(time(NULL));
}

HANDLE getConsole(){
    return GetStdHandle(STD_OUTPUT_HANDLE);
}

void homeCursor(HANDLE console){
    COORD pos = {0,0};
    SetConsoleCursorPosition(console, pos);
}

void setCursorPos(HANDLE console, COORD pos){
    SetConsoleCursorPosition(console, pos);
}

void setColor(HANDLE console, char colorFore, char colorBack ){
    SetConsoleTextAttribute(console,Color(colorFore,colorBack));
}

void slowWrite(char* str, int time){
    while(*str != '\0'){
        std::cout << (*str);
        delay(time);
        str++;
    }
}

void slowWriteRandomForeground(HANDLE console, char* str, int time){
    while(*str != '\0'){
        setColor(console, random(1,16), black);
        std::cout << (*str);
        delay(time);
        str++;
    }
}

void randomForeground(HANDLE console){
    setColor(console, random(1,16), black);
}

void slowWriteRandomColors(HANDLE console, char* str, int time){
    while(*str != '\0'){
        setColor(console, random(0,16), random(0,16));
        std::cout << (*str);
        delay(time);
        str++;
    }
}

void clear(){
    system("cls");
}

#endif // ZOMBIELIB_H_INCLUDED
