/*
  Jogo: As Piranhas GOTY Black Edition v1.337

  TO_DO's:
    *Pulo do player deve ser de uns 4 tiles para cima
    *Implementar piranhas
    *Plataformas
    *Mudan�as no mapa

*/

#include <iostream>
#include "zombieLib.h"
#define ESC 0x1b
#define SPACE 0x20
#define LEFT 37
#define RIGHT 39

using namespace std;

//Prototipos
void intro(HANDLE console);
void drawLogo(HANDLE console);
int menu(HANDLE console);

//Mapa
const int w_mapa = 45, h_mapa = 10;
char mapa[w_mapa][h_mapa];

//Player
const char player = 0x1;
COORD player_pos = {1,1};

//Prototipos 2
void desenhaMapa(HANDLE console, char mapa[w_mapa][h_mapa]);
bool nochao();

int main(){
  //Inicializa mapa
  for(int y=0; y<h_mapa;y++){
    for(int x=0; x<w_mapa;x++){
      //Zera geral
      mapa[x][y] = ' ';

      //Ch�o
      if(y == 4){
        mapa[x][y] = '_';
      }

      //Bordas laterais
      if(x == 0){
        mapa[x][y] = '*';
      }
      if(x == w_mapa-1){
        mapa[x][y] = '*';
      }

      //Bordas topo
      if(y == 0){
        mapa[x][y] = '*';
      }
      if(y == h_mapa-1){
        mapa[x][y] = '*';
      }
    }
  }

  //Pegar o console para fazer coisas legais
  HANDLE console = getConsole();

  //Display intro
  intro(console);
  drawLogo(console);

  //Delay 4s
  delay(2500);

  //Reseta o cursor
  homeCursor(console);
  clear();

  //Chama menu
  int roda = menu(console);

  //Game stuff
  int timer_grav = 0;
  bool pulando = false;


  //Jogo
  while(roda == 1){
    //Update timer
    timer_grav += 1;
    //Zera
    homeCursor(console);
    //Desenha o mapa
    desenhaMapa(console, mapa);
    //Gravidade
    if(player_pos.Y < h_mapa-2 && timer_grav > 7 && !nochao()){
      player_pos.Y++;
      timer_grav = 0;
    }
    //Pulo
    //Pra pular o player precisa estar apertando barra e estar no chao
    if (GetAsyncKeyState(SPACE) && nochao()){
      pulando = true;
    }
    //Se ele estiver pulando entao ele s� para quando bater no teto do mapa
    if (pulando && player_pos.Y>1){
      player_pos.Y--;
    }
    //Se ele chegar no teto e estiver pulando, entao desativa o pulo. ai ele cai
    if(player_pos.Y==1 && pulando){
      pulando = false;
    }
    //Movimento lateral
    //Para direita
    if(player_pos.X<w_mapa-2 && GetAsyncKeyState(RIGHT)){
      player_pos.X++;
    }
    //Para esquerda
    if(player_pos.X>1 && GetAsyncKeyState(LEFT)){
      player_pos.X--;
    }

    //Sai
    if (GetAsyncKeyState(ESC)){
      roda = 0;
    }
  }

  //Cai fora
  return 0;
}

//Checa se o player ta no chao
bool nochao(){
  //Olha no mapa se onde o player est� tem um tile de chao
  if(mapa[player_pos.X][player_pos.Y] == '_')
    //Caso sim entao o player ta no chao, retornamos verdadeiro
    return true;
  //Se a fun��o n�o retornou verdadeiro ela cai aqui e retorna falso
  return false;
}

//Desenha o mapa na tela
void desenhaMapa(HANDLE console, char mapa[w_mapa][h_mapa]){
  //Esse for corre de 0 at� o tamanho em y do mapa
  for(int y=0; y<h_mapa;y++){
    //Esse aqui corre no x do mapa
    for(int x=0; x<w_mapa;x++){
      //Aqui a gente ve se tile que vai ser desenhado tem o player
      if(x == player_pos.X && y == player_pos.Y){
        //Se ele tem entao ao inv�s de desesenhar ele, desenhamos o player
        cout << player;
      }else{
        //Escolhe uma cor aleatoria
        randomForeground(console);
        //Aqui nao tinha player entao desenhamos o mapa mesmo
        cout << mapa[x][y];
      }
    }
    //Ao final de cada 'varredura' do x pulamos uma linha
    cout << endl;
  }
}

//Desenho do cash
void drawLogo(HANDLE console){
  COORD pos = {30,2};
  setCursorPos(console, pos);
  cout << "" << endl;
}

//Fun��o de menu
int menu(HANDLE console){
  int ret = 0;
  setColor(console, lightred, black);
  cout << "Digite 1 para jogar e 2 para sair:" << endl;
  do{
    cin >> ret;
    if(ret !=1 && ret !=2){
      cout << "Opcao invalida!!!11111" << endl;
    }
  }while(ret == 0);
  return ret;
}

//Fun��o de intro do jogo
void intro(HANDLE console){
    COORD pos = {30,1};
    setCursorPos(console, pos);
    slowWriteRandomForeground(console, (char*)"As piranhas", 200);
}
