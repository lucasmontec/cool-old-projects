#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define black 0
#define blue 1
#define green 2
#define watergreen 3
#define red 4
#define purple 5
#define yellow 6
#define white 7
#define grey 8
#define lightblue 9
#define lightgreen 10
#define lightwatergreen 11
#define lightred 12
#define lilac 13
#define lightyellow 14
#define lightwhite 15

int Color(int foreGround, int backGround);

main(){
       //Define a shadow vector
       int shadow[4] = { 176, 177, 178, 219 };
       
       //A hancle to our console
       HANDLE Console;
       
       //Set our console
       Console = GetStdHandle(STD_OUTPUT_HANDLE);
       
       //Draw some colorfull text
       for(int i=0;i<16;i++){
             
             for(int j=0;j<15;j++){
                 //Set the color
                 SetConsoleTextAttribute(Console,Color(i,j));    
                     
                 //Print a line
                 for(int k=0;k<4;k++){
                       printf("%c",shadow[k]);
                 }
                 //Space it
                 printf(" ");
             }
             
             printf("\n");
       
       }
       
       SetConsoleTextAttribute(Console,white);   
       printf("\n");
       //MessageBox(NULL, "Goodbye, cruel world!", "Note", MB_OK);
       system("pause");
       return 0;
       
}

int Color(int foreGround, int backGround){
    if(foreGround > 15){
         foreGround = 15;
    }
    
    if(backGround > 15){
         backGround = 15;
    }
    
    return foreGround+backGround*16;
}
